#pragma once

/** @file
    @brief Header file implementing the @p iDMRG struct template.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <boost/shared_ptr.hpp>
#include <complex>
#include <vector>
#include <random>
#include <memory>
#include <stdexcept>

#include <alps/hdf5.hpp>
#include <alps/numeric/matrix.hpp>

#include "dmrg/block_matrix/detail/alps.hpp"
#include "dmrg/utils/DmrgParameters.h"
#include "idmrg/mp_tensors/imps.h"

template<class M ,class SG>
class iMPO;

class iLattice;

namespace idmrg
{
	template<class Matrix,class SymmGroup>
	struct iModelDescriptor;

	/** @struct iDMRG
	    @brief struct providing all the functionality required for the implementation of the iDMRG algorithm
	*/
	template<class Matrix, class SymmGroup>
	class iDMRG
	{
		struct Impl;
	    std::unique_ptr<Impl> pimpl_;

	public:
		///// Typedefs /////

		/// Tensors ///

		typedef typename maquis::traits::real_type<Matrix>::type real_type;
		typedef typename iMPS<Matrix, SymmGroup>::scalar_type scalar_type;

		typedef Matrix matrix_t;
		typedef typename iMPS<Matrix,SymmGroup>::tensor_t mps_tensor_t;
		typedef typename iMPS<Matrix,SymmGroup>::block_matrix_t block_matrix_t;
		typedef typename iMPS<Matrix,SymmGroup>::block_diag_matrix_t block_diag_matrix_t;
		typedef typename iMPS<Matrix,SymmGroup>::lambda_t lambda_t;
		typedef typename iMPS<Matrix,SymmGroup>::boundary_t boundary_t;
		typedef block_matrix<matrix_t, SymmGroup> op_t;
		typedef alps::numeric::diagonal_matrix<real_type> real_diag_matrix_t;

		/// Containers ///

		typedef std::vector< boundary_t > boundary_cont_t;
		typedef std::vector< mps_tensor_t > mps_tensor_cont_t;

		/// Iterators ///

		typedef typename iMPS<Matrix,SymmGroup>::iterator mps_iterator;
		typedef typename iMPS<Matrix,SymmGroup>::const_iterator mps_const_iterator;
		typedef typename iMPS<Matrix,SymmGroup>::const_reverse_iterator mps_const_reverse_iterator;

		typedef typename MPO<Matrix,SymmGroup>::const_iterator mpo_const_iterator;
		typedef typename MPO<Matrix,SymmGroup>::const_reverse_iterator mpo_const_reverse_iterator;

		typedef typename boundary_cont_t::iterator bound_iterator;
		typedef typename boundary_cont_t::reverse_iterator bound_reverse_iterator;
		typedef typename boundary_cont_t::const_iterator bound_const_iterator;
		typedef typename boundary_cont_t::const_reverse_iterator bound_const_reverse_iterator;

		/// Descriptors ///

		typedef iModelDescriptor<Matrix,SymmGroup> imodel_descriptor_t;

		/// Others ///

		typedef std::pair<real_type,mps_tensor_t> result_t;
		typedef std::pair<std::complex<real_type>,block_matrix_t> complex_result_t;

		///// Methods /////

		/* Default constructor */
		iDMRG();

		/* Copy operations */
	    iDMRG( const iDMRG& ) = delete;
	    iDMRG& operator()( const iDMRG& ) = delete;

	    /* Move operations */
		iDMRG( iDMRG&& ) = delete;
		iDMRG& operator()( const iDMRG&& ) = delete;

	    /* Destructor */
	    ~iDMRG();

		Boundary<Matrix, SymmGroup> make_trivial_boundary();

		void init_left_boundaries(	mps_const_iterator it_mps ,
									const mps_const_iterator it_mps_e ,
									mpo_const_iterator it_mpo ,
									bound_iterator it_bound );

		void init_right_boundaries(	mps_const_reverse_iterator it_mps ,
									const mps_const_reverse_iterator it_mps_e ,
									mpo_const_reverse_iterator it_mpo ,
									bound_reverse_iterator it_bound );

		void initialize_boundaries(	const iMPS<Matrix,SymmGroup>& mps ,
									const MPO<Matrix,SymmGroup>& mpo ,
									boundary_cont_t& left_boundaries ,
									boundary_cont_t& right_boundaries );

		void grow_left_boundary(	mps_const_iterator it ,
									const mps_const_iterator it_e ,
									mpo_const_iterator it_mpo ,
									Boundary<Matrix, SymmGroup>& boundary );

		void grow_right_boundary( 	mps_const_reverse_iterator it ,
									const mps_const_reverse_iterator it_e ,
									mpo_const_reverse_iterator it_mpo ,
									Boundary<Matrix, SymmGroup>& boundary );

		void grow_boundaries(	const iMPS<Matrix,SymmGroup>& mps ,
								const MPO<Matrix,SymmGroup>& mpo ,
								boundary_cont_t& left_boundaries ,
								boundary_cont_t& right_boundaries );

		void shift_unit_cell( const int center , iMPO<Matrix,SymmGroup>& mpo );
		void shift_unit_cell( const int center , const int shift_site , imodel_descriptor_t& model_desc );
		void shift_unit_cell( iMPS<Matrix,SymmGroup>& mps );

		void prepare_guess( iMPS<Matrix,SymmGroup>& mps  , const std::size_t Mmax , const double tol , DmrgParameters& parms );
		void prepare_random( iMPS<Matrix,SymmGroup>& mps  , const std::size_t Mmax , const double tol );

		bool sweep_right(	iMPS<Matrix,SymmGroup>& mps ,
							iMPO<Matrix,SymmGroup>& mpo ,
							MPO<Matrix,SymmGroup>& cache_mpo ,
							boundary_cont_t& left_boundaries ,
							boundary_cont_t& right_boundaries ,
							DmrgParameters& parms );

		bool sweep_right_2site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								mpo_const_iterator cache_mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms );

		bool sweep_right_1site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms );

		bool sweep_left(	iMPS<Matrix,SymmGroup>& mps ,
							iMPO<Matrix,SymmGroup>& mpo ,
							MPO<Matrix,SymmGroup>& cache_mpo ,
							boundary_cont_t& left_boundaries ,
							boundary_cont_t& right_boundaries ,
							DmrgParameters& parms );

		bool sweep_left_2site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								mpo_const_iterator cache_mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms );

		bool sweep_left_1site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms );

		bool sweep_space(	iMPS<Matrix,SymmGroup>& mps ,
							iMPO<Matrix,SymmGroup>& mpo ,
							MPO<Matrix,SymmGroup>& cache_mpo ,
							boundary_cont_t& left_boundaries ,
							boundary_cont_t& right_boundaries ,
							DmrgParameters& parms );

		bool sweep_space(	mps_iterator mps ,
							const mps_const_iterator mps_end ,
							const int center ,
							mpo_const_iterator mpo ,
							mpo_const_iterator cache_mpo ,
							bound_iterator left_boundaries ,
							bound_iterator right_boundaries ,
							DmrgParameters& parms );

		std::size_t sweep_scales(	iMPS<Matrix,SymmGroup>& mps ,
									iMPO<Matrix,SymmGroup>& mpo ,
									boundary_cont_t& left_boundaries ,
									boundary_cont_t& right_boundaries ,
									iLattice& ilattice,
									imodel_descriptor_t& model_desc ,
									DmrgParameters& parms );

		void orthogonalize_left( 	const iMPS<Matrix,SymmGroup>& mps ,
									block_matrix_t& left_gauge ,
									const lambda_t& lambda_inv ,
									DmrgParameters& parms );

		void orthogonalize_right( 	const iMPS<Matrix,SymmGroup>& mps ,
									block_matrix_t& right_gauge ,
									const lambda_t& lambda_inv ,
									DmrgParameters& parms );

		void orthogonalize(	const iMPS<Matrix,SymmGroup>& mps ,
							const lambda_t& lambda_inv ,
							block_matrix_t& left_gauge ,
							block_matrix_t& right_gauge ,
							DmrgParameters& parms );

		std::pair<bool,lambda_t> get_orthogonalized_boundaries( iMPS<Matrix,SymmGroup>& mps ,
																DmrgParameters& parms);

		double compute_fidelity(	const block_matrix<Matrix, SymmGroup>& L_left ,
									const typename iMPS<Matrix,SymmGroup>::lambda_t& lambda_old );

		double measure_fidelity( iMPS<Matrix,SymmGroup>& out_mps );

		void save(	std::string prefix ,
					const iMPS<Matrix, SymmGroup>& mps ,
					const boundary_t& left_boundary ,
					const boundary_t& right_boundary ,
					const DmrgParameters& parms );

		void save_boundaries( 	std::string prefix ,
								const boundary_t& left_boundary ,
								const boundary_t& right_boundary );

		void load_boundaries( 	std::string prefix ,
								boundary_t& left_boundary ,
								boundary_t& right_boundary );

		void load( 	std::string ,
					iMPS<Matrix, SymmGroup>& mps ,
					boundary_t& left_boundary ,
					boundary_t& right_boundary );

		bool restart_from_parameters( DmrgParameters& parms );

	};

} // namespace idmrg
