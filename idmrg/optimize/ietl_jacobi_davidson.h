#pragma once

/** @file
    @brief Header file implementing a wrapper for the jacobi-davidson solver.
*/

#include <complex>

#include "dmrg/utils/BaseParameters.h"

#include "idmrg/optimize/ietl_lanczos_solver.h"
#include "ietl/iteration.h"
#include "ietl/jacobi.h"
#include "ietl/jd.h"

#include "dmrg/block_matrix/block_matrix.h"

/** @fn std::pair<double, MPSTensor<Matrix, SymmGroup> >
                solve_ietl_jcd(SiteProblem<Matrix, SymmGroup> & sp,
                MPSTensor<Matrix, SymmGroup> const & initial,
                BaseParameters & params,
                std::vector<MPSTensor<Matrix, SymmGroup> > ortho_vecs = std::vector<MPSTensor<Matrix, SymmGroup> >() ,
                bool verbose = false )
    @brief function implementing a wrapper for the jacobi-davidson solver.
    @param[in] sp SiteProblem object specifing the properties of the problem Hilbert space
    @param[in] initial MPSTensor to be used as starting vector for the default
    @param[in] params BaseParameters object used for the simulation
    @param[in] ortho_vecs vectors orthogonal to our current solution
    @param[in] verbose use verbose mode
    @pre params shall define the fields: @p ietl_jcd_gmres, @p ietl_jcd_tol, @p ietl_jcd_maxiter
    @return a pair with first_ and second_ members representing the lowest eigenvalue and eigenvector, respectively
*/
template<class Matrix, class SymmGroup>
std::pair<double, MPSTensor<Matrix, SymmGroup> >
solve_ietl_jcd(SiteProblem<Matrix, SymmGroup> & sp,
               MPSTensor<Matrix, SymmGroup> const & initial,
               BaseParameters & params,
               std::vector<MPSTensor<Matrix, SymmGroup> > ortho_vecs = std::vector<MPSTensor<Matrix, SymmGroup> >() ,
			   bool verbose = false )
{
    if (initial.num_elements() <= ortho_vecs.size())
        ortho_vecs.resize(initial.num_elements()-1);
    // Gram-Schmidt the ortho_vecs
    for (int n = 1; n < ortho_vecs.size(); ++n)
        for (int n0 = 0; n0 < n; ++n0)
            ortho_vecs[n] -= ietl::dot(ortho_vecs[n0], ortho_vecs[n])/ietl::dot(ortho_vecs[n0],ortho_vecs[n0])*ortho_vecs[n0];

    typedef MPSTensor<Matrix, SymmGroup> Vector;
    SingleSiteVS<Matrix, SymmGroup> vs(initial, ortho_vecs);

    ietl::jcd_gmres_solver<SiteProblem<Matrix, SymmGroup>, SingleSiteVS<Matrix, SymmGroup> >
    jcd_gmres(sp, vs, params["ietl_jcd_gmres"]);

    ietl::jacobi_davidson<SiteProblem<Matrix, SymmGroup>, SingleSiteVS<Matrix, SymmGroup> >
    jd(sp, vs, ietl::Smallest);

    double tol = params["ietl_jcd_tol"];
    ietl::basic_iteration<double> iter(params["ietl_jcd_maxiter"], tol, tol);

	if( verbose )
	{
		//    maquis::cout << "Ortho vecs " << ortho_vecs.size() << std::endl;
		for (int n = 0; n < ortho_vecs.size(); ++n) {
			//        maquis::cout << "Ortho norm " << n << ": " << ietl::two_norm(ortho_vecs[n]) << std::endl;
			maquis::cout << "Input <MPS|O[" << n << "]> : " << ietl::dot(initial, ortho_vecs[n]) << std::endl;
		}
	}
	std::pair<double, Vector> r0 = jd.calculate_eigenvalue(initial, jcd_gmres, iter);

	if( verbose )
	{
		for (int n = 0; n < ortho_vecs.size(); ++n)
			maquis::cout << "Output <MPS|O[" << n << "]> : " << ietl::dot(r0.second, ortho_vecs[n]) << std::endl;

		maquis::cout << "JCD used " << iter.iterations() << " iterations." << std::endl;
	}

    return r0;
}
