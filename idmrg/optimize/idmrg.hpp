#pragma once

/** @file
    @brief Header file implementing the @p iDMRG struct template.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <boost/shared_ptr.hpp>
#include <complex>
#include <vector>
#include <random>
#include <memory>
#include <stdexcept>

#include <alps/hdf5.hpp>
#include <alps/numeric/matrix.hpp>

#include "dmrg/block_matrix/detail/alps.hpp"
#include "dmrg/block_matrix/symmetry.h"
#include "dmrg/block_matrix/block_matrix.h"
#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "dmrg/models/coded/lattice.hpp"
#include "dmrg/models/model.h"
#include "dmrg/models/generate_mpo.hpp"
#include "dmrg/mp_tensors/contractions.h"
#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/mpo_ops.h"

#include "dmrg/mp_tensors/twositetensor.h"
#include "dmrg/utils/DmrgParameters.h"
#include "dmrg/utils/placement.h"

#include "idmrg/optimize/optimize.h"
#include "idmrg/block_matrix/block_matrix_ietl.hpp"
#include "idmrg/mp_tensors/imps.h"
#include "idmrg/mp_tensors/imps_mpo_ops.h"
#include "idmrg/mp_tensors/impo.h"
#include "idmrg/mp_tensors/ts_ops.h"
#include "idmrg/models/measurements/utils.hpp"
#include "AnasaziBlockKrylovSchurSolMgr.hpp"
#include "AnasaziBasicEigenproblem.hpp"
#include "AnasaziConfigDefs.hpp"
#include "idmrg/lattice/transfer_matrix.h"
#include "idmrg/optimize/ietl_lanczos_solver.h"
#include "idmrg/trilinos/multi_block_matrix.hpp"
#include "idmrg/trilinos/anasazi_multi_block_matrix_traits.hpp"
#include "idmrg/trilinos/anasazi_transfer_matrix_traits.hpp"
#include "idmrg/trilinos/anasazi_block_krylov_schur.hpp"
#include "idmrg/utils/utils.hpp"
#include "idmrg/models/imodel_descriptor.h"
#include "idmrg/exception/exceptions.h"
#include "idmrg/optimize/idmrg.h"

class iLattice;

template<class Matrix , class SymmGroup>
struct TransferMatrix;

namespace idmrg
{
	template<class Matrix,class SymmGroup>
	struct iModelDescriptor;

	using std::cout; using std::endl;
	using std::setw; using std::setprecision;
	using std::scientific; using std::fixed;

	/** @struct iDMRG::Impl
	    @brief struct providing all the functionality required for the implementation of the iDMRG algorithm
	*/
	template<class Matrix, class SymmGroup>
	struct iDMRG<Matrix,SymmGroup>::Impl
	{
		///// Typedefs /////

		/// Tensors ///

		typedef typename iDMRG<Matrix,SymmGroup>::real_type real_type;
		typedef typename iDMRG<Matrix,SymmGroup>::scalar_type scalar_type;

		typedef Matrix matrix_t;
		typedef typename iDMRG<Matrix,SymmGroup>::block_matrix_t block_matrix_t;
		typedef typename iDMRG<Matrix,SymmGroup>::block_diag_matrix_t block_diag_matrix_t;
		typedef typename iDMRG<Matrix,SymmGroup>::lambda_t lambda_t;
		typedef typename iDMRG<Matrix,SymmGroup>::boundary_t boundary_t;
		typedef typename iDMRG<Matrix,SymmGroup>::op_t opt_t;
		typedef typename iDMRG<Matrix,SymmGroup>::real_diag_matrix_t real_diag_matrix_t;

		/// Containers ///

		typedef typename iDMRG<Matrix,SymmGroup>::boundary_cont_t boundary_cont_t;
		typedef typename iDMRG<Matrix,SymmGroup>::mps_tensor_cont_t mps_tensor_cont_t;

		/// Iterators ///

		typedef typename iDMRG<Matrix,SymmGroup>::mps_iterator mps_iterator;
		typedef typename iDMRG<Matrix,SymmGroup>::mps_const_iterator mps_const_iterator;
		typedef typename iDMRG<Matrix,SymmGroup>::mps_const_reverse_iterator mps_const_reverse_iterator;

		typedef typename iDMRG<Matrix,SymmGroup>::mpo_const_iterator mpo_const_iterator;
		typedef typename iDMRG<Matrix,SymmGroup>::mpo_const_reverse_iterator mpo_const_reverse_iterator;

		typedef typename iDMRG<Matrix,SymmGroup>::bound_iterator bound_iterator;
		typedef typename iDMRG<Matrix,SymmGroup>::bound_reverse_iterator bound_reverse_iterator;
		typedef typename iDMRG<Matrix,SymmGroup>::bound_const_iterator bound_const_iterator;
		typedef typename iDMRG<Matrix,SymmGroup>::bound_const_reverse_iterator bound_const_reverse_iterator;

		/// Descriptors ///

		typedef typename iDMRG<Matrix,SymmGroup>::imodel_descriptor_t imodel_descriptor_t;

		/// Others ///

		typedef typename iDMRG<Matrix,SymmGroup>::result_t result_t;
		typedef typename iDMRG<Matrix,SymmGroup>::complex_result_t complex_result_t;

		///// Subclasses /////

		struct Outputer{

			static void print_info( const unsigned system_size , const result_t& result, const double E_delta ) {

				cout << setw(17) << "E =" << setw(15) << setprecision(7) << scientific << result.first << endl;
				cout << setw(17) << "E/L =" << setw(15) << setprecision(7) << scientific << result.first/system_size << endl;
				cout << setw(17) << "ΔE =" << setw(15) << setprecision(7) << scientific << E_delta << "\n" << endl;
			}

			template <class Archive, class T>
			static void store_convergence_data(Archive& ar , std::string name , const T value , DmrgParameters& parms)
			{
				int scale = parms["current_scale"];
				int sweeps = parms["sweeps"];

				std::string path = 	"spectrum/convergence/" + std::to_string(scale) +
									"/iteration/" + std::to_string(sweeps) +
									"/results/" + name + "/mean/value";

				ar[path] << value;
			}

			static void print_info( const unsigned system_size , const iDMRG::result_t& result, const double E_delta , const truncation_results& trunc ) {

				print_info(system_size,result,E_delta);

				cout << setw(17) << "Trunc. weight =" << setw(15) << setprecision(7) << trunc.truncated_weight << fixed << endl;
				cout << setw(17) << "Trunc. fract. =" << setw(15) << setprecision(7) << trunc.truncated_fraction << fixed << endl;
				cout << setw(17) << "Smallest ev. =" << setw(15) << setprecision(7) << trunc.smallest_ev << std::fixed << "\n" << endl;
			}

		};

		///// Methods /////

		Boundary<Matrix, SymmGroup> make_trivial_boundary()
		{
			typename SymmGroup::charge IdCharge = SymmGroup::IdentityCharge;

			Index<SymmGroup> edge;
			edge.insert( std::make_pair( IdCharge , 1 ) );

			Boundary<Matrix, SymmGroup> ret(edge, edge, 1);

			// Here we have a completely trivial boundary
			ret[0][0](0,0) = 1;

			return ret;
		}

		void init_left_boundaries( 	mps_const_iterator it_mps ,
									const mps_const_iterator it_mps_e ,
									mpo_const_iterator it_mpo ,
									bound_iterator it_bound )
		{
			while( it_mps != it_mps_e  )
			{
				*(it_bound+1) = contraction::overlap_mpo_left_step( *it_mps , *it_mps , *it_bound , *it_mpo );

				it_mps++;
				it_mpo++;
				it_bound++;
			}
		}

		void init_right_boundaries( 	mps_const_reverse_iterator it_mps ,
										const mps_const_reverse_iterator it_mps_e ,
										mpo_const_reverse_iterator it_mpo ,
										bound_reverse_iterator it_bound )
		{
			while( it_mps != it_mps_e  )
			{
				*(it_bound+1) = contraction::overlap_mpo_right_step( *it_mps , *it_mps , *it_bound , *it_mpo );

				it_mps++;
				it_mpo++;
				it_bound++;
			}
		}

		void initialize_boundaries(	const iMPS<Matrix,SymmGroup>& mps ,
									const MPO<Matrix,SymmGroup>& mpo ,
									boundary_cont_t& left_boundaries ,
									boundary_cont_t& right_boundaries )
		{
			assert( mps.length() == mpo.length() );
			assert( mps.length() == left_boundaries.size() );
			assert( mps.length() == right_boundaries.size() );

			init_left_boundaries( mps.cbegin() , mps.cend() - 1 , mpo.cbegin() , left_boundaries.begin() );
			init_right_boundaries( mps.crbegin() , mps.crend() - 1 , mpo.crbegin() , right_boundaries.rbegin() );
		}

		void grow_left_boundary(	mps_const_iterator it_mps ,
									const mps_const_iterator it_mps_e ,
									mpo_const_iterator it_mpo ,
									Boundary<Matrix, SymmGroup>& boundary )
		{
			while( it_mps != it_mps_e  )
			{
				boundary = contraction::overlap_mpo_left_step( *it_mps , *it_mps , boundary , *it_mpo );

				it_mps++;
				it_mpo++;
			}
		}

		void grow_right_boundary( 	mps_const_reverse_iterator it_mps ,
									const mps_const_reverse_iterator it_mps_e ,
									mpo_const_reverse_iterator it_mpo ,
									Boundary<Matrix, SymmGroup>& boundary )
		{
			while( it_mps != it_mps_e  )
			{
				boundary = contraction::overlap_mpo_right_step( *it_mps , *it_mps , boundary , *it_mpo );

				it_mps++;
				it_mpo++;
			}
		}

		void grow_boundaries(	const iMPS<Matrix,SymmGroup>& mps ,
														const MPO<Matrix,SymmGroup>& mpo ,
														boundary_cont_t& left_boundaries ,
														boundary_cont_t& right_boundaries )
		{
			grow_left_boundary( mps.cbegin() , mps.cbegin() + mps.center() + 1 , mpo.cbegin() , left_boundaries.front() );
			grow_right_boundary( mps.crbegin() , mps.crbegin() + (mps.length() - mps.center() - 1) , mpo.crbegin() , right_boundaries.back() );
		}

		void shift_unit_cell( const int center , iMPO<Matrix,SymmGroup>& mpo )
		{
			std::rotate( mpo.begin() , mpo.begin() + center + 1 , mpo.end() );
		}

		void shift_unit_cell( const int new_first , const int shift_site , imodel_descriptor_t& model_desc )
		{
			// This method puts the shift site in the center site.
			std::rotate( model_desc.site_type_.begin() , model_desc.site_type_.begin() + new_first , model_desc.site_type_.end() );
			model_desc.shift_site_ = shift_site;
		}

		void shift_unit_cell( iMPS<Matrix,SymmGroup>& mps )
		{
			mps.shift_unit_cell();
		}

		void prepare_guess( iMPS<Matrix,SymmGroup>& mps  , const std::size_t Mmax , const double tol , DmrgParameters& parms )
		{
			mps.absorb_lambda_left();
			mps.absorb_lambda_right();

			mps.shift_unit_cell(); // Will alter the location of the center_
			const std::size_t center = mps.center();

			mps.move_normalization_l2r( 0 , center );
			mps.move_normalization_r2l( mps.length() - 1 , center + 1 );

			lambda_t lambda_old_inv = pinv(mps.lambda_old(),Mmax,tol);
			block_matrix_t t1,t2;

			gemm( mps[center].normalize_left() , lambda_old_inv , t1 );
			gemm( t1 , mps[center+1].normalize_right() , t2 );

			mps[center].multiply_from_right( t2 );
			// Resets lambda matrix to schmidt values
			std::cout << "Exiting state preparation...\n";

			// add noise to predicted state

			double noise = parms["noise"];
			for( int i = 0 ; i < mps.length() ; ++i )
			{
				auto noise_tensor = MPSTensor<Matrix,SymmGroup>( mps[i].site_dim() , mps[i].row_dim() , mps[i].col_dim() , true );
				mps[i] = (1. - noise) * mps[i] + noise * noise_tensor;
			}

			mps.mixed_canonize( Mmax , tol );
			mps.canonize( 0 );

			assert( std::abs(mps.lambda().norm() - 1.) < 1e-14);
		}

		void prepare_random( iMPS<Matrix,SymmGroup>& mps  , const std::size_t Mmax , const double tol )
		{
			mps.shift_unit_cell(); // Will alter the location of the center_
			const std::size_t center = mps.center();

			for( int i = 0 ; i < mps.length() ; ++i )
			{
				mps[i].data().generate(static_cast<dmrg_random::value_type(*)()>(&dmrg_random::uniform));
			}

			mps.mixed_canonize( Mmax , tol );
			mps.canonize( 0 );

			assert( std::abs(mps.lambda().norm() - 1.) < 1e-14);
		}

		bool sweep_right(	iMPS<Matrix,SymmGroup>& mps ,
							iMPO<Matrix,SymmGroup>& mpo ,
							MPO<Matrix,SymmGroup>& cache_mpo ,
							boundary_cont_t& left_boundaries ,
							boundary_cont_t& right_boundaries ,
							DmrgParameters& parms )
		{
			return sweep_right_2site(	mps.begin() ,
								mps.end() ,
								mps.center() ,
								mpo.begin() ,
								cache_mpo.begin() ,
								left_boundaries.begin() ,
								right_boundaries.begin() ,
								parms );
		}

		bool sweep_right_1site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms )
		{
			const std::string resultfile = parms["resultfile"];

			const int M = parms["max_bond_dimension"];
			const double svd_tol = parms["svd_tol"];
			const double delta_energy = parms["delta_energy"];
			bool verbose = parms["verbose"];

			const unsigned current_scale = parms["current_scale"];
			const unsigned UC_size = parms["unit_cell_size"];
			const unsigned system_size = UC_size * (current_scale + 1);

			const int run_length = mps_end - mps - 1;

			double E_old = parms["E_old"];
			double E_delta = parms["E_delta"];
			bool converged = false;
			for( int i = 0 ; i < run_length ; ++i )
			{
				SiteProblem<matrix_t, SymmGroup> site_prob( *(left_boundaries+i) , *(right_boundaries+i) , *(mpo+i) );
				result_t result = solve_ietl_jcd( site_prob , *(mps+i) , parms , mps_tensor_cont_t(0) , verbose );

				*(mps+i) = result.second;

				if( verbose )
				{
					std::cout << "Optimizing site ("  << i << ")\n";
					std::cout << "Delta_E = " << std::setprecision(7) << std::scientific << E_delta << std::endl;
				}

				if( i == center )
				{
					E_delta = std::abs(result.first - E_old)/std::abs(E_old);
					E_old = result.first;

					parms.set("E_delta",E_delta);
					parms.set("E_old",E_old);

					std::cout << "Measuring at site ("  << i <<  ")\n";
					Outputer::print_info(system_size,result,E_delta);

					{
						alps::hdf5::archive ar(resultfile , "w");
						Outputer::store_convergence_data(ar , "energy" , result.first , parms);
					}


					if( E_delta < delta_energy )
					{
						if( verbose )
						{
							std::cout << "Energy converged at site ("  << i << ")\n";
							Outputer::print_info(system_size,result,E_delta);
						}

						converged = true;
					}
				}

				block_matrix_t t = (mps+i)->normalize_left(DefaultSolver());
				(mps+i+1)->multiply_from_left(t);
				*(left_boundaries+i+1) = contraction::overlap_mpo_left_step( *(mps+i) , *(mps+i) , *(left_boundaries+i) , *(mpo+i) );
			}

			return converged;
		}

		bool sweep_right_2site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								mpo_const_iterator cache_mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms )
		{
			const std::string resultfile = parms["resultfile"];

			const int M = parms["max_bond_dimension"];
			const double svd_tol = parms["svd_tol"];
			const double delta_energy = parms["delta_energy"];
			const bool verbose = parms["verbose"];

			const unsigned current_scale = parms["current_scale"];
			const unsigned UC_size = parms["unit_cell_size"];
			const unsigned system_size = UC_size * (current_scale + 1);

			// Modify this line to switch between 1/2-site dmrg
			const int run_length = mps_end - mps - 1;

			double E_old = parms["E_old"];
			double E_delta = parms["E_delta"];
			bool converged = false;
			for( int i = 0 ; i < run_length ; ++i )
			{
				(cache_mpo+i)->placement_l = mpo[i].placement_l;
				(cache_mpo+i)->placement_r = get_right_placement( *(cache_mpo+i) , (mpo+i)->placement_l , (mpo+i+1)->placement_r );

				TwoSiteTensor<matrix_t, SymmGroup> tst( *(mps+i) , *(mps+i+1) );
				MPSTensor<matrix_t, SymmGroup> cache_mps = tst.make_mps();
				SiteProblem<matrix_t, SymmGroup> site_prob( *(left_boundaries+i) , *(right_boundaries+i+1) , *(cache_mpo+i) );

				result_t result = solve_ietl_jcd( site_prob , cache_mps , parms , mps_tensor_cont_t(0) , verbose );

				tst << result.second;

				truncation_results trunc;

				tst.make_both_paired();

				typedef typename alps::numeric::associated_real_diagonal_matrix<Matrix>::type dmt;
				block_matrix<Matrix, SymmGroup> u, v;
				block_matrix<dmt, SymmGroup> s;

				trunc = svd_truncate(tst.data(), u, v, s, svd_tol, M, verbose );

				s *= 1./s.norm();

				*(mps+i) = MPSTensor<Matrix, SymmGroup>((mps+i)->site_dim(), (mps+i)->row_dim(), u.right_basis(), u, LeftPaired);
				gemm(s, v, u);
				*(mps+i+1) = MPSTensor<Matrix, SymmGroup>( (mps+i+1)->site_dim(), u.left_basis(), (mps+i+1)->col_dim(), u, RightPaired);

				if( verbose )
				{
					std::cout << "Optimizing sites ("  << i << "," << i+1 << ")\n";
					Outputer::print_info(system_size,result,E_delta,trunc);
				}

				if( i == center )
				{
					E_delta = std::abs(result.first - E_old)/std::abs(E_old);
					E_old = result.first;

					parms.set("E_delta",E_delta);
					parms.set("E_old",E_old);

					std::cout << "Measuring at sites ("  << i << "," << i+1 << ")\n";
					Outputer::print_info(system_size,result,E_delta,trunc);

					{
						alps::hdf5::archive ar(resultfile , "w");
						Outputer::store_convergence_data(ar , "energy" , result.first , parms);
					}

					if( E_delta < delta_energy )
					{
						if( verbose )
						{
							std::cout << "Energy converged at sites ("  << i << "," << i+1 << ")\n";
							Outputer::print_info(system_size,result,E_delta,trunc);
						}

						converged = true;
					}
				}

				if( i < mps_end - mps - 2 )
				{
					block_matrix_t t = (mps+i+1)->normalize_left(DefaultSolver());
					(mps+i+1+1)->multiply_from_left(t);
					*(left_boundaries+i+1) = contraction::overlap_mpo_left_step( *(mps+i) , *(mps+i) , *(left_boundaries+i) , *(mpo+i) );
				}
			}

			return converged;
		}

		bool sweep_left(	iMPS<Matrix,SymmGroup>& mps ,
							iMPO<Matrix,SymmGroup>& mpo ,
							MPO<Matrix,SymmGroup>& cache_mpo ,
							boundary_cont_t& left_boundaries ,
							boundary_cont_t& right_boundaries ,
							DmrgParameters& parms )
		{
			return sweep_left_2site(	mps.begin() ,
								mps.end() ,
								mps.center() ,
								mpo.begin() ,
								cache_mpo.begin() ,
								left_boundaries.begin() ,
								right_boundaries.begin() ,
								parms );
		}

		bool sweep_left_1site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms )
		{
			const std::string resultfile = parms["resultfile"];

			const int M = parms["max_bond_dimension"];
			const double svd_tol = parms["svd_tol"];
			const double delta_energy = parms["delta_energy"];
			const bool verbose = parms["verbose"];

			const unsigned current_scale = parms["current_scale"];
			const unsigned UC_size = parms["unit_cell_size"];
			const unsigned system_size = UC_size * (current_scale + 1);

			const int run_length = mps_end - mps - 1;

			double E_old = parms["E_old"];
			double E_delta = parms["E_delta"];
			bool converged = false;
			for( int i = run_length ; i > 0 ; --i )
			{
				SiteProblem<matrix_t, SymmGroup> site_prob( *(left_boundaries+i) , *(right_boundaries+i) , *(mpo+i) );
				result_t result = solve_ietl_jcd( site_prob , *(mps+i) , parms , mps_tensor_cont_t(0) , verbose );

				*(mps+i) = result.second;

				if( verbose )
				{
					std::cout << "Optimizing site ("  << i << ")\n";
					Outputer::print_info(system_size,result,E_delta);
				}

				if( i == center )
				{
					E_delta = std::abs(result.first - E_old)/std::abs(E_old);
					E_old = result.first;

					parms.set("E_delta",E_delta);
					parms.set("E_old",E_old);

					std::cout << "Measuring at site ("  << i << ")\n";
					Outputer::print_info(system_size,result,E_delta);

					{
						alps::hdf5::archive ar(resultfile , "w");
						Outputer::store_convergence_data(ar , "energy" , result.first , parms);
					}

					if( E_delta < delta_energy )
					{
						if( verbose )
						{
							std::cout << "Energy converged at site ("  << i << ")\n";
							Outputer::print_info(system_size,result,E_delta);
						}

						converged = true;
					}
				}

				block_matrix_t t = (mps+i)->normalize_right(DefaultSolver());
				(mps+i-1)->multiply_from_right(t);

				// Update right boundaries to include updated tensor
				*(right_boundaries+i-1) = contraction::overlap_mpo_right_step( *(mps+i) , *(mps+i) , *(right_boundaries+i) , *(mpo+i) );
			}
			return converged;
		}

		bool sweep_left_2site(	mps_iterator mps ,
								const mps_const_iterator mps_end ,
								const int center ,
								mpo_const_iterator mpo ,
								mpo_const_iterator cache_mpo ,
								bound_iterator left_boundaries ,
								bound_iterator right_boundaries ,
								DmrgParameters& parms )
		{
			const std::string resultfile = parms["resultfile"];

			const int M = parms["max_bond_dimension"];
			const double svd_tol = parms["svd_tol"];
			const double delta_energy = parms["delta_energy"];
			const bool verbose = parms["verbose"];

			const unsigned current_scale = parms["current_scale"];
			const unsigned UC_size = parms["unit_cell_size"];
			const unsigned system_size = UC_size * (current_scale + 1);

			const int run_length = mps_end - mps - 1;

			double E_old = parms["E_old"];
			double E_delta = parms["E_delta"];
			bool converged = false;
			for( int i = run_length - 1 ; i >= 0 ; --i )
			{
				(cache_mpo+i)->placement_l = get_left_placement( *(cache_mpo+i) , (mpo+i)->placement_l , (mpo+i+1)->placement_r );
				(cache_mpo+i)->placement_r = (mpo+i+1)->placement_r;

				TwoSiteTensor<matrix_t, SymmGroup> tst( *(mps+i) , *(mps+i+1) );
				MPSTensor<matrix_t, SymmGroup> cache_mps = tst.make_mps();
				SiteProblem<matrix_t, SymmGroup> site_prob( *(left_boundaries+i) , *(right_boundaries+i+1) , *(cache_mpo+i) );

				result_t result = solve_ietl_jcd( site_prob , cache_mps , parms , mps_tensor_cont_t(0) , verbose );

				// Update tensors, recover MPS form and shift normalization
				tst << result.second;

				truncation_results trunc;

				tst.make_both_paired();

				typedef typename alps::numeric::associated_real_diagonal_matrix<Matrix>::type dmt;
				block_matrix<Matrix, SymmGroup> u, v;
				block_matrix<dmt, SymmGroup> s;

				trunc = svd_truncate(tst.data(), u, v, s, svd_tol, M, verbose );

				s *= 1./s.norm();

				*(mps+i+1) = MPSTensor<Matrix, SymmGroup>( (mps+i+1)->site_dim(), v.left_basis(), (mps+i+1)->col_dim(), v, RightPaired);
				gemm(u, s, v);
				*(mps+i) = MPSTensor<Matrix, SymmGroup>((mps+i)->site_dim(), (mps+i)->row_dim(), u.right_basis(), v, LeftPaired);

				if( verbose )
				{
					std::cout << "Optimizing sites ("  << i << "," << i+1 << ")\n";
					Outputer::print_info(system_size,result,E_delta,trunc);
				}

				if( i == center )
				{
					E_delta = std::abs(result.first - E_old)/std::abs(E_old);
					E_old = result.first;

					parms.set("E_delta",E_delta);
					parms.set("E_old",E_old);

					std::cout << "Measuring at sites ("  << i << "," << i+1 << ")\n";
					Outputer::print_info(system_size,result,E_delta,trunc);

					{
						alps::hdf5::archive ar(resultfile , "w");
						Outputer::store_convergence_data(ar , "energy" , result.first , parms);
					}

					if( E_delta < delta_energy )
					{
						if( verbose )
						{
							std::cout << "Energy converged at sites ("  << i << "," << i+1 << ")\n";
							Outputer::print_info(system_size,result,E_delta,trunc);
						}

						converged = true;
					}
				}

				if( i > 0 )
				{
					block_matrix_t t = (mps+i)->normalize_right(DefaultSolver());
					(mps+i-1)->multiply_from_right(t);

					// Update right boundaries to include updated tensor
					*(right_boundaries+i) = contraction::overlap_mpo_right_step( *(mps+i+1) , *(mps+i+1) , *(right_boundaries+i+1) , *(mpo+i+1) );
				}
			}
			return converged;
		}

		bool sweep_space(	iMPS<Matrix,SymmGroup>& mps ,
							iMPO<Matrix,SymmGroup>& mpo ,
							MPO<Matrix,SymmGroup>& cache_mpo ,
							boundary_cont_t& left_boundaries ,
							boundary_cont_t& right_boundaries ,
							DmrgParameters& parms )
		{
			return sweep_space(	mps.begin() ,
								mps.end() ,
								mps.center() ,
								mpo.begin() ,
								cache_mpo.begin() ,
								left_boundaries.begin() ,
								right_boundaries.begin() ,
								parms );
		}

		bool sweep_space(	mps_iterator mps ,
							const mps_const_iterator mps_end ,
							const int center ,
							mpo_const_iterator mpo ,
							mpo_const_iterator cache_mpo ,
							bound_iterator left_boundaries ,
							bound_iterator right_boundaries ,
							DmrgParameters& parms )
		{
			const int max_sweeps = parms["max_sweeps"];
			const int min_sweeps = parms["min_sweeps"];
			const bool verbose = parms["verbose"];

			std::string opt = parms["curr_optimization"];
			bool ss_opt = opt == std::string("singlesite");

			parms.set("E_old",1.);
			parms.set("E_delta",1.);

			std::size_t sweeps = parms["sweeps"];
			bool converged = false;
			for(  ; sweeps < max_sweeps ; ++sweeps )
			{
				if( verbose )
				{
					std::cout << "\n\n";
					std::cout << "Sweeping right..." << "\n\n";
				}

				converged = ss_opt ? 	sweep_right_1site(	mps ,
															mps_end ,
															center ,
															mpo ,
															left_boundaries ,
															right_boundaries ,
															parms ) :
										sweep_right_2site(	mps ,
															mps_end ,
															center ,
															mpo ,
															cache_mpo ,
															left_boundaries ,
															right_boundaries ,
															parms );

				if( verbose )
				{
					std::cout << "\n\n";
					std::cout << "Sweeping left..." << "\n\n";
				}

				converged = ss_opt ? 	sweep_left_1site(	mps ,
															mps_end ,
															center ,
															mpo ,
															left_boundaries ,
															right_boundaries ,
															parms ) :
										sweep_left_2site(	mps ,
															mps_end ,
															center ,
															mpo ,
															cache_mpo ,
															left_boundaries ,
															right_boundaries ,
															parms );

				parms.set("sweeps" , sweeps+1);

				if( converged && (sweeps > min_sweeps))
					break;
			}

			return converged;
		}

		std::size_t sweep_scales( 	iMPS<Matrix,SymmGroup>& mps ,
									iMPO<Matrix,SymmGroup>& mpo ,
									boundary_cont_t& left_boundaries ,
									boundary_cont_t& right_boundaries ,
									iLattice& ilattice ,
									imodel_descriptor_t& model_desc ,
									DmrgParameters& parms )
		{
			using MPO = MPO<Matrix,SymmGroup>;

			const std::size_t min_scales = parms["min_scales"];
			const std::size_t max_scales = parms["max_scales"];

			const std::size_t init_bond_dimension = parms["init_bond_dimension"];
			const std::size_t max_bond_dimension = parms["max_bond_dimension"];
			const int ngrowscales = parms["ngrowscales"];
			const int bd_step = (max_bond_dimension - init_bond_dimension + 1)/ngrowscales;
			const std::string opt_mode = parms["optimization"];

			const double pinv_tol = parms["pinv_tol"];
			const double fidelity_tol = parms["fidelity_tol"];
			const std::size_t UC_size = parms["unit_cell_size"];
			bool predict_state = parms["predict_state"];
			bool fix_boundary_mpos = parms["fix_boundary_mpos"];

			std::string resultfile = parms["resultfile"];
			std::string chkpfile = parms["chkpfile"];

			init_right_boundaries( mps.crbegin() , mps.crend() - 1 , mpo.crbegin() , right_boundaries.rbegin() );

			MPO cache_mpo;
			make_ts_cache_mpo( mpo , cache_mpo , mps );

			bool converged = false;
			std::size_t scale = parms["current_scale"];
			while( scale < max_scales )
			{
				std::size_t M;
				if( scale < ngrowscales )
				{
					M = init_bond_dimension + bd_step * scale;
					std::cout << "Setting M = " << M << std::endl;
				}
				else if( (scale >= ngrowscales) && (M != max_bond_dimension) && (parms["curr_optimization"] != opt_mode))
				{
					M = max_bond_dimension;

					parms.set("curr_optimization" , opt_mode);

					std::cout << "Setting M to " << M << std::endl;
					std::cout << "Setting optimization mode to " << opt_mode << "." << std::endl;
				}

				std::cout << "Working on scale " << scale << "\n\n";
				parms.set("system_size", UC_size * (scale + 1) );

				std::cout << "Starting space iterations...\n\n";
				sweep_space(mps , mpo , cache_mpo , left_boundaries , right_boundaries , parms );
				std::cout << "Exiting space iterations...\n\n";

				/// Monitor convergence ///
				const double fidelity = measure_fidelity( mps );
				{
					alps::hdf5::archive ar(resultfile , "w");
					Outputer::store_convergence_data(ar , "fidelity" , fidelity , parms);
				}

				if( (fidelity > 1. - fidelity_tol) && scale > min_scales )
				{
					std::cout << "Fidelity has converged (F = " << fidelity << ")\nExiting scale iterations after " << scale << " iterations.\nFinal fidelity was " << fidelity << '\n';
					converged = true;
				}

				int meas_freq = parms["convergence_measure_every"];
				if( (meas_freq > 0) && (scale % meas_freq == 0) )
				{
					lambda_t lambda_inverse;
					bool measure_ready;
					std::tie(measure_ready,lambda_inverse) = get_orthogonalized_boundaries(mps,parms);

					mps.save(chkpfile);

					parms.set("convergence_measure_step",scale/meas_freq);

					if( measure_ready )
						imeasurement::measure( lambda_inverse , mps , ilattice , model_desc , parms , true );
				}

				scale++;
				parms.set("sweeps",0);

				if( scale < max_scales && !converged )
				{
					mps.mixed_canonize( M , pinv_tol );
					lambda_t lambda_temp = mps.lambda();

					// Grow system size by absorbing left and right normalized tensors into left and right edge boundaries, respectively.
					grow_boundaries( mps , mpo , left_boundaries , right_boundaries );

					if( fix_boundary_mpos )
					{
						//mpo.match_edge_placements();
						mpo.set_edges();
						fix_boundary_mpos = false;
						parms.set("fix_boundary_mpos",fix_boundary_mpos);
					}
					shift_unit_cell( mps.center() , mpo ); // mpo.swap_unit_cell();

					//construct_placements( mpo );
					// Here we perform state prediction as described in Schollwöck's paper (arXiv:1008.3477).
					// This is optional and a random MPS can also be used.

					if( predict_state )
						prepare_guess( mps , M , pinv_tol , parms ); // mps exits right normalized and with proper lambda matrix
					else
						prepare_random( mps , M , pinv_tol );

					swap( mps.lambda_old() , lambda_temp );

					// Prepare grown boundaries for next scale
					initialize_boundaries( mps , mpo , left_boundaries , right_boundaries );
					make_ts_cache_mpo( mpo , cache_mpo , mps );

					parms.set("current_scale" , scale);
				}
				else
				{
					mps.canonize(0);
				}

				save( chkpfile , mps , left_boundaries.front() , right_boundaries.back() , parms );

				if( converged )
					break;

			} // Scales

			return scale;
		}

		double measure_fidelity( iMPS<Matrix,SymmGroup>& mps )
		{
			mps[0].make_right_paired();
			block_matrix<Matrix, SymmGroup> L , Q;
			lq( mps[0].data() , L , Q );

			return compute_fidelity( L , mps.lambda_old() );
		}

		void orthogonalize_left(	const iMPS<Matrix,SymmGroup>& mps ,
									block_matrix_t& left_gauge ,
									const lambda_t& lambda_inv ,
									DmrgParameters& parms )
		{
			// initialize guess for edge eigenvector
			typedef std::vector< block_matrix<Matrix, SymmGroup> > bm_cont_t;
			typedef typename TransferMatrix<Matrix,SymmGroup>::vector_t vector_t;
			typedef typename iMPS<Matrix, SymmGroup>::scalar_type scalar_type;
			typedef typename maquis::traits::real_type<Matrix>::type real_type;

			typedef TransferMatrix<Matrix,SymmGroup> TransferMatrix;
			typedef Anasazi::MultiBlockMatrix<Matrix,SymmGroup> MultiBlockMatrix;

			bool verbose = parms["verbose"];

			//assert( mps.is_mixed_canonical() );

			if(verbose) std::cout << "Orthogonalizing left\n";

			Teuchos::RCP<TransferMatrix> transfer_matrix = Teuchos::rcp( new TransferMatrix( mps , lambda_inv , TransferMatrix::Left ) );
			Teuchos::RCP<MultiBlockMatrix> start = Teuchos::rcp ( new MultiBlockMatrix( block_matrix_t( mps[0].row_dim() , mps[0].row_dim() ) ) );
			start->generate();

			complex_result_t result = trilinos_block_krylov_schur( transfer_matrix , start );

			scalar_type phase = result.second[0](0,0) / std::abs(result.second[0](0,0));
			if(verbose) std::cout << "Removing phase: " << phase <<'\n';

			result.second /= phase;

			swap( left_gauge , result.second );

			result.second = left_gauge;
			result.second.adjoint_inplace();
			left_gauge += result.second;
			left_gauge *= 0.5;

			result.second -= left_gauge;
			if(verbose) std::cout << "Left gauge relative distance from hermitian matrix: " << result.second.norm() / left_gauge.norm() << std::endl;

			typedef typename alps::numeric::associated_real_diagonal_matrix<Matrix>::type real_diag_mat_t;
			block_matrix<real_diag_mat_t, SymmGroup> evals;

			// Now we need the sqrt of the leading eigenvector as a matrix
			heev( left_gauge , result.second , evals );

			typename real_diag_mat_t::diagonal_iterator it,ite;

			for( int k = 0 ; k < evals.n_blocks() ; ++k )
			{
				boost::tie(it,ite) = evals[k].diagonal();

				while( it != ite )
		        {
					*it = std::sqrt( std::abs( *it ) );
					it++;
		        }
			}

			gemm( result.second , evals , left_gauge );

			left_gauge.adjoint_inplace();
		}

		void orthogonalize_right( 	const iMPS<Matrix,SymmGroup>& mps ,
									block_matrix_t& right_gauge ,
									const lambda_t& lambda_inv ,
									DmrgParameters& parms )
		{
			typedef std::vector< block_matrix<Matrix, SymmGroup> > bm_cont_t;
			typedef typename TransferMatrix<Matrix,SymmGroup>::vector_t vector_t;

			typedef TransferMatrix<Matrix,SymmGroup> TransferMatrix;
			typedef Anasazi::MultiBlockMatrix<Matrix,SymmGroup> MultiBlockMatrix;

			bool verbose = parms["verbose"];

			if(verbose) std::cout << "Orthogonalizing right\n";

			Teuchos::RCP<TransferMatrix> transfer_matrix = Teuchos::rcp( new TransferMatrix( mps , lambda_inv , TransferMatrix::Right ) );
			Teuchos::RCP<MultiBlockMatrix> start = Teuchos::rcp ( new MultiBlockMatrix( block_matrix_t( mps[0].row_dim() , mps[0].row_dim() ) ) );
			start->generate();

			complex_result_t result = trilinos_block_krylov_schur( transfer_matrix , start );

			// We need to remove an overall phase that might arise in the complex case
			// Get it from the upper-left most entry
			scalar_type phase = result.second[0](0,0) / std::abs(result.second[0](0,0));
			if(verbose) std::cout << "Removing phase: " << phase <<'\n';

			result.second /= phase;

			swap( right_gauge , result.second );
			result.second = right_gauge;
			result.second.adjoint_inplace();
			right_gauge += result.second;
			right_gauge *= 0.5;

			result.second -= right_gauge;
			if(verbose) std::cout << "Right gauge relative distance from hermitian matrix: " << result.second.norm() / right_gauge.norm() << std::endl;

			typedef typename alps::numeric::associated_real_diagonal_matrix<Matrix>::type real_diag_mat_t;
			block_matrix<real_diag_mat_t, SymmGroup> evals;

			// Now we need the sqrt of the leading eigenvector as a matrix
			heev( right_gauge , result.second , evals );

			typename real_diag_mat_t::diagonal_iterator it,ite;

			for( int k = 0 ; k < evals.n_blocks() ; ++k )
			{
				boost::tie(it,ite) = evals[k].diagonal();

				while( it != ite )
		        {
					*it = std::sqrt( std::abs( *it ) );
		            it++;
		        }
			}

			gemm( result.second , evals , right_gauge );

			right_gauge.conjugate_inplace();
		}

		void orthogonalize( const iMPS<Matrix,SymmGroup>& mps ,
							const lambda_t& lambda_inv ,
							block_matrix_t& left_gauge ,
							block_matrix_t& right_gauge ,
							DmrgParameters& parms )
		{
			bool verbose = parms["verbose"];

			try{
				orthogonalize_left( mps , left_gauge , lambda_inv , parms );
				orthogonalize_right( mps , right_gauge , lambda_inv , parms );
			}
			catch( std::invalid_argument& e)
			{
				if(verbose){
					std::cout << "Oops, it might be that your MPS has m <= 2.\n";
					std::cout << "Trying to proceed under the assumption of a trivial bond dimension.\n";
				}

				auto edge_charges = mps[0].row_dim().charges();

				if( edge_charges.size() == 1 )
				{


					int block_size = mps[0].row_dim().size_of_block( edge_charges[0] , false );

					if(verbose){
						std::cout << "Charge found: " << edge_charges[0] << '\n';
						std::cout << "Row dim found: " << mps[0].row_dim() << '\n';
					}

					if( block_size == 1 )
					{
						if(verbose) std::cout << "Succeeded. Carrying on...\n";
						real_diag_matrix_t mid = real_diag_matrix_t::identity_matrix(1);

						left_gauge = block_matrix_t( mps.front().row_dim() , mps.front().row_dim() );
						right_gauge = block_matrix_t( mps.back().col_dim() , mps.back().col_dim() );

						left_gauge[0](0,0) = 1;
						right_gauge[0](0,0) = 1;
					}
					else
					{
						std::cerr << "Cannot proceed assuming trivial bond dimension.\nHere is the original exception \n" + std::string(e.what()) << std::endl;

						throw std::runtime_error("Skipping measurements.");
					}
				}
				else
				{
					std::cerr << "Cannot proceed assuming trivial bond dimension.\nSkipping measurements.\nHere is the original exception \n" + std::string(e.what()) << std::endl;

					throw std::runtime_error("Skipping measurements.");
				}
			}
		}

		std::pair<bool , lambda_t>
		get_orthogonalized_boundaries( iMPS<Matrix,SymmGroup>& mps ,
										DmrgParameters& parms )
		{
			const size_t M = 		parms["max_bond_dimension"];
			const double cutoff = 	parms["pinv_tol"];

			lambda_t lambda_inverse = pinv( mps.lambda_old() , M , cutoff );

			bool measure_ready = true;
			try{
				block_matrix_t left_gauge,right_gauge;
				orthogonalize( mps , lambda_inverse , left_gauge , right_gauge , parms );
				mps.start_boundaries( left_gauge , right_gauge );
			}
			catch( std::runtime_error ){
				std::cout << "Orthogonalization failed.\nSkipping measurements." << std::endl;
				measure_ready = false;
			}

			return std::make_pair(measure_ready,lambda_inverse);
		}

		double compute_fidelity(	const block_matrix<Matrix, SymmGroup>& L_left ,
									const typename iMPS<Matrix,SymmGroup>::lambda_t& lambda_old )
		{
			typedef typename alps::numeric::associated_real_diagonal_matrix<Matrix>::type diag_mat_t;
			typedef block_matrix<diag_mat_t,SymmGroup> real_diagonal_block_diag_mat_t;

			block_matrix_t overlap_matrix,dummy1,dummy2;
			gemm( L_left , lambda_old , overlap_matrix );

			real_diagonal_block_diag_mat_t S;
			svd( overlap_matrix , dummy1 , dummy2 , S );

			return S.trace();
		}

		void save_boundaries( 	std::string prefix ,
								const boundary_t& left_boundary ,
								const boundary_t& right_boundary )
		{
			std::string fname = prefix + "/imps_mpo_boundaries.h5";
			alps::hdf5::archive ar(fname,"w");

			ar["/left_boundary"] << left_boundary;
			ar["/right_boundary"] << right_boundary;
		}

		void save( 	std::string root_path ,
					const iMPS<Matrix, SymmGroup>& mps ,
					const boundary_t& left_boundary ,
					const boundary_t& right_boundary ,
					const DmrgParameters& parms )
		{
			if(!boost::filesystem::exists(root_path))
			{
				std::cout << root_path << std::endl;
				boost::filesystem::create_directory(root_path);
			}

			mps.save( root_path );

			{
				alps::hdf5::archive ar( root_path + "/parameters.h5" , "w");
				ar["/parameters"] << parms;
			}

			save_boundaries( root_path , left_boundary , right_boundary );
		}

		void load_boundaries( 	std::string prefix ,
								boundary_t& left_boundary ,
								boundary_t& right_boundary )
		{
			std::string fname = prefix + "/imps_mpo_boundaries.h5";
			alps::hdf5::archive ar(fname);

			ar["/left_boundary"] >> left_boundary;
			ar["/right_boundary"] >> right_boundary;
		}

		void load( 	std::string prefix ,
					iMPS<Matrix, SymmGroup>& mps ,
					boundary_t& left_boundary ,
					boundary_t& right_boundary )
		{
			mps.load( prefix );
			load_boundaries( prefix , left_boundary , right_boundary );
		}

		bool restart_from_parameters( DmrgParameters& parms )
		{
			/************** CHECK!!!!!!*/
			bool continue_optimization = parms.defined("loadfile");

			if( continue_optimization )
			{
				std::string prefix = parms["loadfile"];
				std::string fname = prefix + "/parameters.h5";

				assert( boost::filesystem::exists(fname) );

				storage::archive ar(fname);

				DmrgParameters old_parms;
				ar["/parameters"] >> old_parms;

				bool continue_simulation = old_parms["continue_simulation"];
				if( !continue_simulation )
				{
					// User needs to request extension explicitly
					if( !(parms.defined("continue_simulation") && bool(parms["continue_simulation"])) )
						throw SimulationFinished();
				}

				size_t current_scale = old_parms["current_scale"];
				parms.set("current_scale" , current_scale);
			}

			return continue_optimization;
		}
	};

	///// iDMRG (forwarding) definitions /////

	template <class M, class SG>
	iDMRG<M,SG>::iDMRG():
	pimpl_(new iDMRG<M,SG>::Impl())
	{}

	template <class M, class SG>
	iDMRG<M,SG>::~iDMRG() = default;

	template<class M, class SG>
	Boundary<M, SG> iDMRG<M,SG>::make_trivial_boundary()
	{
		return pimpl_->make_trivial_boundary();
	}

	template<class M, class SG>
	void iDMRG<M,SG>::init_left_boundaries(	typename iDMRG<M,SG>::mps_const_iterator it_mps ,
														const typename iDMRG<M,SG>::mps_const_iterator it_mps_e ,
														typename iDMRG<M,SG>::mpo_const_iterator it_mpo ,
														typename iDMRG<M,SG>::bound_iterator it_bound )
	{
		pimpl_->init_left_boundaries( it_mps , it_mps_e , it_mpo , it_bound );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::init_right_boundaries( typename iDMRG<M,SG>::mps_const_reverse_iterator it_mps ,
								const typename iDMRG<M,SG>::mps_const_reverse_iterator it_mps_e ,
								typename iDMRG<M,SG>::mpo_const_reverse_iterator it_mpo ,
								typename iDMRG<M,SG>::bound_reverse_iterator it_bound )
	{
		pimpl_->init_right_boundaries( it_mps , it_mps_e , it_mpo , it_bound );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::initialize_boundaries(	const iMPS<M,SG>& mps ,
								const MPO<M,SG>& mpo ,
								typename iDMRG<M,SG>::boundary_cont_t& left_boundaries ,
								typename iDMRG<M,SG>::boundary_cont_t& right_boundaries )
	{
		pimpl_->initialize_boundaries( mps , mpo , left_boundaries , right_boundaries );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::grow_left_boundary(	typename iDMRG<M,SG>::mps_const_iterator it ,
								const typename iDMRG<M,SG>::mps_const_iterator it_e ,
								typename iDMRG<M,SG>::mpo_const_iterator it_mpo ,
								Boundary<M, SG>& boundary )
	{
		pimpl_->grow_left_boundary( it , it_e , it_mpo , boundary );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::grow_right_boundary( 	typename iDMRG<M,SG>::mps_const_reverse_iterator it ,
								const typename iDMRG<M,SG>::mps_const_reverse_iterator it_e ,
								typename iDMRG<M,SG>::mpo_const_reverse_iterator it_mpo ,
								Boundary<M, SG>& boundary )
	{
		pimpl_->grow_right_boundary( it , it_e , it_mpo , boundary );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::grow_boundaries(	const iMPS<M,SG>& mps ,
							const MPO<M,SG>& mpo ,
							typename iDMRG<M,SG>::boundary_cont_t& left_boundaries ,
							typename iDMRG<M,SG>::boundary_cont_t& right_boundaries )
	{
		pimpl_->grow_boundaries( mps , mpo , left_boundaries , right_boundaries );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::shift_unit_cell( const int center , iMPO<M,SG>& mpo )
	{
		pimpl_->shift_unit_cell( center , mpo );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::shift_unit_cell( const int center ,
		const int shift_site ,
		typename iDMRG<M,SG>::imodel_descriptor_t& model_desc )
	{
		pimpl_->shift_unit_cell( center , shift_site , model_desc );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::shift_unit_cell( iMPS<M,SG>& mps )
	{
		pimpl_->shift_unit_cell( mps );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::prepare_guess( iMPS<M,SG>& mps  ,
		const std::size_t Mmax ,
		const double tol ,
		DmrgParameters& parms )
	{
		pimpl_->prepare_guess( mps , Mmax , tol , parms );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::prepare_random( iMPS<M,SG>& mps  ,
		const std::size_t Mmax ,
		const double tol )
	{
		pimpl_->prepare_random( mps , Mmax , tol );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_right(	iMPS<M,SG>& mps ,
						iMPO<M,SG>& mpo ,
						MPO<M,SG>& cache_mpo ,
						typename iDMRG<M,SG>::boundary_cont_t& left_boundaries ,
						typename iDMRG<M,SG>::boundary_cont_t& right_boundaries ,
						DmrgParameters& parms )
	{
		return pimpl_->sweep_right(	mps , mpo , cache_mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_right_2site(	typename iDMRG<M,SG>::mps_iterator mps ,
							const typename iDMRG<M,SG>::mps_const_iterator mps_end ,
							const int center ,
							typename iDMRG<M,SG>::mpo_const_iterator mpo ,
							typename iDMRG<M,SG>::mpo_const_iterator cache_mpo ,
							typename iDMRG<M,SG>::bound_iterator left_boundaries ,
							typename iDMRG<M,SG>::bound_iterator right_boundaries ,
							DmrgParameters& parms )
	{
		return pimpl_->sweep_right_2site( mps , mps_end , center , mpo , cache_mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_right_1site(	typename iDMRG<M,SG>::mps_iterator mps ,
							const typename iDMRG<M,SG>::mps_const_iterator mps_end ,
							const int center ,
							typename iDMRG<M,SG>::mpo_const_iterator mpo ,
							typename iDMRG<M,SG>::bound_iterator left_boundaries ,
							typename iDMRG<M,SG>::bound_iterator right_boundaries ,
							DmrgParameters& parms )
	{
		return pimpl_->sweep_right_1site( mps , mps_end , center , mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_left(	iMPS<M,SG>& mps ,
						iMPO<M,SG>& mpo ,
						MPO<M,SG>& cache_mpo ,
						typename iDMRG<M,SG>::boundary_cont_t& left_boundaries ,
						typename iDMRG<M,SG>::boundary_cont_t& right_boundaries ,
						DmrgParameters& parms )
	{
		return pimpl_->sweep_left( mps , mpo , cache_mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_left_2site(	typename iDMRG<M,SG>::mps_iterator mps ,
							const typename iDMRG<M,SG>::mps_const_iterator mps_end ,
							const int center ,
							typename iDMRG<M,SG>::mpo_const_iterator mpo ,
							typename iDMRG<M,SG>::mpo_const_iterator cache_mpo ,
							typename iDMRG<M,SG>::bound_iterator left_boundaries ,
							typename iDMRG<M,SG>::bound_iterator right_boundaries ,
							DmrgParameters& parms )
	{
		return pimpl_->sweep_left_2site( mps , mps_end , center , mpo , cache_mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_left_1site(	typename iDMRG<M,SG>::mps_iterator mps ,
							const typename iDMRG<M,SG>::mps_const_iterator mps_end ,
							const int center ,
							typename iDMRG<M,SG>::mpo_const_iterator mpo ,
							typename iDMRG<M,SG>::bound_iterator left_boundaries ,
							typename iDMRG<M,SG>::bound_iterator right_boundaries ,
							DmrgParameters& parms )
	{
		return pimpl_->sweep_left_1site( mps , mps_end , center , mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_space(	iMPS<M,SG>& mps ,
						iMPO<M,SG>& mpo ,
						MPO<M,SG>& cache_mpo ,
						typename iDMRG<M,SG>::boundary_cont_t& left_boundaries ,
						typename iDMRG<M,SG>::boundary_cont_t& right_boundaries ,
						DmrgParameters& parms )
	{
		return pimpl_->sweep_space( mps , mpo , cache_mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::sweep_space(	typename iDMRG<M,SG>::mps_iterator mps ,
						const typename iDMRG<M,SG>::mps_const_iterator mps_end ,
						const int center ,
						typename iDMRG<M,SG>::mpo_const_iterator mpo ,
						typename iDMRG<M,SG>::mpo_const_iterator cache_mpo ,
						typename iDMRG<M,SG>::bound_iterator left_boundaries ,
						typename iDMRG<M,SG>::bound_iterator right_boundaries ,
						DmrgParameters& parms )
	{
		return pimpl_->sweep_space( mps , mps_end , center , mpo , cache_mpo , left_boundaries , right_boundaries , parms );
	}

	template<class M, class SG>
	std::size_t iDMRG<M,SG>::sweep_scales(	iMPS<M,SG>& mps ,
								iMPO<M,SG>& mpo ,
								typename iDMRG<M,SG>::boundary_cont_t& left_boundaries ,
								typename iDMRG<M,SG>::boundary_cont_t& right_boundaries ,
								iLattice& ilattice,
								typename iDMRG<M,SG>::imodel_descriptor_t& model_desc ,
								DmrgParameters& parms )
	{
		return pimpl_->sweep_scales( mps , mpo , left_boundaries , right_boundaries , ilattice, model_desc , parms );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::orthogonalize_left( 	const iMPS<M,SG>& mps ,
								typename iDMRG<M,SG>::block_matrix_t& left_gauge ,
								const typename iDMRG<M,SG>::lambda_t& lambda_inv ,
								DmrgParameters& parms )
	{
		pimpl_->orthogonalize_left( mps , left_gauge , lambda_inv , parms );
	}

	template<class M , class SG>
	void iDMRG<M,SG>::orthogonalize_right( 	const iMPS<M,SG>& mps ,
								typename iDMRG<M,SG>::block_matrix_t& right_gauge ,
								const typename iDMRG<M,SG>::lambda_t& lambda_inv ,
								DmrgParameters& parms )
	{
		pimpl_->orthogonalize_right( mps , right_gauge , lambda_inv , parms );
	}

	template<class M , class SG>
	void iDMRG<M,SG>::orthogonalize(	const iMPS<M,SG>& mps ,
						const typename iDMRG<M,SG>::lambda_t& lambda_inv ,
						typename iDMRG<M,SG>::block_matrix_t& left_gauge ,
						typename iDMRG<M,SG>::block_matrix_t& right_gauge ,
						DmrgParameters& parms )
	{
		pimpl_->orthogonalize( mps , lambda_inv , left_gauge , right_gauge , parms );
	}

	template<class M, class SG>
	std::pair<bool,typename iMPS<M,SG>::lambda_t> iDMRG<M,SG>::get_orthogonalized_boundaries( iMPS<M,SG>& mps ,
															DmrgParameters& parms)
	{
		return pimpl_->get_orthogonalized_boundaries( mps , parms );
	}

	template<class M, class SG>
	double iDMRG<M,SG>::compute_fidelity(	const block_matrix<M,SG>& L_left ,
											const typename iDMRG<M,SG>::lambda_t& lambda_old )
	{
		return pimpl_->compute_fidelity( L_left , lambda_old );
	}

	template<class M, class SG>
	double iDMRG<M,SG>::measure_fidelity( iMPS<M,SG>& out_mps )
	{
		return pimpl_->measure_fidelity( out_mps );
	}

	/*
	template<class M, class SG>
	double iDMRG<M,SG>::get_imps_norm( 	const typename iDMRG<M,SG>::lambda_t& inverse_lambda ,
							const iMPS<M, SG>& mps ,
							const std::vector< std::pair< std::size_t , block_matrix<M,SG> > >& list )
	{
		return pimpl_->get_imps_norm( inverse_lambda , mps , list );
	}

	template<class M, class SG>
	double iDMRG<M,SG>::measure( const typename iMPS<M,SG>::lambda_t& inverse_lambda ,
					const iMPS<M, SG>& mps ,
					const std::vector< std::pair< std::size_t , block_matrix<M,SG> > >& list )
	{
		return pimpl_->measure( inverse_lambda , mps , list );
	}

	*/

	template<class M, class SG>
	void iDMRG<M,SG>::save(	std::string prefix ,
				const iMPS<M, SG>& mps ,
				const typename iDMRG<M,SG>::boundary_t& left_boundary ,
				const typename iDMRG<M,SG>::boundary_t& right_boundary ,
				const DmrgParameters& parms )
	{
		pimpl_->save( prefix , mps , left_boundary , right_boundary , parms );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::save_boundaries( 	std::string prefix ,
							const typename iDMRG<M,SG>::boundary_t& left_boundary ,
							const typename iDMRG<M,SG>::boundary_t& right_boundary )
	{
		pimpl_->save_boundaries( prefix , left_boundary , right_boundary );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::load_boundaries( 	std::string prefix ,
							typename iDMRG<M,SG>::boundary_t& left_boundary ,
							typename iDMRG<M,SG>::boundary_t& right_boundary )
	{
		pimpl_->load_boundaries( prefix , left_boundary , right_boundary );
	}

	template<class M, class SG>
	void iDMRG<M,SG>::load( std::string prefix ,
				iMPS<M, SG>& mps ,
				typename iDMRG<M,SG>::boundary_t& left_boundary ,
				typename iDMRG<M,SG>::boundary_t& right_boundary )
	{
		pimpl_->load( prefix , mps , left_boundary , right_boundary );
	}

	template<class M, class SG>
	bool iDMRG<M,SG>::restart_from_parameters( DmrgParameters& parms )
	{
		return pimpl_->restart_from_parameters( parms );
	}

} // namespace idmrg
