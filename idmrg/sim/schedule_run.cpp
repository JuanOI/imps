#include "idmrg/libpscan/scheduler.hpp"

#include <alps/utility/copyright.hpp>
#include <iostream>

//#include "dmrg/version.h"

int main(int argc, char ** argv)
{
    try {
        /*
        std::cout << "ALPS/MPS version " DMRG_VERSION_STRING " (2013-2014)\n"
                  << "  Density Matrix Renormalization Group algorithm\n"
                  << "  available from http://alps.comp-phys.org/\n"
                  << "  copyright (c) 2013 Institute for Theoretical Physics, ETH Zurich\n"
                  << "  copyright (c) 2010-2011 by Bela Bauer\n"
                  << "  copyright (c) 2011-2013 by Michele Dolfi\n"
                  << "  for details see the publication: \n"
                  << "  M. Dolfi et al., Computer Physics Communications 185, 3430 (2014).\n"
                  << "                   doi: 10.1016/j.cpc.2014.08.019\n"
                  << std::endl;
        alps::print_copyright(std::cout);
        */
        Options opt(argc,argv);
        if (opt.valid) {
            Scheduler pscan(opt);
            pscan.run();
        }
    } catch (std::exception & e) {
        std::cerr << "Exception thrown:" << std::endl;
        std::cerr << e.what() << std::endl;
        exit(1);
    }
}
