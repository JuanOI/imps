#pragma once

/** @file
    @brief File defining the SimUtils struct.
*/

#include <tuple>
#include <string>
#include <unordered_map>
#include <list>
#include <fstream>

#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#include <boost/any.hpp>

#include "dmrg/utils/DmrgParameters.h"



/** @struct SimUtils
    @brief class implementing basic functioality required for an iDMRG simulation.
*/
struct SimUtils
{
	/** @fn void add_default_parameters( DmrgParameters& parms )
		@brief convenience function defining a set of useful parameters during a iDMRG simulation.
		@param[in] parms an object of type DmrgParameters
	*/
	static void add_default_parameters( DmrgParameters& parms )
	{
		std::unordered_map< std::string , std::string > default_values;

		default_values["predict_state"] = boost::lexical_cast<std::string>( true );
		default_values["noise"] = boost::lexical_cast<std::string>( 0. );
		default_values["max_bond_dimension"] = boost::lexical_cast<std::string>( 10 );
		default_values["ngrowscales"] = boost::lexical_cast<std::string>( -1 );
		default_values["min_sweeps"] = boost::lexical_cast<std::string>( 0 );
		default_values["max_sweeps"] = boost::lexical_cast<std::string>( 10 );
		default_values["min_scales"] = boost::lexical_cast<std::string>( 0 );
		default_values["max_scales"] = boost::lexical_cast<std::string>( 0 );
		default_values["convergence_measure_every"] = boost::lexical_cast<std::string>( -1 );
		default_values["svd_tol"] = boost::lexical_cast<std::string>( 1e-10 );
		default_values["pinv_tol"] = boost::lexical_cast<std::string>( 1e-10 );
		default_values["ietl_jcd_tol"] = boost::lexical_cast<std::string>( 1e-9 );
		default_values["fidelity_tol"] = boost::lexical_cast<std::string>( 1e-7 );
		default_values["delta_energy"] = boost::lexical_cast<std::string>( 1e-7 );
		default_values["seed"] = boost::lexical_cast<std::string>( 13 );
		default_values["verbose"] = boost::lexical_cast<std::string>( true );
		default_values["print_time"] = boost::lexical_cast<std::string>( true );
		default_values["reuse_parameters"] = boost::lexical_cast<std::string>( true );
		default_values["optimization"] = boost::lexical_cast<std::string>( "twosite" );
		default_values["curr_optimization"] = boost::lexical_cast<std::string>( "twosite" );

		for (const auto& op : default_values)
		{
			const std::string& name = op.first;
		 	const std::string& value = op.second;

 		 	parms.set( name , value );
		}
	}

	/** @fn void initialize_internal_parameters( DmrgParameters& parms )
		@brief convenience function setting a few internal parameters.
		@param[in] parms an object of type DmrgParameters
	*/
	static void initialize_internal_parameters( DmrgParameters& parms )
	{
		parms.set("current_scale",0);
		parms.set("sweeps",0);
		parms.set("fix_boundary_mpos",	true );
	}

	/** @fn void parse_parameters( int argc , char ** argv , DmrgParameters& parms )
		@brief convenience function taking all program input and parsing parameters passed
		@param[in] argc number of user-specified parameters received by the application
		@param[in] argv pointer to user-specified parameters received by the application
		@param[in] parms an object of type DmrgParameters
	*/
	static void parse_parameters( int argc , char ** argv , DmrgParameters& parms )
	{
		namespace po = boost::program_options;

		typedef std::vector<std::string> meas_cont_t;
		meas_cont_t measurements;

		po::options_description desc("Allowed options");
		desc.add_options()
			("help",																					"Options that need to be specified include: prefix.")

			("LATTICE_LIBRARY" , po::value<std::string>(),												"Lattice library (file in xml format) to be used.")
			("LATTICE" , po::value<std::string>()->required(),														"Lattice for the simulaton.")
			("MODEL_LIBRARY" , po::value<std::string>(),												"Model library (file in xml format) to be used.")
			("MODEL" , po::value<std::string>()->required(),														"Model being simulated.")
			("a" , po::value<double>() ,																"Lattice constant.")
			("L" , po::value<std::size_t>(),															"Lattice length (along horizontal dimension). In the iMPS simulation this is the horizontal length of the unit cell size, and thus the \"infinite\" dimension.")
			("W" , po::value<std::size_t>(),															"Lattice width (along vertical dimension).")

			("COMPLEX" , po::value<bool>() ,															"Whether to use complex numbers or not.")

			("CONSERVED_QUANTUMNUMBERS" , po::value<std::string>() , 									"Numbers to be conserved.")

			("predict_state" , po::value<bool>() ,														"Whether to perform state prediction or not as the iMPS is grown.")
			("noise" , po::value<double>() , 															"Amount of mixing between predicted state and random state. Should be in range [0,1]. Only meaningful if setting predict_state to 1.")
			("max_bond_dimension" , po::value<std::size_t>() ,											"Maximum bond dimension allowed.")
			("min_sweeps" , po::value<std::size_t>() ,													"Minimum number of sweeps to be performed in space.")
			("max_sweeps" , po::value<std::size_t>() ,													"Maximum number of sweeps to be performed in space.")
			("min_scales" , po::value<std::size_t>() ,													"Minimum number of scales to grow the system.")
			("max_scales" , po::value<std::size_t>() ,													"Maximum number of scales to grow the system.")
			("optimization" , po::value<std::string>() ,													"Optimization scheme to be used. (singlesite,twosite)")
			("convergence_measure_every" , po::value<std::size_t>() ,										"Frequency with which to perform thermodynamic limit measurements. (-1 skips measurements during optimization)")
			("cutoff" , po::value<double>() ,																"Cutoff parameter to be used for the truncation of pseudoinverse (deprecated - use pinv_tol instead).")
			("svd_tol" , po::value<double>() ,																	"Cutoff parameter to be used for the truncation of SVD singular values.")
			("pinv_tol" , po::value<double>() ,																	"Cutoff parameter to be used for the truncation of pseudoinverse.")
			("ietl_jcd_tol" , po::value<double>() ,															"Cutoff parameter to be used for the JCD optimization.")
			("fidelity_tol" , po::value<double>() ,														"Cutoff parameter to be used for stopping scale iterations.")
			("delta_energy" , po::value<double>() ,														"Cutoff parameter to be used for determining energy convergence.")
			("load_prefix",	po::value<std::string>() ,													"File prefix to be used for loading the simulation.")
			("save_prefix",	po::value<std::string>()->required() ,										"File prefix to be used for saving the simulation.")
			("seed" , po::value<int>() ,																"Seed to be used for the pseudorandom number generator.")
			("verbose" , po::value<bool>() ,															"Print more information during the run.")
			("print_time" , po::value<bool>() ,															"Print total run time.")

			("reuse_parameters" , po::value<bool>() , 													"Whether to reuse parameters from previous run or not. Only used if load_prefix is provided." )

			("config_file" , po::value<std::string>() ,													"Provide a configuration file.");

		po::options_description dev("Developer options");

		po::options_description cmdl;
		cmdl.add(desc).add(dev);

		add_default_parameters( parms );

		po::parsed_options cmdl_parsed = po::command_line_parser(argc , argv ).options(cmdl).allow_unregistered().run();

		// Store command line options
		for( auto op : cmdl_parsed.options )
		{
			const std::string& name = op.string_key;
		 	const std::string& value = op.value.front();

 		 	parms.set( name , value );
		}

		po::variables_map vm;
		po::store( cmdl_parsed , vm );

		// Store config file options
		if( vm.count("config_file") )
		{
			std::ifstream file_stream(vm["config_file"].as<std::string>()) ;
			po::parsed_options conff_parsed = po::parse_config_file(file_stream , desc , true);

			po::store( conff_parsed , vm );

			for( auto op : conff_parsed.options )
			{
				const std::string& name = op.string_key;
			 	const std::string& value = op.value.front();

				parms.set( name , value );
			}
		}

		po::notify(vm);

		if( vm.count("cutoff") ){

			auto cutoff = vm["cutoff"].as<std::string>();
			parms.set( "pinv_tol" , cutoff );
		}

		if( vm.count("help") ){
			std::cout << desc << '\n';
			return;
		}

		if( vm.count("LATTICE_LIBRARY") && vm["LATTICE_LIBRARY"].as<std::string>().size() != 0 )
		{	std::cout << "Using user provided lattice library: " + vm["LATTICE_LIBRARY"].as<std::string>() << std::endl;	}
		else
		{	std::cout << "Using ALPS lattice library." << std::endl;	}

		if( vm.count("MODEL_LIBRARY") && vm["MODEL_LIBRARY"].as<std::string>().size() != 0 )
		{
			std::cout << "Using user provided model library: " + vm["MODEL_LIBRARY"].as<std::string>() << std::endl;
		}
		else
		{	std::cout << "Using ALPS model library." << std::endl;	}

		if( vm.count("CONSERVED_QUANTUMNUMBERS") && vm["CONSERVED_QUANTUMNUMBERS"].as<std::string>().size() != 0 )
		{
			std::cout << "Using conserved quantum numbers: " << vm["CONSERVED_QUANTUMNUMBERS"].as<std::string>() << std::endl;
		}

		if( vm.count("LATTICE") ){
			std::cout << "Lattice is: " << vm["LATTICE"].as<std::string>() << '\n';
		}

		if( vm.count("MODEL") ){
			std::cout << "Model is: " << vm["MODEL"].as<std::string>() << '\n';
		}

		initialize_internal_parameters(parms);
	}
};
