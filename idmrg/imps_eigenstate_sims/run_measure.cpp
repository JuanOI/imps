#include <boost/filesystem/fstream.hpp>
#include <string>
#include <complex>

#include <alps/hdf5.hpp>

#include <alps/numeric/matrix.hpp>
#include "dmrg/block_matrix/detail/alps.hpp"
typedef double scalar_type;
typedef std::complex< double > complex_scalar_type;

#include "idmrg/libpscan/run_sim.hpp"

typedef alps::numeric::matrix< scalar_type > matrix_type;
typedef alps::numeric::matrix< complex_scalar_type > complex_matrix_type;

#include "dmrg/block_matrix/symmetry.h"
typedef NU1 sym_group;

#include "idmrg/imps_eigenstate_sims/imps_measure.hpp"

void run_sim(const boost::filesystem::path& infile, const boost::filesystem::path& outfile,
             bool write_xml, double time_limit)
{
    /// Load parameters
    DmrgParameters parms;
    {
        boost::filesystem::ifstream param_file(infile);
        if (!param_file)
            throw std::runtime_error(std::string("Could not open parameter file ") + infile.string() +".");
        alps::Parameters p; p.extract_from_xml(param_file);
        parms = DmrgParameters(p);
    }

    assert(parms.defined("chkpfile"));
    std::string chkpfile = parms["chkpfile"];
    assert(boost::filesystem::exists(chkpfile));

    parms.set("resultfile", (outfile.parent_path() / outfile.stem()).string() + ".h5");

    /// Start simulation
    const bool use_complex = parms["COMPLEX"];

    if( use_complex )
        measure<complex_matrix_type , sym_group>( parms );
    else
        measure<matrix_type , sym_group>( parms );
}
