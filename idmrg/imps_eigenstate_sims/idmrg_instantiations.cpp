/** @file
    @brief File instantiating the iDMRG class for both real and complex ALPS matrices using U(1) quantum numbers.
*/



#include <alps/numeric/matrix.hpp>

typedef double scalar_t;
typedef std::complex< double > complex_scalar_t;

typedef alps::numeric::matrix< scalar_t > matrix_t;
typedef alps::numeric::matrix< complex_scalar_t > complex_matrix_t;

#include "alps/numeric/matrix/algorithms.hpp"
#include "dmrg/block_matrix/detail/alps_detail.hpp"
#include "dmrg/block_matrix/symmetry.h"
typedef NU1 sym_group_t;

#include "idmrg/optimize/idmrg.hpp"

template class idmrg::iDMRG<matrix_t,sym_group_t>;
template class idmrg::iDMRG<complex_matrix_t,sym_group_t>;
