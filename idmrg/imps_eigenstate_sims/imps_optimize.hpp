#pragma once

/** @file
    @brief Header file implementing @p optimize function template.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <iomanip>
#include <complex>
#include <vector>
#include <random>
#include <memory>
#include <chrono>
#include <ctime>
#include <cmath>

#include <alps/hdf5.hpp>
#include <alps/numeric/matrix.hpp>

#include "dmrg/block_matrix/detail/alps.hpp"
#include "dmrg/block_matrix/symmetry.h"
#include "dmrg/block_matrix/block_matrix.h"
#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "dmrg/models/coded/lattice.hpp"
#include "dmrg/models/alps/model.hpp"
#include "dmrg/models/generate_mpo.hpp"
#include "dmrg/mp_tensors/contractions.h"
#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/mpo_ops.h"
#include "dmrg/mp_tensors/twositetensor.h"
#include "dmrg/utils/DmrgParameters.h"
#include "dmrg/utils/placement.h"

#include "idmrg/block_matrix/block_matrix_ietl.hpp"
#include "idmrg/mp_tensors/imps.h"
#include "idmrg/mp_tensors/imps_initializers.h"
#include "idmrg/mp_tensors/impo.h"
#include "idmrg/optimize/idmrg.h"
#include "idmrg/mp_tensors/impo_maker.h"
#include "idmrg/models/measurements/utils.hpp"

class iLattice;

/** @fn void optimize(DmrgParams&& parms)
    @brief function optimizing a given iMPS.
    @param [in] parms DmrgParameters object specifying inputs for the simulation.
	@pre @p parms shall be of type DmrgParameters.
	@pre @p parms shall define the fields: LATTICE, MODEL and chkpfile.
	@remark @p parms may the define the field print_time to 1, in which case the total run time of the simulation will be printed upon conclusion.
	@remark @p parms may the define the field verbose to 1, in which case additional information will be printed out to stdout during the simulation.
*/
template <class Matrix , class SymmGroup, class DmrgParams>
void optimize(DmrgParams&& parms) {

		typedef Matrix matrix_t;
		typedef typename Matrix::value_type value_type;
		typedef block_matrix<matrix_t, SymmGroup> block_matrix_t;
		typedef typename iMPS<Matrix,SymmGroup>::real_diag_matrix_t real_diag_matrix_t;
		typedef typename iMPS<Matrix,SymmGroup>::lambda_t lambda_t;
		typedef typename SymmGroup::charge charge_t;
		typedef Index<SymmGroup> index_t;
		typedef std::vector< Boundary<matrix_t, SymmGroup> > bound_cont_t;

		using iDMRG = idmrg::iDMRG<Matrix,SymmGroup>;

		using iMPS = iMPS<Matrix,SymmGroup>;
		using iMPO = iMPO<Matrix,SymmGroup>;
		using MPO = MPO<Matrix,SymmGroup>;
		using TaggedIMPOMaker = generate_mpo::TaggedIMPOMaker<Matrix, SymmGroup>;

		using ALPSModel = ALPSModel<Matrix, SymmGroup>;
		using iModelDescriptor = idmrg::iModelDescriptor<Matrix, SymmGroup>;

		using default_imps_init = default_imps_init<Matrix,SymmGroup>;

		/* parse input arguments */
		const size_t M = 											parms["max_bond_dimension"]; /// Maximum MPS bond dimension
		const double cutoff = 										parms["pinv_tol"]; /// Cutoff used for monitoring convergence of eigensolvers and computation of the pseudoinverse
		const std::string chkpfile = 								parms["chkpfile"]; /// Prefix used for saving files.
		const int seed = 											parms["seed"]; /// Seed used for the random number generation.
		bool verbose =												parms["verbose"]; /// Run on verbose mode
		bool print_time =											parms["print_time"]; /// Time the execution of the program

		dmrg_random::engine.seed(seed);

		// Create lattice and model for the optimization
		boost::shared_ptr<iLattice> ilattice_p( new iLattice( parms ) );
		Lattice lattice( ilattice_p );

		int center = lattice.size()/2 - 1;

		// We will treat the shift index by introducing a different type of index which has been shifted.
		// We need to account for this during the creation of the MPO

		iModelDescriptor model_desc( parms , lattice , center );

		///// ===== MAKE MPO ===== /////

		TaggedIMPOMaker mpom(lattice, model_desc );
		iMPO mpo = mpom.create_mpo();

		// just for debugging
		if( verbose ) print_mpo( std::cout , mpo );

		construct_placements( mpo );
		mpo.match_edge_placements();

		if( verbose )
		{
			print_mpo( std::cout , mpo );

			std::cout << "System size: " << lattice.size() << '\n';
			std::cout << "Center: " << center << '\n';
			std::cout << "Shift charge: " << model_desc.shift_charge() << '\n';

			std::vector<index_t> indices;
			indices = model_desc.indices();
			indices.push_back( model_desc.shift_index() );

			std::cout << "Indices in use are:\n\n";
			for( auto i : indices )
				std::cout << i <<'\n';

			std::string opt = parms["optimization"];
			std::string curr_opt = parms["curr_optimization"];


			std::cout << "Using " 	<< (curr_opt == "singlesite" ? std::string("one-site") : std::string("two-site"))
			 						<< " optimization as starting method." << std::endl;
			std::cout << "Using " 	<< (opt == "singlesite" ? std::string("one-site") : std::string("two-site"))
									 << " optimization as main method." << std::endl;
		}

		parms.set("unit_cell_size" , lattice.size() );

		bound_cont_t left_boundaries( lattice.size() );
		bound_cont_t right_boundaries( lattice.size() );

		/// === Continue from checkpoint? === ///
		// Determine whether there exists a status file and if so load current scale and sweep.

		iMPS mps;

		iDMRG idmrg;
		if( !idmrg.restart_from_parameters( parms ) )
		{
			///// ===== BEGIN MPS CREATION ===== /////

			default_imps_init imps_init(parms , model_desc);
			mps = iMPS( lattice.size() , center , imps_init );

			// Initialize lambdas
			{
				charge_t IdCharge;
				IdCharge = SymmGroup::IdentityCharge;

				real_diag_matrix_t mid = real_diag_matrix_t::identity_matrix(1);
				mps.lambda_old().insert_block( mid , IdCharge , IdCharge );
			}

			if( verbose )
			{
				for( int i = 0 ; i < mps.length() ; ++i )
					std::cout << mps[i] << std::endl;
				std::cout << "Old Schmidt values\n" << mps.lambda_old() << '\n';
			}

			// This puts the iMPS into mixed canonical form, normalizes then absorbs lambda on the left
			mps.normalize( M , cutoff );
			mps.normalize_right();

			///// ===== END MPS CREATION ===== /////

			left_boundaries.front() = idmrg.make_trivial_boundary();
			right_boundaries.back() = idmrg.make_trivial_boundary();

			idmrg.save( chkpfile , mps , left_boundaries.front() , right_boundaries.back() , parms );
		}
		else
		{
			const std::string loadfile = 					parms["loadfile"]; /// Prefix used for loading files.

			idmrg.load( loadfile , mps , left_boundaries.front() , right_boundaries.back() );

			std::size_t current_scale = parms["current_scale"];
			if( current_scale > 0 )
			{
				bool fix_boundary_mpos = false;
				mpo.set_edges();
				parms.set("fix_boundary_mpos",fix_boundary_mpos);
			}

			//if( current_scale % 2 != 0 )
			if( mps.center() != mps.shift_site() )
				idmrg.shift_unit_cell( mps.center() , mpo );


			if( 0 != mps.canonization() )
			{
				std::cout << "Performing canonization." << std::endl;
				mps.canonize( 0 );
			}

			std::cout << "Reloaded information from scale " << current_scale << "." << std::endl;

		}

		///// ===== OPTIMIZE ===== /////
		// Begin scale simulation (MPS will exit in right canonical form)
		if(verbose) std::cout << "Starting scale sweeps." << std::endl;

		const int scales_done = idmrg.sweep_scales( mps , mpo , left_boundaries , right_boundaries , *ilattice_p , model_desc , parms );

		if( mps.center() != mps.shift_site() )
			idmrg.shift_unit_cell( (mps.length() - 1) - (mps.center() + 1) + 1, mps.shift_site() , model_desc );

		if(verbose) std::cout << "Finished all sweeps.\nPreparing to measure.\n" << std::endl;

		lambda_t lambda_inverse;
		bool measure_ready; /// Use exception instead
		std::tie(measure_ready,lambda_inverse) = idmrg.get_orthogonalized_boundaries(mps,parms);

		// store one last time to make sure we know its ready to measure
		mps.save(chkpfile);

		/////===== MEASURE =====/////
        if( measure_ready )
			imeasurement::measure( lambda_inverse , mps , *ilattice_p , model_desc , parms );

		parms.set("continue_simulation",false);
		alps::hdf5::archive ar( chkpfile + "/parameters.h5" , "w");
		ar["/parameters"] << parms;

		return;
}
