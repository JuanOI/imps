/** @file
    @brief File exposing the optimize function as an application.
*/



#include <complex>

#include <alps/hdf5.hpp>

#include <alps/numeric/matrix.hpp>
#include "dmrg/block_matrix/detail/alps.hpp"
typedef double scalar_type;
typedef std::complex< double > complex_scalar_type;

typedef alps::numeric::matrix< scalar_type > matrix_type;
typedef alps::numeric::matrix< complex_scalar_type > complex_matrix_type;

#include "dmrg/block_matrix/symmetry.h"
typedef NU1 sym_group;

#include "idmrg/sim/utils.h"
#include "idmrg/imps_eigenstate_sims/imps_optimize.hpp"

int main(int argc, char ** argv)
{
    if( argc == 1 ){
        std::cout << "Usage: ./idmrg [option1] <[option2] ...>";
        return 0;
    }

	try
	{
		DmrgParameters parms;

		SimUtils::parse_parameters( argc , argv , parms );

		const bool use_complex = parms["COMPLEX"];

		if( use_complex )
			optimize<complex_matrix_type , sym_group>( parms );
		else
			optimize<matrix_type , sym_group>( parms );

	}catch(std::exception& e)
	{
		std::cerr << "error: " << e.what() << '\n';
		return 1;
	}

	return 0;
}
