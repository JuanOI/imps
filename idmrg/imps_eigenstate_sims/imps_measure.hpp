#pragma once

/** @file
    @brief Header file implementing @p measure function template.
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <random>
#include <chrono>
#include <ctime>
#include <cmath>

#include <alps/hdf5.hpp>
#include <alps/numeric/matrix.hpp>

#include "dmrg/block_matrix/detail/alps.hpp"
#include "dmrg/block_matrix/symmetry.h"
#include "dmrg/block_matrix/block_matrix.h"
#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "dmrg/models/coded/lattice.hpp"
#include "dmrg/models/alps/model.hpp"
#include "dmrg/mp_tensors/contractions.h"
#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/mpo_ops.h"
#include "dmrg/utils/DmrgParameters.h"
#include "dmrg/utils/placement.h"

#include "idmrg/mp_tensors/imps.h"
#include "idmrg/optimize/idmrg.h"
#include "idmrg/mp_tensors/ts_ops.h"
#include "idmrg/lattice/lattice.h"
#include "idmrg/mp_tensors/impo.h"
#include "idmrg/mp_tensors/impo_maker.h"
#include "idmrg/models/measurements/utils.hpp"
#include "idmrg/utils/utils.hpp"

/** @fn void measure(DmrgParams&& parms)
    @brief function running measurements on a given iMPS.
    @param [in] parms DmrgParameters object specifying inputs for the simulation.
	@pre @p parms shall be of type DmrgParameters.
	@pre @p parms shall define the field chkpfile.
	@remark @p parms may the define the field print_time, in which case the total run time of the simulation will be printed upon conclusion.
*/
template <class Matrix , class SymmGroup, class DmrgParams>
void measure(DmrgParams&& parms) {

		typedef Matrix matrix_t;
		typedef typename Matrix::value_type value_type;
		typedef block_matrix<matrix_t, SymmGroup> block_matrix_t;
		typedef typename iMPS<Matrix,SymmGroup>::real_diag_matrix_t real_diag_matrix_t;
		typedef typename iMPS<Matrix,SymmGroup>::lambda_t lambda_t;
		typedef typename SymmGroup::charge charge_t;
		typedef Index<SymmGroup> index_t;
		typedef std::vector< Boundary<matrix_t, SymmGroup> > bound_cont_t;

		using iDMRG = idmrg::iDMRG<Matrix,SymmGroup>;

		using iMPS = iMPS<Matrix,SymmGroup>;
		using TaggedIMPOMaker = generate_mpo::TaggedIMPOMaker<Matrix, SymmGroup>;
		using iModelDescriptor = idmrg::iModelDescriptor<Matrix, SymmGroup>;

		/* parse input arguments */
		const size_t M = 											parms["max_bond_dimension"]; /// Maximum MPS bond dimension
		const double cutoff = 								parms["pinv_tol"]; /// Cutoff used for monitoring convergence of eigensolvers and computation of the pseudoinverse
		const std::string chkpfile = 							parms["chkpfile"]; /// Prefix used for loading files.
		const int seed = 											parms["seed"]; /// Seed used for the random number generation.
		bool verbose =												parms["verbose"]; /// Run on verbose mode
		bool print_time =											parms["print_time"]; /// Time the execution of the program

		dmrg_random::engine.seed(seed);

		// Begin timing if requested
		std::chrono::time_point<std::chrono::system_clock> start;
		if( print_time )
		{
			start = std::chrono::system_clock::now();

			std::time_t start_time = std::chrono::system_clock::to_time_t(start);
			std::cout << "Starting computations on: " << std::ctime( &start_time ) << std::endl;
		}

		// Create lattice and model for the optimization
		boost::shared_ptr<iLattice> ilattice_p( new iLattice( parms ) );
		Lattice lattice( ilattice_p );

		int center = lattice.size()/2 - 1;

		// We will treat the shift index by introducing a different type of index which has been shifted.
		// We need to account for this during the creation of the MPO

		iModelDescriptor model_desc( parms , lattice , center );

		if( verbose )
		{
			std::cout << "System size: " << lattice.size() << '\n';
			std::cout << "Center: " << center << '\n';
			std::cout << "Shift charge: " << model_desc.shift_charge() << '\n';

			std::vector<index_t> indices;
			indices = model_desc.indices();
			indices.push_back( model_desc.shift_index() );

			std::cout << "Indices in use are:\n\n";
			for( auto i : indices )
				std::cout << i <<'\n';
		}

		parms.set("unit_cell_size" , lattice.size() );

		iMPS mps;
		mps.load( chkpfile );

		iDMRG idmrg;

		if( mps.center() != mps.shift_site() )
			idmrg.shift_unit_cell( (mps.length() - 1) - (mps.center() + 1) , mps.shift_site() , model_desc );

		// Ensure orthogonal form for imps prior to thermodynamic limit measurements
		lambda_t lambda_inverse;
		bool perform_measurements = true;
		if( !mps.is_measure_ready() )
			std::tie(perform_measurements,lambda_inverse) = idmrg.get_orthogonalized_boundaries(mps,parms);
		else
			lambda_inverse = pinv( mps.lambda_old() , M , cutoff );

		/////===== MEASURE =====/////
		if( perform_measurements )
			imeasurement::measure( lambda_inverse , mps , *ilattice_p , model_desc , parms );

		// Stop recording time
		auto end = std::chrono::system_clock::now();
		if( print_time )
		{
			std::chrono::duration<double> elapsed_seconds = end-start;
			std::time_t end_time = std::chrono::system_clock::to_time_t(end);
			std::cout << "Finished computations on: " << std::ctime( &end_time ) << std::endl;
			std::cout << "Elapsed time (s): " << elapsed_seconds.count() << std::endl;
		}

		return;
}
