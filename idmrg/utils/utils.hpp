#pragma once

/** @file
    @brief Header file defining utility functions.
*/

#include <complex>

#include <alps/hdf5.hpp>
#include <alps/numeric/matrix.hpp>

#include "dmrg/block_matrix/detail/alps.hpp"
#include "dmrg/block_matrix/block_matrix.h"

/** @fn void inv( Matrix& mat , const int Mmax , const double threshold )
    @brief computes the inverse of a diagonal matrix
*/
template<class Matrix>
void inv( Matrix& mat , const int Mmax , const double threshold )
{
	for( int i = 0 ; i < mat.num_rows() ; ++i )
		if( std::abs(mat(i,i)) > threshold )
			mat(i,i) = 1./mat(i,i);
		else
			mat(i,i) = 0.;

}

/** @fn auto	pinv(	block_matrix<Matrix , SymmGroup> mat ,
				const int Mmax ,
				const double threshold ) -> block_matrix<Matrix , SymmGroup>
    @brief computes the inverse of a block diagonal matrix
*/
template<class Matrix,class SymmGroup>
auto	pinv(	block_matrix<Matrix , SymmGroup> mat ,
				const int Mmax ,
				const double threshold ) -> block_matrix<Matrix , SymmGroup>
{

	for( int k = 0 ; k < mat.n_blocks() ; ++k )
		inv( mat[k] , Mmax , threshold );

	return mat;
}
