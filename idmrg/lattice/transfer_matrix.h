#pragma once

/** @file
    @brief Header file implementing the TransferMatrix struct.
*/



#include <complex>
#include <iostream>

#include "dmrg/mp_tensors/contractions.h"
#include "dmrg/block_matrix/block_matrix.h"
#include "dmrg/block_matrix/indexing_stable.hpp"
#include "dmrg/mp_tensors/mpstensor.h"



/** @struct TransferMatrix
    @brief struct implementing the concept of a transfer matrix. This is required when performing measurements in the thermodnamic limit which are carried out by computing the leading eigenvectors of the a system's transfer matrix.
*/
template<class Matrix, class SymmGroup>
struct TransferMatrix
{
	typedef iMPS< Matrix, SymmGroup> object_t;
	typedef block_matrix<Matrix, SymmGroup> block_matrix_t;
	typedef typename iMPS<Matrix, SymmGroup>::lambda_t lambda_t;
	typedef block_matrix_t vector_t;
	typedef typename vector_t::scalar_type scalar_type;
	typedef typename vector_t::value_type value_type;
	enum Orientation { Left , Right };

	/// @param[in] mps the MPS defining the system.
	/// @param[in] old_lambda_inv a matrix containing the (pseudo-)inverse of the Schmidt values of the MPS one scale before the transfer matrix is constructed.
	/// @param[in] orient the orientation with which the transfer matrix is considered as a matrix. Required to determine from which direction to multiply the matrix from.
	TransferMatrix( const object_t& mps , const lambda_t& old_lambda_inv , const Orientation orient = Left )
	: mps_(mps), old_lambda_inv_(old_lambda_inv) , orient_(orient)
	{}

	/// @brief set the orientation of the transfer matrix
	void set_orientation( const Orientation orient ) const
		{ orient_ = orient;	}

	/// @brief multiplies the transfer matrix by the vector provided.
	/// @param[in] vec the vector to multiply the transfer matrix with.
	vector_t multiply( vector_t vec ) const;
	void verify_canonical_form() const;
	vector_t construct() const;

	const object_t& mps_; /// a reference to the underlying MPS.
	const lambda_t& old_lambda_inv_; /// a reference to the previous inverse of the Schmidt values of the MPS.
	mutable Orientation orient_; /// Determines from which direction the transfer matrix will be multiplied by a vector
};

template<class Matrix, class SymmGroup>
typename TransferMatrix< Matrix , SymmGroup>::vector_t
	TransferMatrix< Matrix , SymmGroup>::multiply( typename TransferMatrix< Matrix , SymmGroup>::block_matrix_t vec ) const
{
	typedef typename TransferMatrix< Matrix , SymmGroup >::block_matrix_t block_matrix_t;

	block_matrix_t  lambda_temp;

	if( orient_ == Left ) // Left to right multiplication
	{
		for( int i = 0 ; i < mps_.length() ; ++i )
		{
			vec = contraction::overlap_left_step( mps_[i] , mps_[i] , vec );
		}

		// Unit cell is closed by inverse old schmidt values
		gemm( vec , old_lambda_inv_ , lambda_temp );
		gemm( old_lambda_inv_ , lambda_temp , vec );
	}
	else
	{
		for( int i = mps_.length() - 1 ; i >= 0 ; --i )
		{
			vec = contraction::overlap_right_step( mps_[i] , mps_[i] , vec );
		}

		gemm( vec , old_lambda_inv_ , lambda_temp );
		gemm( old_lambda_inv_ , lambda_temp , vec );
	}

	return vec;
}

template<class Matrix, class SymmGroup>
void TransferMatrix< Matrix , SymmGroup>::verify_canonical_form() const
{
	typedef typename TransferMatrix< Matrix , SymmGroup >::block_matrix_t block_matrix_t;

	// This function should only be called with an iMPS which has already been canonized for measurements in the thermodynamic limit.

	object_t holder_mps = mps_;

	block_matrix_t id_init;
	block_matrix_t id_vec;

	const Index<SymmGroup> virtual_index = mps_.left_gauge().left_basis();

	typedef typename Index<SymmGroup>::const_iterator const_iter_t;
	const_iter_t it = virtual_index.begin();
	const_iter_t it_e = virtual_index.end();

	id_init = block_matrix<Matrix, SymmGroup>();

	while( it != it_e )
	{
		id_init.insert_block( Matrix::identity_matrix( virtual_index.size_of_block( it->first ) ) , it->first , it->first );
		it++;
	}

	block_matrix_t lambda_temp;

	std::cout << "verifying inverse condition on gauges\n";
	std::cout << "Left gauge\n";
	std::cout << mps_.left_gauge() << '\n';
	std::cout << mps_.left_gauge_inv() << '\n';
	gemm( mps_.left_gauge() , mps_.left_gauge_inv() , lambda_temp );
	std::cout << lambda_temp;

	gemm( mps_.left_gauge_inv() , mps_.left_gauge() , lambda_temp );
	std::cout << lambda_temp;

	std::cout << "Right gauge\n";
	gemm( mps_.right_gauge() , mps_.right_gauge_inv() , lambda_temp );
	std::cout << lambda_temp;

	gemm( mps_.right_gauge_inv() , mps_.right_gauge() , lambda_temp );
	std::cout << lambda_temp;

	std::cout << "Id block matrix before multiplication (should be identically an identity)\n";
	std::cout << id_init << '\n';

	// Test left canonical condition (id -> id)
	mps_[0].multiply_from_left( mps_.left_gauge() );

	set_orientation( Orientation::Left );
	id_vec = multiply( id_init );

	gemm( id_vec , mps_.left_gauge_inv() , lambda_temp );
	gemm( transpose( conjugate( mps_.left_gauge_inv() ) ) , lambda_temp , id_vec );

	std::cout << "Id block matrix after multiplication (should be very close to an identity)\n";
	std::cout << id_vec << '\n';

	mps_ = holder_mps;

	// Test right canonical condition (id -> id)
	mps_[mps_.length() - 1].multiply_from_right( mps_.right_gauge() );

	set_orientation( Orientation::Right );
	id_vec = multiply( id_init );

	gemm( id_vec , transpose( mps_.right_gauge_inv() ) , lambda_temp );
	gemm( conjugate( mps_.right_gauge_inv() ) , lambda_temp , id_vec );

	std::cout << "Id block matrix after multiplication (should be very close to an identity)\n";
	std::cout << id_vec << '\n';

	mps_ = holder_mps;
	std::cout << "Finished verifying.\n";
}
