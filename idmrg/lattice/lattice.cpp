/** @file
    @brief File implementing the iLattice class.
	This class is required to extend the functionality of the underlying ALPS lattice to simulations of infinite systems.
*/

#include <sstream>
#include <vector>
#include <set>
#include <boost/lexical_cast.hpp>

#include "dmrg/utils/BaseParameters.h"
#include "dmrg/models/alps/lattice.hpp"
#include "idmrg/lattice/lattice.h"



iLattice::iLattice ( parameters_type& p ):
 alps_base(p) ,
 num_graphs_(1) ,
 site_types_(),
 bonds_()
{
    initialize_lattice_info();
    update_lattice_info();
}

const std::vector<std::vector<iLattice::pos_t>>& iLattice::forward() const
{   return forward_; }

std::vector<iLattice::pos_t> iLattice::forward( iLattice::pos_t p ) const
{   return forward_[p]; }

std::vector<iLattice::pos_t> iLattice::all( iLattice::pos_t p ) const
{
    std::vector<pos_t> ret = forward_[p];
    std::copy(backward_[p].begin(), backward_[p].end(), std::back_inserter(ret));

    return ret;
}

const std::vector<std::vector<iLattice::pos_t>>& iLattice::bonds() const
{   return bonds_;  }

iLattice::pos_t iLattice::bond_index( const iLattice::pos_t src , const iLattice::pos_t tgt ) const
{   return bond_index_map[src][tgt];    }

iLattice::pos_t iLattice::size() const
{   return graph.num_sites() * num_graphs_; }

int iLattice::maximum_vertex_type() const
{   return alps::maximum_vertex_type(graph.graph());    }

boost::any iLattice::get_prop_(std::string const & property, std::vector<iLattice::pos_t> const & pos) const
{
    if (property == "label" && pos.size() == 1)
        return boost::any( site_label( pos[0] ) );
    else if (property == "label" && pos.size() == 2)
        return boost::any( bond_label( pos[0] , pos[1] ) );
    else if (property == "type" && pos.size() == 1)
        return boost::any( site_types_[ pos[0] % graph.num_sites() ] );
    else if (property == "type" && pos.size() == 2)
        return boost::any( 0 ); // Essentially disabled...
    else if (property == "uc_site" && pos.size() == 1)
        return boost::any( pos[0] % graph.num_sites() );
    else if (property == "wraps_pbc" && pos.size() == 2)
        return boost::any( static_cast<bool>(boost::get(alps::boundary_crossing_t(),
                                                        graph.graph(),
                                                        graph.bond(bond_index_map[pos[0] % graph.num_sites()][pos[1] % graph.num_sites()]))) );
    else {
        std::ostringstream ss;
        ss << "No property '" << property << "' with " << pos.size() << " points implemented.";
        throw std::runtime_error(ss.str());
        return boost::any();
    }
}

const iLattice::graph_type& iLattice::alps_graph() const
{   return graph;   }

void iLattice::set_num_graphs( const iLattice::pos_t ng )
{
    if( ng == num_graphs_ )
        return;

    num_graphs_ = ng;

    update_lattice_info();
}

iLattice::pos_t iLattice::get_num_graphs() const
{   return num_graphs_; }

std::string iLattice::site_label (int i) const
{   return "( " + boost::lexical_cast<std::string>(i) + " )";   }

std::string iLattice::bond_label (int i, int j) const
{
    return (  "( " + boost::lexical_cast<std::string>(i) + " )"
            + " -- "
            + "( " + boost::lexical_cast<std::string>(j) + " )");
}

void iLattice::initialize_lattice_info() const
{
	for( int i = 0 ; i < graph.num_sites() ; ++i )
		site_types_.push_back( static_cast<int>( graph.site_type( graph.site( i ) ) ) );
}

void iLattice::update_lattice_info()
{
	if( this->size() < forward_.size() )
	{
	    forward_.resize(this->size());
	    backward_.resize(this->size());

        auto disconnected = [this]( const std::vector<pos_t>& p ){
            return (p.front()>this->size()) && (p.back()>this->size());
        };
        auto it = std::remove_if(bonds_.begin(),bonds_.end(),disconnected);
        bonds_.erase(it,bonds_.end());
	}
	else
	{
		pos_t new_graphs = this->size() / graph.num_sites();

		const std::size_t L = parms["L"];

#if defined VERBOSE
		std::cout << "Old length is " << L << "\n";
#endif

		parms["L"] = boost::lexical_cast<std::string>( L * (new_graphs + 1) ); // Original + addtionally requested + additional required to get the outer edges right.
		graph_type enlarged_graph( parms );
		parms["L"] = boost::lexical_cast<std::string>(L);

		forward_.clear();
		backward_.clear();
        bonds_.clear();

        forward_.resize( graph.num_sites() );
        backward_.resize(graph.num_sites() );

		for (graph_type::bond_iterator it=enlarged_graph.bonds().first; it!=enlarged_graph.bonds().second; ++it) {
			graph_type::size_type s, t;
			s = enlarged_graph.vertex_index(enlarged_graph.source(*it));
			t = enlarged_graph.vertex_index(enlarged_graph.target(*it));

            if( (s < graph.num_sites()) || (t < graph.num_sites()) )
            {
                bonds_.push_back({s,t});
            }

			if( s < graph.num_sites() )
			{
				forward_[s].push_back(t);
				bond_index_map[s][t] = enlarged_graph.edge_index(*it);
			}

			if( t < graph.num_sites() )
			{
				backward_[t].push_back(s);
				bond_index_map[t][s] = enlarged_graph.edge_index(*it);
			}
		}
 	}
}
