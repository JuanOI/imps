#pragma once

/** @file
    @brief Header file declaring the iLattice class.
	This class is required to extend the functionality of the underlying ALPS lattice to simulations of infinite systems.
*/



#include <vector>
#include <boost/lexical_cast.hpp>

#include "dmrg/utils/BaseParameters.h"
#include "dmrg/models/alps/lattice.hpp"

/** @class iLattice
    @brief class deriving from alps_lattice (defined as part of the ALPS MPS framework) and implementing the notion of an infinite lattice
*/
class iLattice : public alps_lattice
{

public:
    typedef lattice_impl::pos_t pos_t; // Int
    typedef alps::graph_helper<> graph_type;
	typedef alps::Parameters parameters_type;
    typedef graph_type::site_descriptor site_descriptor;
    typedef graph_type::site_iterator site_iterator;
    typedef alps_lattice alps_base;
	typedef std::map<pos_t, std::map<pos_t, pos_t> > bond_index_map_t;

	/// @param[in] p parameters specifying the properties of the lattice.
    iLattice ( parameters_type& p );

	/// @return a vector with as many entries as sites in the unit cell of the infinite lattice. Each entry contains a vector of the positions of all 'forward' neighbours of a given site.
    const std::vector<std::vector<pos_t>>& forward() const;

	/// @param[in] p numerical label of a site a site.
	/// @return a vector of the positions of all 'forward' neighbours of site @p p.
    std::vector<pos_t> forward(pos_t p) const override;

	/// @param[in] p numerical label of a site a site.
	/// @return a vector of the positions of all neighbours of site @p p.
    std::vector<pos_t> all(pos_t p) const override;

    const std::vector<std::vector<pos_t>>& bonds() const;

	/// @param[in] src source site
	/// @param[in] tgt target site
	/// @return the index of the bond joining sites src and tgt.
	pos_t bond_index( const pos_t src , const pos_t tgt ) const;

	/// @return the number of sites in the unit cell in its current configuration.
    pos_t size() const override;

	/// @return the maximum type of all vertex.
    int maximum_vertex_type() const override;

	/// @param[in] property the property requested
	/// @param[in] p the position of the site
	/// @return the propery of site p.
    boost::any get_prop_(std::string const & property, std::vector<pos_t> const & pos) const override;

	/// @return reference to the underlying ALPS graph.
    const graph_type& alps_graph() const override;

	/// @brief dynamically set the size of the unit cell.
	void set_num_graphs( const pos_t ng );

	/// @brief gets the current size of the unit cell.
	/// @return the size of the unit cell in units of the underlying number of graphs.
	pos_t get_num_graphs() const;

protected:
	void initialize_lattice_info() const;
	void update_lattice_info();

    std::string site_label (int i) const;
    std::string bond_label (int i, int j) const;

	std::size_t num_graphs_;
	mutable std::vector<int> site_types_;
    mutable std::vector<std::vector<pos_t> > bonds_;
};
