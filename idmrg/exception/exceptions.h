#pragma once

#include <stdexcept>

namespace idmrg{

struct SimulationFinished : std::runtime_error {
    SimulationFinished():
    std::runtime_error("")
    {};

    virtual const char* what() const noexcept override
    {
        return std::string("Simulation finished.\nIf you wish to extend the simulation increase the number of scales and set a parameter \'continue_simulation\' to true.").c_str();
    }
};

} // namespace idmrg
