#pragma once

/** @file
    @brief Header file defining the class TaggedIMPOMaker.
*/

#include <string>
#include <sstream>

#include <boost/bind.hpp>

#include "dmrg/block_matrix/block_matrix.h"
#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "dmrg/block_matrix/symmetry.h"
#include "dmrg/mp_tensors/mpo.h"
#include "dmrg/mp_tensors/mpo_ops.h"
#include "dmrg/models/alps/lattice.hpp"
#include "dmrg/models/model.h"
#include "dmrg/models/generate_mpo.hpp"

#include "idmrg/mp_tensors/impo.h"
#include "idmrg/models/imodel_descriptor.h"

namespace generate_mpo
{

    /** @struct TaggedIMPOMaker
        @brief class designed to construct iMPOs.
    */
    template<class Matrix, class SymmGroup>
    class TaggedIMPOMaker
    {
        typedef typename Matrix::value_type scale_type;
        typedef typename MPOTensor<Matrix, SymmGroup>::index_type index_type;
        typedef block_matrix<Matrix, SymmGroup> op_t;

        typedef Lattice::pos_t pos_t;
        typedef typename Operator_Tag_Term<Matrix, SymmGroup>::tag_type tag_type;
        typedef typename Operator_Tag_Term<Matrix, SymmGroup>::op_pair_t pos_op_type;
        typedef boost::tuple<std::size_t, std::size_t, tag_type, scale_type> tag_block;

        typedef ::term_descriptor<typename Matrix::value_type> term_descriptor;

        typedef detail::prempo_key<pos_t, tag_type, index_type> prempo_key_type;
        typedef std::pair<tag_type, scale_type> prempo_value_type;
        // TODO: consider moving to hashmap
        typedef std::map<std::pair<prempo_key_type, prempo_key_type>, prempo_value_type> prempo_map_type;
        typedef idmrg::iModelDescriptor<Matrix, SymmGroup> iModelDescriptor;

        enum merge_kind {attach, detach};

    public:
        TaggedIMPOMaker(	Lattice const& lat_,
							iModelDescriptor const& model_desc_ )
        : lat(lat_)
        , model_desc(model_desc_)
        , length(lat.size())
        , tag_handler(model_desc_.get_tag_handler())
        , prempo(length)
        , trivial_left(prempo_key_type::trivial_left)
        , trivial_right(prempo_key_type::trivial_right)
        , leftmost_right(length)
        , finalized(false)
        {
            for (size_t p = 0; p < length; ++p) {
                prempo[p][make_pair(trivial_left,trivial_left)] = prempo_value_type(model_desc.identity_matrix_tag(p), 1.);
                prempo[p][make_pair(trivial_right,trivial_right)] = prempo_value_type(model_desc.identity_matrix_tag(p), 1.);
            }

            typename iModelDescriptor::terms_type const& terms = model_desc.hamiltonian_terms();
            std::for_each(terms.begin(), terms.end(), boost::bind(&TaggedIMPOMaker<Matrix,SymmGroup>::add_term, this, _1) );
        }

        void add_term(term_descriptor term)
        {
            //std::cout << term << std::endl;
            std::sort(term.begin(), term.end(), pos_tag_lt());
            index_type nops = term.size();
#if defined VERBOSE
			std::cout << "Term: \n" <<  term << std::endl;
#endif // VERBOSE
            switch (nops) {
                case 1:
                    add_1term(term);
                    break;
                case 2:
                    add_2term(term);
                    break;
                case 3:
                    add_3term(term);
                    break;
                case 4:
                    add_4term(term);
                    break;
                default:
                    add_nterm(term); /// here filling has to be done manually
                    break;
            }


            leftmost_right = std::min(leftmost_right, boost::get<0>(*term.rbegin()) % length);
        }

		MPOTensor<Matrix, SymmGroup> left_mpo,right_mpo;

        iMPO<Matrix, SymmGroup> create_mpo()
        {
            if (!finalized) finalize();
            iMPO<Matrix, SymmGroup> mpo( length );
#if defined VERBOSE
            std::cout << "PREMPO DUMP\n=============" << std::endl;
            for (unsigned i=0; i<prempo.size(); ++i) {
                std::cout << "++ SITE " << i << " ++" << std::endl;
                for (typename prempo_map_type::const_iterator it=prempo[i].begin(); it!= prempo[i].end(); ++it) {
                    std::cout << "-- k1 : " << it->first.first << std::endl;
                    std::cout << "-- k2 : " << it->first.second << std::endl;
                    std::cout << "-- val : " << it->second.second << "*{" << it->second.first << "}" << std::endl;
                    std::cout << std::endl;
                }
                std::cout << std::endl;
            }
#endif
            typedef std::map<prempo_key_type, index_type> index_map;
            typedef typename index_map::iterator index_iterator;
            index_map left, first_left;
            left[trivial_left] = 0;
            left[trivial_right] = 1;

            for (pos_t p = 0; p < length; ++p) {
                std::vector<tag_block> pre_tensor; pre_tensor.reserve(prempo[p].size());

                index_map right;
                index_type r = 2;
                if (p == length-1) right = first_left;
                for (typename prempo_map_type::const_iterator it = prempo[p].begin();
                     it != prempo[p].end(); ++it) {
                    prempo_key_type const& k1 = it->first.first;
                    prempo_key_type const& k2 = it->first.second;
                    prempo_value_type const& val = it->second;

                    index_iterator ll = left.find(k1);
                    if (ll == left.end() && p != 0)
                        throw std::runtime_error(std::string("k1 not found! k1=") + boost::lexical_cast<std::string>(k1));
                    else if (ll == left.end())
                        boost::tie(ll, boost::tuples::ignore) = left.insert( make_pair(k1, left.size()) );

                    index_iterator rr = right.find(k2);
                    if (k2 == trivial_left && rr == right.end())
                        boost::tie(rr, boost::tuples::ignore) = right.insert( make_pair(k2, 0) );
                    else if (k2 == trivial_right && rr == right.end())
                        boost::tie(rr, boost::tuples::ignore) = right.insert( make_pair(k2, 1) );
                    else if (rr == right.end() && p != length-1)
                        boost::tie(rr, boost::tuples::ignore) = right.insert( make_pair(k2, r++) );
                    else if (rr == right.end())
                        throw std::runtime_error(std::string("k2 not found for right boundary! k2=") + boost::lexical_cast<std::string>(k2));

                    pre_tensor.push_back( tag_block(ll->second, rr->second, val.first, val.second) );
                }
#if defined VERBOSE
                std::cout << "PREPARE SITE " << p << std::endl;
                std::cout << "..pre_tensor.size = " << pre_tensor.size() << std::endl;
                for (typename index_map::const_iterator it=left.begin(); it!=left.end(); ++it)
                    std::cout << "..left  - " << it->first << " : " << it->second << std::endl;
                for (typename index_map::const_iterator it=right.begin(); it!=right.end(); ++it)
                    std::cout << "..right - " << it->first << " : " << it->second << std::endl;
#endif // VERBOSE
                std::pair<std::size_t, std::size_t> rcd = rcdim(pre_tensor);

				mpo[p] = MPOTensor<Matrix, SymmGroup>(rcd.first, rcd.second, pre_tensor, tag_handler->get_operator_table());

				if (p == 0) {
					pre_tensor.erase(std::remove_if(pre_tensor.begin(), pre_tensor.end(), [](tag_block const& block) {
						return boost::get<0>(block) != 0;
					}), pre_tensor.end());

					left_mpo = MPOTensor<Matrix, SymmGroup>(1, rcd.second, pre_tensor, tag_handler->get_operator_table());
				}

				if (p == length - 1) {
					pre_tensor.erase(std::remove_if(pre_tensor.begin(), pre_tensor.end(), [](tag_block const& block) {
						return boost::get<1>(block) != 1;
					}), pre_tensor.end());

					right_mpo = MPOTensor<Matrix, SymmGroup>( rcd.first , 1 , pre_tensor , tag_handler->get_operator_table() );
				}




                if (p == 0) first_left = left;
                swap(left, right);
            }

			swap( mpo[0] , left_mpo );
			swap( mpo[mpo.length() - 1] , right_mpo );
			mpo.set_edge_caches( left_mpo , right_mpo );

			assert(length == 0 || mpo[0].row_dim() == mpo[length-1].col_dim());

            return mpo;
        }

    private:

        typename term_descriptor::value_type wrap_posop(typename term_descriptor::value_type posop)
        {
            boost::get<0>(posop) = boost::get<0>(posop) % length;
            return posop;
        }

        void add_1term(term_descriptor const& term)
        {
            assert(term.size() == 1);

            /// retrieve the actual operator from the tag table
            // TODO implement plus operation
            op_t current_op = tag_handler->get_op(term.operator_tag(0));
            current_op *= term.coeff;
            site_terms[term.position(0)] += current_op;
        }

        void add_2term(term_descriptor const& term)
        {
            assert(term.size() == 2);

            prempo_key_type k1 = trivial_left;
            {
                int i = 0;
                prempo_key_type k2;
                k2.pos_op.push_back(to_pair(wrap_posop(term[i+1])));
                k1 = insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), term.coeff), detach);
            }
            bool trivial_fill = !tag_handler->is_fermionic(term.operator_tag(1));
            insert_filling(term.position(0)+1, term.position(1), k1, trivial_fill); // todo: check with long-range n_i*n_j
            {
                int i = 1;
                prempo_key_type k2 = trivial_right;
                insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), 1.), detach);
            }
        }

        void add_3term(term_descriptor const& term)
        {
            assert(term.size() == 3);
            int nops = term.size();

            /// number of fermionic operators
            int nferm = 0;
            for (int i = 0; i < nops; ++i) {
                if (tag_handler->is_fermionic(term.operator_tag(i)))
                    nferm += 1;
            }

            prempo_key_type k1 = trivial_left;
            std::vector<pos_op_type> ops_left;

            /// op_0
            {
                int i = 0;
                prempo_key_type k2;
                k2.pos_op.push_back(to_pair(term[i])); // k2: applied operator
                k1 = insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), 1.), attach);

                if (tag_handler->is_fermionic(term.operator_tag(i)))
                    nferm -= 1;
                bool trivial_fill = (nferm % 2 == 0);
                insert_filling(term.position(i)+1, term.position(i+1), k1, trivial_fill);
            }
            /// op_1
            {
                int i = 1;
                prempo_key_type k2;
                k2.pos_op.push_back(to_pair(term[i+1])); // k2: future operators
                k1 = insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), term.coeff), detach);

                if (tag_handler->is_fermionic(term.operator_tag(i)))
                    nferm -= 1;
                bool trivial_fill = (nferm % 2 == 0);
                insert_filling(term.position(i)+1, term.position(i+1), k1, trivial_fill);
            }
            /// op_2
            {
                int i = 2;
                insert_operator(term.position(i), make_pair(k1, trivial_right), prempo_value_type(term.operator_tag(i), 1.), detach);
            }
        }

        void add_4term(term_descriptor const& term)
        {
            assert(term.size() == 4);
            int nops = term.size();

            /// number of fermionic operators
            int nferm = 0;
            for (int i = 0; i < nops; ++i) {
                if (tag_handler->is_fermionic(term.operator_tag(i)))
                    nferm += 1;
            }

            prempo_key_type k1 = trivial_left;
            std::vector<pos_op_type> ops_left;

            /// op_0, op_1
            for (int i = 0; i < 2; ++i) {
                ops_left.push_back(to_pair(term[i])); prempo_key_type k2(ops_left);
                k1 = insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), 1.), attach);

                if (tag_handler->is_fermionic(term.operator_tag(i)))
                    nferm -= 1;
                bool trivial_fill = (nferm % 2 == 0);
                insert_filling(term.position(i)+1, term.position(i+1), k1, trivial_fill);
            }
            /// op_2
            {
                int i = 2;
                prempo_key_type k2;
                k2.pos_op.push_back(to_pair(term[3]));
                k1 = insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), term.coeff), detach);

                if (tag_handler->is_fermionic(term.operator_tag(i)))
                    nferm -= 1;
                bool trivial_fill = (nferm % 2 == 0);
                insert_filling(term.position(i)+1, term.position(i+1), k1, trivial_fill);
            }

            /// op_3
            {
                int i = 3;
                insert_operator(term.position(i), make_pair(k1, trivial_right), prempo_value_type(term.operator_tag(i), 1.), detach);
            }
        }

        void add_nterm(term_descriptor const& term)
        {
            int nops = term.size();
            assert( nops > 2 );

            static index_type next_offset = 0;
            index_type current_offset = (next_offset++);

            prempo_key_type k1 = trivial_left;
            prempo_key_type k2(prempo_key_type::bulk_no_merge, current_offset);
            k2.pos_op.push_back( to_pair(term[nops-1]) );

            {
                int i = 0;
                insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), term.coeff), detach);
                k1 = k2;

                if (i < nops-1 && term.position(i)+1 != term.position(i+1))
                    throw std::runtime_error("for n > 4 operators filling is assumed to be done manually. the list of operators contains empty sites.");
            }


            for (int i = 1; i < nops; ++i) {
                if (i == nops-1)
                    k2 = trivial_right;

                insert_operator(term.position(i), make_pair(k1, k2), prempo_value_type(term.operator_tag(i), 1.), detach);

                if (i < nops-1 && term.position(i)+1 != term.position(i+1))
                    throw std::runtime_error("for n > 4 operators filling is assumed to be done manually. the list of operators contains empty sites.");
            }

        }

        void insert_filling(pos_t i, pos_t j, prempo_key_type k, bool trivial_fill)
        {
            bool go = true;
            while (go) {
                pos_t end = std::min(length, j);
                for (; i < end; ++i) {
                    tag_type op = (trivial_fill) ? model_desc.identity_matrix_tag(i) : model_desc.filling_matrix_tag(i);
                    std::pair<typename prempo_map_type::iterator,bool> ret = prempo[i].insert( make_pair(make_pair(k,k), prempo_value_type(op, 1.)) );
                    if (!ret.second && ret.first->second.first != op)
                        throw std::runtime_error("Pre-existing term at site "+boost::lexical_cast<std::string>(i)
                                                 + ". Needed "+boost::lexical_cast<std::string>(op)
                                                 + ", found "+boost::lexical_cast<std::string>(ret.first->second.first));
                }
                go = (j >= length);
                j -= length;
                i = 0;
            }
        }

        prempo_key_type insert_operator(pos_t p, std::pair<prempo_key_type, prempo_key_type> kk, prempo_value_type val,
                                        merge_kind merge_behavior=detach)
        {
            p = p % length;

            /// merge_behavior == detach: a new branch will be created, in case op already exist, an offset is used
            /// merge_behavior == attach: if operator tags match, keep the same branch
            std::pair<typename prempo_map_type::iterator,bool> match = prempo[p].insert( make_pair(kk, val) );
            if (merge_behavior == detach) {
                if (!match.second) {
                    std::pair<prempo_key_type, prempo_key_type> kk_max = kk;
                    kk_max.second.offset = std::numeric_limits<index_type>::max();

                    typename prempo_map_type::iterator highest_offset = prempo[p].upper_bound(kk_max);
                    highest_offset--;
                    kk.second.offset = highest_offset->first.second.offset + 1;
                    prempo[p].insert(highest_offset, make_pair(kk, val));
                }
            }
            else {
                // still slow, but seems never to be used
                while (!match.second && match.first->second != val) {
                    kk.second.offset += 1;
                    match = prempo[p].insert( make_pair(kk, val) );
                }
            }
            return kk.second;
        }

        void finalize()
        {
            /// site terms
            std::pair<prempo_key_type,prempo_key_type> kk = make_pair(trivial_left,trivial_right);
            for (typename std::map<pos_t, op_t>::const_iterator it = site_terms.begin();
                 it != site_terms.end(); ++it) {
                tag_type site_tag = tag_handler->register_op(it->second, tag_detail::bosonic);
                std::pair<typename prempo_map_type::iterator,bool> ret;
                ret = prempo[it->first].insert( make_pair( kk, prempo_value_type(site_tag,1.) ) );
                if (!ret.second)
                    throw std::runtime_error("another site term already existing!");
            }

            /// Not needed, since we set it in the constructor.
            /// fill with ident until the end
            // bool trivial_fill = true;
            // insert_filling(leftmost_right+1, length, trivial_right, trivial_fill);

            finalized = true;
        }


    private:
        Lattice const& lat;
        iModelDescriptor const& model_desc;

        pos_t length;

        boost::shared_ptr<TagHandler<Matrix, SymmGroup> > tag_handler;
        std::vector<prempo_map_type> prempo;
        prempo_key_type trivial_left, trivial_right;
        std::map<pos_t, op_t> site_terms;

        pos_t leftmost_right;
        bool finalized;
    };

}
