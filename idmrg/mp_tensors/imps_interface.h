#pragma once

/** @file
    @brief Header file declaring the (abstract) struct iMPSInterface
*/

#include "dmrg/block_matrix/indexing_stable.hpp"



/** @struct iMPSInterface
    @brief struct defining an abstract iMPS interface.
    This struct provides a common interface for both iMPS as well as iMPS::Impl classes to interact with the initializers.
*/
template<class Matrix, class SymmGroup>
struct iMPSInterface {

    typedef std::size_t size_t;
    typedef typename SymmGroup::charge charge_t;
    typedef MPSTensor<Matrix, SymmGroup> value_t;

    virtual ~iMPSInterface() {}

    virtual size_t size() const = 0;

    virtual Index<SymmGroup> const & site_dim(size_t i) const = 0;
    virtual Index<SymmGroup> const & row_dim(size_t i) const = 0;
    virtual Index<SymmGroup> const & col_dim(size_t i) const = 0;

    virtual const value_t& operator[](size_t i) const = 0;
    virtual value_t& operator[](size_t i) = 0;

    virtual const size_t& shift_site() const = 0;
    virtual size_t& shift_site() = 0;

    virtual const charge_t& shift_charge() const = 0;
    virtual charge_t& shift_charge() = 0;
};
