#pragma once

/** @file
    @brief Header file declaring the iMPS class
*/

#include <alps/numeric/matrix.hpp>
#include <alps/numeric/diagonal_matrix.hpp>

#include "dmrg/mp_tensors/mpstensor.h"
#include "dmrg/mp_tensors/boundary.h"
#include "dmrg/mp_tensors/mps.h"

#include "idmrg/mp_tensors/imps_interface.h"
#include "idmrg/mp_tensors/imps_initializers.h"



template<class Matrix, class SymmGroup>
struct imps_initializer;

/** @class iMPS
    @brief class implementing the concept of an iMPS.
*/
template<class Matrix, class SymmGroup>
class iMPS : public iMPSInterface<Matrix, SymmGroup>
{
    typedef std::vector<MPSTensor<Matrix, SymmGroup> > data_t;
	friend struct default_imps_init<Matrix, SymmGroup>;

    struct Impl;
    std::unique_ptr<Impl> pimpl_;

public:
    typedef std::size_t size_t;

    // reproducing interface of std::vector
    typedef typename data_t::size_type size_type;
    typedef typename data_t::value_type value_type;
    typedef typename data_t::iterator iterator;
	typedef typename data_t::const_iterator const_iterator;
	typedef typename data_t::reverse_iterator reverse_iterator;
    typedef typename data_t::const_reverse_iterator const_reverse_iterator;
    typedef typename MPSTensor<Matrix, SymmGroup>::scalar_type scalar_type;
	typedef Matrix matrix_t;
	typedef alps::numeric::diagonal_matrix< typename maquis::traits::real_type<Matrix>::type > real_diag_matrix_t;
	typedef block_matrix<Matrix,SymmGroup> block_matrix_t;
	typedef block_matrix<real_diag_matrix_t,SymmGroup> block_diag_matrix_t;
	typedef block_diag_matrix_t lambda_t;
	typedef MPSTensor<Matrix, SymmGroup> tensor_t;
	typedef Boundary<Matrix, SymmGroup> boundary_t;
	typedef std::vector< std::pair< size_type , block_matrix_t > > op_cont_t;
	typedef typename SymmGroup::charge charge_t;

    iMPS();
    iMPS(size_t L , const int center , const int seed = 13 );
	iMPS(size_t L , const int center , imps_initializer<Matrix, SymmGroup> & init , const int seed = 13  );

    /* Copy operations */
    iMPS(const iMPS&) = delete;
    iMPS& operator=(const iMPS&) = delete;

    /* Move operations */
    iMPS(iMPS&&);
    iMPS& operator=(iMPS&&);

    /* Destructor */
    ~iMPS();

    size_t size() const override;
    size_t length() const;
    Index<SymmGroup> const & site_dim(size_t i) const override;
    Index<SymmGroup> const & row_dim(size_t i) const override;
    Index<SymmGroup> const & col_dim(size_t i) const override;

    const value_type& operator[](size_t i) const override;
    value_type& operator[](size_t i) override;

    const lambda_t& lambda() const;
    lambda_t& lambda();

    const lambda_t & lambda_old() const;
    lambda_t & lambda_old();

    const boundary_t& right_boundary() const;
    boundary_t& right_boundary();

    const boundary_t& left_boundary() const;
    boundary_t& left_boundary();

    const size_t& center() const;
    size_t& center();

    const size_t& shift_site() const override;
    size_t& shift_site() override;

    const charge_t& shift_charge() const override;
    charge_t& shift_charge() override;

    void absorb_lambda_left();
    void absorb_lambda_right();

    void resize(size_t L);

    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    iterator begin();
    iterator end();

    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    value_type& front();
    const value_type& front() const;
    value_type& back();
    const value_type& back() const;

    data_t& data();
    const data_t& data() const;

    size_t canonization(bool b = true) const;
    void canonize(size_t center, DecompMethod method = DefaultSolver() );
    void mixed_canonize(  const size_t Mmax , const double tol , DecompMethod method = DefaultSolver() );

    block_matrix_t normalize_left();
    block_matrix_t normalize_right();
    void normalize( const size_t Mmax , const double tol );

    void move_normalization_l2r(size_t p1, size_t p2, DecompMethod method = DefaultSolver());
    void move_normalization_r2l(size_t p1, size_t p2, DecompMethod method = DefaultSolver());

    void shift_unit_cell();

    bool mixed_canonical() const;
    bool measure_ready() const;
    bool is_mixed_canonical() const;
    bool is_measure_ready() const;

    void start_boundaries( const block_matrix_t& left_gauge , const block_matrix_t& right_gauge );

    void apply(const block_matrix_t& op , const size_type i );
    void apply(const block_matrix_t& fill , const block_matrix_t& op, const size_type i);

    void save( const std::string& prefix ) const;
    void load( const std::string& prefix );

    std::string description() const;

    friend void swap(iMPS& a, iMPS& b){ std::swap(a.pimpl_,b.pimpl_); }
};
