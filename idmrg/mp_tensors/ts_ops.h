#pragma once

/** @file
    @brief Header file implementing two-site operations.
*/

#include "dmrg/mp_tensors/mpstensor.h"
#include "dmrg/mp_tensors/mpotensor.h"
#include "dmrg/block_matrix/indexing.h"
#include "dmrg/block_matrix/multi_index.h"
#include "dmrg/block_matrix/block_matrix.h"
#include "dmrg/block_matrix/block_matrix_algorithms.h"

#include "dmrg/mp_tensors/ts_ops.h"
#include "idmrg/mp_tensors/imps.h"



/** @fn void make_ts_cache_mpo( MPO<MPOMatrix, SymmGroup> const & mpo_orig,
                                MPO<MPSMatrix, SymmGroup> & mpo_out,
                                iMPS<MPSMatrix, SymmGroup> const & mps,
                                bool verbose = false )
    @brief function constructing a cache MPO for which each site is actually a compressed form of two sites. Useful when employing a two-site optimization scheme in the iDMRG algorithm.
*/
template<class MPOMatrix, class MPSMatrix, class SymmGroup>
void make_ts_cache_mpo(MPO<MPOMatrix, SymmGroup> const & mpo_orig,
                       MPO<MPSMatrix, SymmGroup> & mpo_out,
                       iMPS<MPSMatrix, SymmGroup> const & mps,
                       bool verbose = false )
{
    std::size_t L_ts = mpo_orig.length() - 1;
    mpo_out.resize(L_ts);

    bool global_table = true;
    for (int p=0; p<L_ts && global_table; ++p)
        global_table = (mpo_orig[p].get_operator_table() == mpo_orig[0].get_operator_table());

    parallel_for(/*removed...*/, std::size_t p = 0; p < L_ts; ++p)
        mpo_out[p] = make_twosite_mpo<MPOMatrix, MPSMatrix>(mpo_orig[p], mpo_orig[p+1],
                                                            mps[p].site_dim(), mps[p+1].site_dim(), global_table);

    std::size_t ntags=0;
    for (int p=0; p<mpo_out.length(); ++p) {
        ntags += mpo_out[p].get_operator_table()->size();
    }
    if( verbose )
        maquis::cout << "Total number of tags: " << ntags << std::endl;

}
