#pragma once

/** @file
    @brief Header file defining the class iMPO.
*/

#include <algorithm>

#include "dmrg/mp_tensors/mpotensor.h"
#include "dmrg/mp_tensors/mpo.h"
#include "dmrg/utils/placement.h"



/** @struct iMPO
	@brief class deriving from the MPO class and implementing the notion of an iMPO.
*/
template<class Matrix ,class SymmGroup>
class iMPO : public MPO<Matrix,SymmGroup>
{
public:

	typedef MPO<Matrix,SymmGroup> mpo_t;
	typedef MPOTensor<Matrix, SymmGroup> mpo_tensor_t;

	iMPO()
	: mpo_t(),left_(),right_(),is_ready_(false),is_finite_(true)
	{}

	iMPO( const std::size_t L )
	: mpo_t(L), left_() , right_(), is_ready_(false),is_finite_(true)
	{}

	iMPO( const std::size_t L , const mpo_tensor_t& tens , const mpo_tensor_t& left ,  const mpo_tensor_t& right )
	: mpo_t(L,tens), left_(left) , right_(right), is_ready_(true),is_finite_(true)
	{}

	void set_edge_caches( const mpo_tensor_t& left , const mpo_tensor_t& right );

	mpo_tensor_t& left() { return left_; }
	const mpo_tensor_t& left() const { return left_; }

	mpo_tensor_t& right() { return right_; }
	const mpo_tensor_t& right() const { return right_; }

	void set_edges()
	{
		swap( this->front() , left_ );
		swap( this->back() , right_ );
		//this->front() = left();
		//this->back() = right();

		is_finite_ = !is_finite_;
	}

	void make_finite()
	{
		if( is_finite_ )
			return;

		set_edges();
	}

	void make_infinite()
	{
		if( !is_finite_ )
			return;

		set_edges();
	}

	void match_edge_placements()
	{
		right().placement_l = this->back().placement_l;
		right().placement_r.resize( right().col_dim() , -1 );
		get_right_placement( right() , right().placement_l , right().placement_r );

		left().placement_r = this->front().placement_r;
		left().placement_l.resize( left().row_dim() , -1 );
		get_left_placement( left() , left().placement_l , left().placement_r );
	}

	void info() const
	{
		std::cout << "iMPO info\n\n";

		auto it = this->begin();
		while( it != this->end() )
			std::cout << "Dimensions: " << it->row_dim() << " " << it->col_dim() << '\n';
	}

private:
	mpo_tensor_t left_,right_; /// Members always keeping clean versions of the bulk MPOs for the edges
	bool is_ready_; /// Member specifying whether left_ and right_ have been properly loaded.
	bool is_finite_; /// Member containing the configuration of the iMPO (either finite or infinite).
};

template<class Matrix, class SymmGroup>
std::vector<std::vector<int> > construct_placements(const iMPO<Matrix, SymmGroup>& mpo){
    std::vector<std::vector<int> > placements(mpo.length()+1);
    std::vector<std::pair<std::vector<int>, std::vector<int> > > exceptions(mpo.length()+1);

	for( int i = 0 ; i < mpo[0].row_dim() ; ++i )
		placements[0].push_back(i);

    for(int s = 0; s < mpo.length(); s++){
        placements[s+1].resize(mpo[s].col_dim(), -1);

        for(int b1 = 0; b1 < placements[s].size(); b1++)
        for(int b2 = 0; b2 < placements[s+1].size(); b2++){
            if(mpo[s].has(b1,b2)){
                make_consistent(placements[s+1], b2, exceptions[s+1].second, placements[s], b1, exceptions[s].first);
            }
        }
        make_complete(placements[s+1]);
        mpo[s].placement_l = placements[s];
        mpo[s].placement_r = placements[s+1];
    }
    return placements;
}
template<class Matrix ,class SymmGroup>
void iMPO<Matrix,SymmGroup>::set_edge_caches( const mpo_tensor_t& left , const mpo_tensor_t& right )
{
	this->left() = left;
	this->right() = right;

	is_ready_ = true;
}

template <class Matrix, class SymmGroup>
void print_mpo(std::ostream & os, iMPO<Matrix, SymmGroup> const& mpo, bool print_each_table=false)
{
    for (int p = 0; p < mpo.size(); ++p) {
        os << "+++ SITE " << p << " +++" << "\n++++++++++++++" << std::endl;
        for (int b1 = 0; b1 < mpo[p].row_dim(); ++b1) {
            for (int b2 = 0; b2 < mpo[p].col_dim(); ++b2) {
                if (mpo[p].has(b1, b2)) os << mpo[p].tag_number(b1,b2) << " ";
                else os << ". ";
            }
            os << std::endl;
        }

        os << std::endl;

        if (print_each_table) {
            typename MPOTensor<Matrix, SymmGroup>::op_table_ptr op_table = mpo[p].get_operator_table();
            for (unsigned tag=0; tag<op_table->size(); ++tag) {
                os << "TAG " << tag << std::endl;
                os << " * op :\n" << (*op_table)[tag] << std::endl;
            }
        }
    }

    os << "+++ SITE " << "Left" << " +++" << "\n++++++++++++++" << std::endl;
    for (int b1 = 0; b1 < mpo.left().row_dim(); ++b1) {
        for (int b2 = 0; b2 < mpo.left().col_dim(); ++b2) {
            if (mpo.left().has(b1, b2)) os << mpo.left().tag_number(b1,b2) << " ";
            else os << ". ";
        }
        os << std::endl;
    }

    os << "+++ SITE " << "Right" << " +++" << "\n++++++++++++++" << std::endl;
    for (int b1 = 0; b1 < mpo.right().row_dim(); ++b1) {
        for (int b2 = 0; b2 < mpo.right().col_dim(); ++b2) {
            if (mpo.right().has(b1, b2)) os << mpo.right().tag_number(b1,b2) << " ";
            else os << ". ";
        }
        os << std::endl;
    }

    os << std::endl;

    if (!print_each_table) {
        os << "+++ OPERATORS +++" << "\n+++++++++++++++++" << std::endl;
        typename MPOTensor<Matrix, SymmGroup>::op_table_ptr op_table = mpo[0].get_operator_table();
        for (unsigned tag=0; tag<op_table->size(); ++tag) {
            os << "TAG " << tag << std::endl;
            os << " * op :\n" << (*op_table)[tag] << std::endl;
        }
    }


}
