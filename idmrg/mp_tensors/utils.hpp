#pragma once

/** @file
    @brief Header file defining a check_equal_mps function for use with iMPS.
*/



#include "imps.h"



/** @fn void check_equal_mps (iMPS<Matrix, SymmGroup> const & mps1, iMPS<Matrix, SymmGroup> const & mps2)
    @brief function verifying equality of two iMPS.
    @param [in] mps1 first iMPS
    @param [in] mps2 first iMPS
	@remark The checks performed by this function are more a plausibility check than a definitive assertion regarding the equality of the two iMPS states.
*/
template <class Matrix, class SymmGroup>
void check_equal_mps (iMPS<Matrix, SymmGroup> const & mps1, iMPS<Matrix, SymmGroup> const & mps2)
{
    // Length
    if (mps1.length() != mps2.length())
        throw std::runtime_error("Length doesn't match.");

    for (int i=0; i<mps1.length(); ++i)
        try {
            mps1[i].check_equal(mps2[i]);
        } catch (std::exception & e) {
            maquis::cerr << "Problem on site " << i << ":" << e.what() << std::endl;
            exit(1);
        }

	if( mps1.center() != mps2.center() )
	{
        maquis::cerr << "Problem with center labels!" << std::endl;
        exit(1);
	}
}
