#pragma once

/** @file
    @brief Header file containing all iMPS initializers.
*/

#include "dmrg/mp_tensors/mpstensor.h"
#include "dmrg/mp_tensors/mps_initializers.h"

#include "idmrg/mp_tensors/imps_sectors.h"
#include "idmrg/mp_tensors/imps_interface.h"

template<class Matrix, class SymmGroup>
class iMPS;

namespace idmrg{
    template<class Matrix, class SymmGroup>
    struct iModelDescriptor;
}

template<class Matrix, class SymmGroup>
struct imps_initializer
{
    virtual ~imps_initializer() {}
    virtual void operator()(iMPSInterface<Matrix, SymmGroup>& imps) = 0;
};

/** @struct default_imps_init
    @brief construct an iMPS in default mode.
    Initializes the iMPS by first computing all possible charge sectors and degeneracies for each bond and then filling all tensors with random numbers.
*/
template<class Matrix, class SymmGroup>
struct default_imps_init : public imps_initializer<Matrix, SymmGroup>
{
	typedef typename SymmGroup::charge charge_t;
    typedef idmrg::iModelDescriptor<Matrix, SymmGroup> iModelDescriptor;

    default_imps_init(	BaseParameters & parms,
                     	iModelDescriptor& model_desc	)
    : init_bond_dimension(parms["init_bond_dimension"])
    , phys_dims(model_desc.indices())
    , site_type(model_desc.site_types())
	, shift_charge(model_desc.shift_charge())
    , verbose_( parms["verbose"] )
    {
		// We need to include the presence of the shift site in the description
		// This type will be referenced internally by a type which is equal to the largest physical type plus one.

		phys_dims.push_back( model_desc.shift_index() );
		site_type[ model_desc.shift_site() ] = model_desc.shift_type();
    }

    void operator()(iMPSInterface<Matrix, SymmGroup>& mps)
    {
        init_sectors(mps , true);
    }

    void init_sectors(iMPSInterface<Matrix, SymmGroup>& mps, bool fillrand = true, typename Matrix::value_type val = 0)
    {
        const std::size_t L = mps.size();

		// For an iMPS both ends need to have matching indices (we'll work with a shift charge starting at the central site)

        std::vector<Index<SymmGroup> > allowed = idmrg::allowed_sectors(site_type , phys_dims , init_bond_dimension);

#ifndef NDEBUG
            for( int i = 0 ; i < allowed.size() ; ++i )
                std::cout << "Allowed states (bond " << i << "):\n" << allowed[i] << "\n\n";
#endif

        for(std::size_t i = 0; i < L; ++i) {

#ifndef NDEBUG
                std::cout << "Site = " << i << '\n';
                std::cout << "Physical index = " << phys_dims[site_type[i]] << '\n';
                std::cout << "Allowed left = " << allowed[i] << '\n';
                std::cout << "Allowed right = " << allowed[i+1] << '\n';
                std::cout << "Fillrand = " << fillrand << '\n';
#endif

            mps[i] = MPSTensor<Matrix, SymmGroup>(phys_dims[site_type[i]], allowed[i], allowed[i+1], fillrand, val);
            mps[i].divide_by_scalar(mps[i].scalar_norm());
        }

		mps.shift_charge()=shift_charge;
    }

    const std::size_t init_bond_dimension;
    std::vector<Index<SymmGroup> > phys_dims;
    std::vector<int> site_type;
	charge_t shift_charge;
    bool verbose_;
};
