#pragma once

/** @file
    @brief Header file defining common imps-impo operations
*/

#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/mpo.h"

#include "dmrg/mp_tensors/special_mpos.h"
#include "dmrg/mp_tensors/contractions.h"

#include "dmrg/utils/utils.hpp"
#include "utils/traits.hpp"

#include "idmrg/mp_tensors/imps.h"



/** @fn double norm(	const int L ,
						const lambda_t& inverse_lambda ,
						const iMPS<Matrix, SymmGroup>& mps )
    @brief computes the norm of an iMPS
	@param [in] L length of the iMPS unit cell
	@param [in] inverse_lambda matrix of inverse Schmidt values
	@param [in] mps an iMPS
	@pre parameter @p mps shall be initialized for usage in the thermodynamic limit, i.e. the left and right boundary members have been initialized.
*/
template<class Matrix, class SymmGroup>
double norm(	const int L ,
				const typename iMPS<Matrix,SymmGroup>::lambda_t& inverse_lambda ,
				const iMPS<Matrix, SymmGroup>& mps )
{
	// We will assume that the iMPS was in mixed canonical form and the weights were absorbed on the center site
	typedef MPSTensor<Matrix, SymmGroup> mps_tensor_t;
	typedef block_matrix<Matrix, SymmGroup> block_matrix_t;

	block_matrix_t left_boundary = mps.left_boundary()[0];

	int uc = 0;
	for( int i = 0 ; i < L ; ++i )
	{
		if( uc != i / mps.length() ) // Crossed a UC boundary -> add inverse weights
		{
			block_matrix<Matrix, SymmGroup> temp;
			gemm( left_boundary , inverse_lambda , temp );
			gemm( inverse_lambda , temp , left_boundary );
		}

		uc = i / mps.length();
		int uc_i = i % mps.length();

		left_boundary = contraction::overlap_left_step( mps[uc_i] , mps[uc_i] , left_boundary );
	}

	block_matrix<Matrix, SymmGroup> temp;
	gemm( left_boundary , transpose( mps.right_boundary()[0] ) , temp );

	return std::real(temp.trace());
}

/** @fn scalar_type expval(	const lambda_t& inverse_lambda ,
							const iMPS<Matrix, SymmGroup>& mps ,
							const MPO<Matrix, SymmGroup>& mpo )
    @brief computes the expectaction value of the operator defined by the MPO
	@param [in] inverse_lambda length of the iMPS unit cell
	@param [in] mps an iMPS state on which to perform the measurement
	@param [in] mpo an MPO representing an arbitrary observable
*/
template<class Matrix, class SymmGroup>
typename iMPS<Matrix,SymmGroup>::scalar_type expval( 	const typename iMPS<Matrix,SymmGroup>::lambda_t& inverse_lambda ,
														const iMPS<Matrix, SymmGroup>& mps ,
														const MPO<Matrix, SymmGroup>& mpo )
{
	// We will assume that the iMPS was in mixed canonical form and the weights were absorbed on the center site
	typedef MPSTensor<Matrix, SymmGroup> mps_tensor_t;
	typedef Boundary<Matrix, SymmGroup> boundary_t;

	boundary_t left_boundary = mps.left_boundary();

	for( int uc = 0, i = 0 ; i < mpo.length() ; ++i )
	{
		if( uc != i / mps.length() ) // Crossed a UC boundary -> add inverse weights
		{
			block_matrix<Matrix, SymmGroup> temp;
			for( int i = 0 ; i < left_boundary.aux_dim() ; ++i )
			{
				gemm( left_boundary[i] , inverse_lambda , temp );
				gemm( inverse_lambda , temp , left_boundary[i] );
			}
		}

		uc = i / mps.length();
		int uc_i = i % mps.length();

		left_boundary = contraction::overlap_mpo_left_step( mps[uc_i] , mps[uc_i] , left_boundary , mpo[i] );
	}

	assert( mps.right_boundary().aux_dim() == 1 );
	assert( mps.right_boundary().aux_dim() == left_boundary.aux_dim() );


	block_matrix<Matrix, SymmGroup> temp;
	gemm( left_boundary[0] , transpose( mps.right_boundary()[0] ) , temp );

	return temp.trace();
}
