#pragma once

/** @file
    @brief Header file defining the iMPS implementation.
*/

#include <memory>
#include <complex>
#include <limits>
#include <cstdlib>
#include <iomanip>

#include <boost/filesystem.hpp>

#include "alps/numeric/matrix/matrix.hpp"

#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/contractions.h"
#include "dmrg/mp_tensors/mpstensor.h"
#include "dmrg/block_matrix/indexing_stable.hpp"
#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "dmrg/utils/BaseParameters.h"
#include "dmrg/utils/DmrgParameters.h"
#include "dmrg/utils/archive.h"
#include "dmrg/block_matrix/block_matrix.h"

#include "idmrg/block_matrix/block_matrix_ietl.hpp"
#include "idmrg/mp_tensors/imps_interface.h"
#include "idmrg/mp_tensors/imps.h"



/** @class iMPS::Impl
    @brief class deriving from the iMPSInterface class and implementing the iMPS.
*/
template<class Matrix, class SymmGroup>
struct iMPS<Matrix,SymmGroup>::Impl : public iMPSInterface<Matrix, SymmGroup> {

	size_t center_;  							/// Central bond in the iMPS unit cell.
	size_t shift_site_;							/// Location of index containing shifted charges.
	mutable charge_t shift_charge_;				/// Shift charge containing sector being targeted.
	size_t uc_size_;							/// Final unit cell size
    lambda_t lambda_,lambda_old_;				/// Schmidt values on the two last scales.
	boundary_t left_boundary_,right_boundary_;	/// Boundaries for the unit cell(s),
	data_t data_; 								/// Container carrying MPS tensors making up the unit cell.
    mutable size_t canonized_i_;				/// Location of current canonization center.
	mutable bool mixed_canonical_;				/// Is the imps in mixed canonical form? i.e. needs to have weights pushed in.
	bool measure_ready_;						/// Flag specifying whether or not the gauge boundaries have been initialized for measuring.

    Impl()
    : center_(std::numeric_limits<size_t>::max())
    , shift_site_(center_)
    , shift_charge_()
    , uc_size_()
    , lambda_()
    , lambda_old_()
    , left_boundary_()
    , right_boundary_()
    , canonized_i_(std::numeric_limits<size_t>::max())
    , mixed_canonical_(false)
    , measure_ready_(false)
    {}

    Impl(size_t L , const int center , const int seed )
    : center_( center < 0 ? L/2 - 1 : center )
    , shift_site_(center_)
    , shift_charge_()
    , uc_size_(L)
    , lambda_()
    , lambda_old_()
    , left_boundary_()
    , right_boundary_()
    , data_(L)
    , canonized_i_(std::numeric_limits<size_t>::max())
    , mixed_canonical_(false)
    , measure_ready_(false)
    {}

    Impl(size_t L , const int center , imps_initializer<Matrix, SymmGroup> & init , const int seed )
    : center_( center < 0 ? L/2 - 1 : center )
    , shift_site_(center_)
    , shift_charge_()
    , uc_size_(L)
    , lambda_()
    , lambda_old_()
    , left_boundary_()
    , right_boundary_()
    , data_(L)
    , canonized_i_(std::numeric_limits<size_t>::max())
    , mixed_canonical_(false)
    , measure_ready_(false)
    {
    	std::srand(seed);

    	init(*this);

    	if( center < 0 || center >= L )
    		throw std::runtime_error("Provided center site does not have a valid value! (Must be between 0 and L-1)");

        // MD: this is actually important
        //     it turned out, this is also quite dangerous: if a block is 1x2,
        //     normalize_left will resize it to 1x1
        //     init() should take care of it, in case needed. Otherwise some
        //     adhoc states will be broken (e.g. identity MPS)
        // for (int i = 0; i < L; ++i)
        //     (*this)[i].normalize_left(DefaultSolver());

    }

    Impl( Impl& impl ) = delete;
    Impl& operator=( Impl& impl ) = delete;

    Impl( Impl&& impl ) = default;
    Impl& operator=( Impl&& impl ) = default;

    size_t size() const override { return data_.size(); }
    size_t length() const { return size(); }

    const value_type& operator[](size_t i) const override { return data_[i]; }
    value_type& operator[](size_t i) override
    {
    	mixed_canonical_ = false;
        if (i != canonized_i_)
            canonized_i_=std::numeric_limits<size_t>::max();
        return data_[i];
    }

    Index<SymmGroup> const & site_dim(size_t i) const override { return data_[i].site_dim(); }
    Index<SymmGroup> const & row_dim(size_t i) const override { return data_[i].row_dim(); }
    Index<SymmGroup> const & col_dim(size_t i) const override { return data_[i].col_dim(); }

    lambda_t const & lambda() const { return lambda_; }
	lambda_t & lambda() { return lambda_; }

	lambda_t const & lambda_old() const { return lambda_old_; }
	lambda_t & lambda_old() { return lambda_old_; }

	boundary_t const & right_boundary() const { return right_boundary_; }
	boundary_t & right_boundary() { return right_boundary_; }

	boundary_t const & left_boundary() const { return left_boundary_; }
	boundary_t & left_boundary() { return left_boundary_; }

    size_t const & center() const { return center_; }
    size_t & center() { return center_; }

    size_t const & shift_site() const override { return shift_site_; }
    size_t & shift_site() override { return shift_site_; }

    const charge_t& shift_charge() const override { return shift_charge_; }
    charge_t& shift_charge() override { return shift_charge_; }

	void absorb_lambda_left()
	{
		(*this)[center()].multiply_from_right( lambda() );
		mixed_canonical_ = false;
	}

	void absorb_lambda_right()
	{
		(*this)[center()+1].multiply_from_left( lambda() );
		mixed_canonical_ = false;
	}

    void resize(size_t L)
    {
        // if canonized_i_ < L and L < current L, we could conserve canonized_i_
    	mixed_canonical_ = false;
        canonized_i_=std::numeric_limits<size_t>::max();
        data_.resize(L);
    }

    const_iterator begin() const {return data_.begin();}
    const_iterator end() const {return data_.end();}
    const_iterator cbegin() const {return data_.cbegin();}
    const_iterator cend() const {return data_.cend();}
    iterator begin() {return data_.begin();}
    iterator end() {return data_.end();}

	reverse_iterator rbegin() {return data_.rbegin();}
	reverse_iterator rend() {return data_.rend();}
	const_reverse_iterator crbegin() const {return data_.crbegin();}
	const_reverse_iterator crend() const {return data_.crend();}

	value_type& front() { return data_.front(); }
	const value_type& front() const { return data_.front(); }
    value_type& back() { return data_.back(); }
	const value_type& back() const { return data_.back(); }

	data_t& data() { return data_; }
	const data_t& data() const { return data_; }

    size_t canonization(bool search) const
    {
        if (!search)
            return canonized_i_;

        size_t center = ((*this)[0].isleftnormalized()) ? 1 : 0;
        for (size_t i=1; i<length(); ++i) {
            if (!(*this)[i].isnormalized() && center != i) {
    			mixed_canonical_ = false;
                canonized_i_ = std::numeric_limits<size_t>::max();
                return canonized_i_;
            } else if ((*this)[i].isleftnormalized() && center == i)
                center = i+1;
            else if ((*this)[i].isleftnormalized()) {
                canonized_i_ = std::numeric_limits<size_t>::max();
                return canonized_i_;
            }
        }
        if (center == length())
            center = length()-1;

        canonized_i_ = center;
        return canonized_i_;
    }

    block_matrix<Matrix, SymmGroup> normalize_left()
    {
    	if(mixed_canonical_)
    		absorb_lambda_left();

        canonize(length()-1);
        // now state is: A A A A A A M
        block_matrix<Matrix, SymmGroup> t = (*this)[length()-1].normalize_left(DefaultSolver());
        // now state is: A A A A A A A
    	mixed_canonical_ = false;
        canonized_i_ = length()-1;

    	return t; /// We return this matrix for the purpose of monitoring convergence
    }

    block_matrix<Matrix, SymmGroup> normalize_right()
    {
    	if(mixed_canonical_)
    		absorb_lambda_left();

        canonize(0);
        // now state is: M B B B B B B
        block_matrix<Matrix, SymmGroup> t = (*this)[0].normalize_right(DefaultSolver());
        // now state is: B B B B B B B
    	mixed_canonical_ = false;
        canonized_i_ = 0;
    	return t;
    }

    void normalize( const size_t Mmax , const double tol )
    {
        mixed_canonize( Mmax , tol );
        // now state is: A A A L B B B

    	lambda() /= lambda().norm();

    	absorb_lambda_left();
    }

    // input:  M  M  M  M  M  M  M
    //  (idx)        c
    // output: A  A  M  B  B  B  B
    void canonize(std::size_t site, DecompMethod method = DefaultSolver())
    {
    	if( mixed_canonical_ )
    		absorb_lambda_left();

    	if (canonized_i_ == site)
            return;

        if (canonized_i_ < site)
            move_normalization_l2r(canonized_i_, site, method);
        else if (canonized_i_ < length())
            move_normalization_r2l(canonized_i_, site, method);
        else {
            move_normalization_l2r(0, site, method);
            move_normalization_r2l(length()-1, site, method);
        }
    	mixed_canonical_ = false;
        canonized_i_ = site;
    }

    template<class Mat , class DiagMat>
    Mat diag_to_rect( DiagMat& md )
    {
    	typedef typename Mat::diagonal_iterator matrix_diag_iterator;
    	typedef typename DiagMat::diagonal_iterator diag_matrix_diag_iterator;

    	matrix_diag_iterator it1,it1e;
    	diag_matrix_diag_iterator it2,it2e;

    	Mat mr( md.num_rows() , md.num_cols() );

    	std::tie(it2,it2e) = md.diagonal();
    	std::tie(it1,it1e) = mr.diagonal();

    	while( it2 != it2e )
    		*it1++ = *it2++;

    	return mr;
    }

    template<class Mat,class DiagMat,class SG>
    void block_diag_to_rect( block_matrix<DiagMat, SG> & diag , block_matrix<Mat, SG> & rect )
    {
    	rect.clear();

    	const Index<SG>& index = diag.left_basis();

    	typedef typename Index<SG>::const_iterator const_iter_t;
    	const_iter_t it = index.begin();
    	const_iter_t it_e = index.end();

    	while( it != it_e )
    	{
    		int block_label = diag.find_block( it->first , it->first );
    		rect.insert_block( diag_to_rect<Mat>( diag[block_label] ) , it->first , it->first );
    		it++;
    	}
    }

    void mixed_canonize( const size_t Mmax , const double tol , DecompMethod method = DefaultSolver() )
    {
        if ( mixed_canonical_ )
            return;

    	canonize( center() );

    	(*this)[center()].make_left_paired();

    	block_matrix<Matrix,SymmGroup> U,V;
    	typename iMPS<Matrix,SymmGroup>::block_diag_matrix_t SD;
    	truncation_results trunc = svd_truncate( (*this)[center()].data() , U , V , SD , tol , Mmax , false );

    	block_diag_to_rect( SD , lambda() );

    	// Modify tensors around center bond.
    	// Weights are formally missing from MPS and need to be reabsorbed!!!
    	// Redefine lambda() to contain the schmidt values to reach mixed canonical form
    	(*this)[center()] = MPSTensor<Matrix,SymmGroup>( (*this)[center()].site_dim() , (*this)[center()].row_dim() , U.right_basis() , U , LeftPaired );

    	(*this)[center()+1].multiply_from_left(V);

    	lambda() *= (1./lambda().norm());

        #ifndef NDEBUG
    	std::cout << "Norm (preparation): " << lambda().norm() << '\n';
        #endif
    	mixed_canonical_ = true;
        canonized_i_ = center();
    }

    // input:  M  M  M  L  M  M  M  M
    //  (idx)     p1       p2
    // output: M  A  A  A  M  M  M  M
    void move_normalization_l2r(size_t p1, size_t p2, DecompMethod method = DefaultSolver())
    {
    	mixed_canonical_ = false;

        size_t tmp_i = canonized_i_;
        for (int i = p1; i < std::min(p2, length()); ++i)
        {
            if ((*this)[i].isleftnormalized())
    			continue;

            block_matrix<Matrix, SymmGroup> t = (*this)[i].normalize_left(method);
            if (i < length()-1) {
                (*this)[i+1].multiply_from_left(t);
                (*this)[i+1].divide_by_scalar((*this)[i+1].scalar_norm());
            }
        }
        if (tmp_i == p1)
            canonized_i_ = p2;
        else
            canonized_i_ = std::numeric_limits<size_t>::max();
    }

    // input:  M  M  M  L  M  M  M  M
    //  (idx)     p2       p1
    // output: M  M  B  B  B  M  M  M
    void move_normalization_r2l(size_t p1, size_t p2, DecompMethod method = DefaultSolver())
    {
    	mixed_canonical_ = false;

        size_t tmp_i = canonized_i_;
        for (int i = p1; i > static_cast<int>(std::max(p2, size_t(0))); --i)
        {
            if ((*this)[i].isrightnormalized())
                continue;
            block_matrix<Matrix, SymmGroup> t = (*this)[i].normalize_right(method);

            if (i > 0) {
                (*this)[i-1].multiply_from_right(t);
                (*this)[i-1].divide_by_scalar((*this)[i-1].scalar_norm());
            }
        }
        if (tmp_i == p1)
            canonized_i_ = p2;
        else
            canonized_i_ = std::numeric_limits<size_t>::max();
    }

    void shift_unit_cell()
    {
    	// Place right half of unit cell in the left half and viceversa
    	std::rotate( this->begin() , this->begin() + center_ + 1 , this->end() );

    	center() = (this->length() - 1) - (center() + 1);

    	if( shift_site() == center() )
    		shift_site() = this->length() - 1;
    	else
    		shift_site() = center();
    }

    bool mixed_canonical() const { return mixed_canonical_; }
    bool measure_ready() const { return measure_ready_; }

    void start_boundaries(  block_matrix<Matrix, SymmGroup> const& left_gauge ,
    	 					block_matrix<Matrix, SymmGroup> const& right_gauge )
    {
    	assert( left_gauge.left_basis() == right_gauge.right_basis() );

    	const Index<SymmGroup>& virtual_index = left_gauge.left_basis();

    	typedef typename Index<SymmGroup>::const_iterator const_iter_t;
    	const_iter_t it = virtual_index.begin();
    	const_iter_t it_e = virtual_index.end();

    	left_boundary()[0].clear();
    	right_boundary()[0].clear();
    	while( it != it_e )
    	{
    		left_boundary()[0].insert_block( Matrix::identity_matrix( virtual_index.size_of_block( it->first ) ) , it->first , it->first );
    		right_boundary()[0].insert_block( Matrix::identity_matrix( virtual_index.size_of_block( it->first ) ) , it->first , it->first );
    		it++;
    	}

        //TODO: Figure out problem involving transpose()
    	// We load the gauge matrices from the beginning since these will always be there no matter what we measure
    	block_matrix<Matrix, SymmGroup> temp;

    	gemm( left_boundary()[0] , left_gauge , temp );
		auto lg = left_gauge; lg.adjoint_inplace();
    	gemm( lg , temp , left_boundary()[0] );

		auto trg = right_gauge; trg.transpose_inplace();

    	gemm( right_boundary()[0] , trg , temp );
    	gemm( conjugate( right_gauge ) , temp , right_boundary()[0] );

    	measure_ready_ = true;
    }

    void apply(block_matrix<Matrix, SymmGroup> const& op, typename iMPS<Matrix, SymmGroup>::size_type p)
    {
        typedef typename SymmGroup::charge charge;
        using std::size_t;

        /// Compute (and check) charge difference
        charge diff = SymmGroup::IdentityCharge;
        if (op.n_blocks() > 0)
            diff = SymmGroup::fuse(op.right_basis()[0].first, -op.left_basis()[0].first);
        for (size_t n=0; n< op.n_blocks(); ++n) {
            if ( SymmGroup::fuse(op.right_basis()[n].first, -op.left_basis()[n].first) != diff )
                throw std::runtime_error("Operator not allowed. All non-zero blocks have to provide same `diff`.");
        }

        /// Apply operator
        (*this)[p] = contraction::multiply_with_op((*this)[p], op);

        /// Propagate charge difference
        for (size_t i=p+1; i<length(); ++i) {
            (*this)[i].shift_aux_charges(diff);

        }
    }

    void apply(block_matrix<Matrix, SymmGroup> const& fill, block_matrix<Matrix, SymmGroup> const& op, typename iMPS<Matrix, SymmGroup>::size_type p)
    {
        for (size_t i=0; i<p; ++i) {
            (*this)[i] = contraction::multiply_with_op((*this)[i], fill);
        }
        apply(op, p);
    }
    /*
    void swap(Impl& a, Impl& b)
    {
        swap(a.center_,b.center_);
        swap(a.shift_site_,b.shift_site_);
        // shift_charg emissing (not directly swappable)
        swap(a.lambda_,b.lambda_);
        swap(a.lambda_old_,b.lambda_old_);
        swap(a.left_boundary_,b.left_boundary_);
        swap(a.right_boundary_,b.right_boundary_);
        swap(a.data_, b.data_);
        swap(a.canonized_i_, b.canonized_i_);
        swap(a.mixed_canonical_,b.mixed_canonical_);
        swap(a.measure_ready_,b.measure_ready_);
    }
    */
    void save(const std::string& prefix) const
    {
    	{
    		std::string fname = prefix + "/lambda.h5.new";
    		storage::archive ar(fname , "w");

    		ar["/lambda"] << lambda_;
    		ar["/lambda_old"] << lambda_old_;
    	}

        size_t loop_max = length();
    	auto mps_it = begin();
        parallel_for(/*removed...*/, std::size_t k = 0; k < loop_max; ++k){
            const std::string fname = prefix + "/mps_" + std::to_string(k) + ".h5.new";
            storage::archive ar(fname, "w");
            ar["/tensor"] << *(mps_it + k);
        }

    	{
    		std::string fname = prefix + "/imps_boundaries.h5.new";
    		storage::archive ar(fname , "w");

    		ar["/left_boundary"] << left_boundary_;
    		ar["/right_boundary"] << right_boundary_;
    	}

    	// We need to do this since accesing the mps tensors above will falsedly modify the canonical flags of the state
    	{
    		std::string fname = prefix + "/data.h5.new";
    		storage::archive ar(fname , "w");

    		ar["/center"] << center_;
    		ar["/shift_site"] << shift_site_;
    		ar["/shift_charge"] << shift_charge_;
    		ar["/uc_size"] << uc_size_;
    		ar["/canonized_i"] << canonized_i_;
    		ar["/mixed_canonical"] << mixed_canonical_;
    		ar["/measure_ready_"] << measure_ready_;
    		ar["/canonized_i"] << canonized_i_;
    	}

    	{
    		std::string fname = prefix + "/lambda.h5";
    		boost::filesystem::rename(fname+".new", fname);
    	}

    	{
    		std::string fname = prefix + "/imps_boundaries.h5";
    		boost::filesystem::rename(fname+".new", fname);
    	}

    	{
    		std::string fname = prefix + "/data.h5";
    		boost::filesystem::rename(fname+".new", fname);
    	}

        parallel_for(/*removed...*/, std::size_t k = 0; k < loop_max; ++k){
            const std::string fname = prefix + "/mps_" + std::to_string(k) + ".h5";
            boost::filesystem::rename(fname+".new", fname);
        }
    }

    void load( std::string const& prefix )
    {
        using boost::filesystem::exists;
    	exists( prefix + "/data.h5" );
    	exists( prefix + "/imps_boundaries.h5" );
    	exists( prefix + "/lambda.h5" );

        /// get size of MPS
        std::size_t L = 0;
        while( exists( prefix + "/mps_" + std::to_string(++L) + ".h5" ) )
    	{}

    	{
    		std::string fname = prefix+"/lambda.h5";
    		storage::archive ar(fname);

    		ar["/lambda"] >> lambda_;
    		ar["/lambda_old"] >> lambda_old_;
    	}
    	{
    		std::string fname = prefix+"/imps_boundaries.h5";
    		storage::archive ar(fname);

    		ar["/left_boundary"] >> left_boundary_;
    		ar["/right_boundary"] >> right_boundary_;
    	}

        /// load tensors
    	resize(L);
        size_t loop_max = length();
    	auto imps_it = begin();
        semi_parallel_for(/*removed...*/, std::size_t k = 0; k < loop_max; ++k){
            std::string fname = prefix+"/mps_"+std::to_string(k)+".h5";
            storage::archive ar(fname);

    		MPSTensor<Matrix, SymmGroup> tmp(Index<SymmGroup>(),Index<SymmGroup>(),Index<SymmGroup>(),false);

            ar["/tensor"] >> tmp;

    		(imps_it+k)->swap_with(tmp);
        }

    	{
    		std::string fname = prefix+"/data.h5";
    		storage::archive ar(fname);

    		ar["/center"] >> center_;
    		ar["/shift_site"] >> shift_site_;
    		ar["/shift_charge"] >> shift_charge_;
    		ar["/uc_size"] >> uc_size_;
    		ar["/canonized_i"] >> canonized_i_;
    		ar["/mixed_canonical"] >> mixed_canonical_;
    		ar["/measure_ready_"] >> measure_ready_;
    	}
    }

    std::string description() const
    {
        std::ostringstream oss;
        for (int i = 0; i < length(); ++i)
        {
            oss << "iMPS site " << i << std::endl;
            oss << (*this)[i].row_dim() << std::endl;
            oss << "Sum: " << (*this)[i].row_dim().sum_of_sizes() << std::endl;
            oss << (*this)[i].col_dim() << std::endl;
            oss << "Sum: " << (*this)[i].col_dim().sum_of_sizes() << std::endl;
        }
        return oss.str();
    }
};

/* iMPS definitions */

template <class M, class SG>
iMPS<M,SG>::iMPS():
pimpl_(new iMPS<M,SG>::Impl())
{}

template <class M, class SG>
iMPS<M,SG>::iMPS(size_t L , const int center , const int seed ):
pimpl_(new iMPS<M,SG>::Impl(L,center,seed))
{}

template <class M, class SG>
iMPS<M,SG>::iMPS(size_t L , const int center , imps_initializer<M, SG> & init , const int seed  ):
pimpl_(new iMPS<M,SG>::Impl(L,center,init,seed))
{}

template <class M, class SG>
iMPS<M,SG>::iMPS(iMPS<M,SG>&& imps):
pimpl_(std::move(imps.pimpl_))
{}

template <class M, class SG>
iMPS<M,SG>& iMPS<M,SG>::operator=(iMPS<M,SG>&& imps)
{
    assert(this != &imps);
    pimpl_ = std::move(imps.pimpl_);

    return *this;
}

template <class M, class SG>
iMPS<M,SG>::~iMPS() = default;

template <class M, class SG>
size_t iMPS<M,SG>::size() const { return pimpl_->data().size(); }
template <class M, class SG>
size_t iMPS<M,SG>::length() const { return size(); }
template <class M, class SG>
Index<SG> const & iMPS<M,SG>::site_dim(size_t i) const { return (*pimpl_)[i].site_dim(); }
template <class M, class SG>
Index<SG> const & iMPS<M,SG>::row_dim(size_t i) const { return (*pimpl_)[i].row_dim(); }
template <class M, class SG>
Index<SG> const & iMPS<M,SG>::col_dim(size_t i) const { return (*pimpl_)[i].col_dim(); }

template <class M, class SG>
const typename iMPS<M,SG>::value_type& iMPS<M,SG>::operator[](size_t i) const { return (*pimpl_)[i]; }
template <class M, class SG>
typename iMPS<M,SG>::value_type& iMPS<M,SG>::operator[](size_t i) { return (*pimpl_)[i]; }

template <class M, class SG>
const typename iMPS<M,SG>::lambda_t& iMPS<M,SG>::lambda() const { return pimpl_->lambda(); }
template <class M, class SG>
typename iMPS<M,SG>::lambda_t& iMPS<M,SG>::lambda() { return pimpl_->lambda(); }

template <class M, class SG>
const typename iMPS<M,SG>::lambda_t & iMPS<M,SG>::lambda_old() const	{ return pimpl_->lambda_old(); }
template <class M, class SG>
typename iMPS<M,SG>::lambda_t & iMPS<M,SG>::lambda_old()	{ return pimpl_->lambda_old(); }

template <class M, class SG>
const typename iMPS<M,SG>::boundary_t& iMPS<M,SG>::right_boundary() const { return pimpl_->right_boundary(); }
template <class M, class SG>
typename iMPS<M,SG>::boundary_t& iMPS<M,SG>::right_boundary() { return pimpl_->right_boundary(); }

template <class M, class SG>
const typename iMPS<M,SG>::boundary_t& iMPS<M,SG>::left_boundary() const { return pimpl_->left_boundary(); }
template <class M, class SG>
typename iMPS<M,SG>::boundary_t& iMPS<M,SG>::left_boundary() { return pimpl_->left_boundary(); }

template <class M, class SG>
const typename iMPS<M,SG>::size_t& iMPS<M,SG>::center() const { return pimpl_->center(); }
template <class M, class SG>
typename iMPS<M,SG>::size_t& iMPS<M,SG>::center() { return pimpl_->center(); }

template <class M, class SG>
const typename iMPS<M,SG>::size_t& iMPS<M,SG>::shift_site() const { return pimpl_->shift_site(); }
template <class M, class SG>
typename iMPS<M,SG>::size_t& iMPS<M,SG>::shift_site() { return pimpl_->shift_site(); }

template <class M, class SG>
const typename iMPS<M,SG>::charge_t& iMPS<M,SG>::shift_charge() const { return pimpl_->shift_charge(); }
template <class M, class SG>
typename iMPS<M,SG>::charge_t& iMPS<M,SG>::shift_charge() { return pimpl_->shift_charge(); }

template <class M, class SG>
void iMPS<M,SG>::absorb_lambda_left() { pimpl_->absorb_lambda_left(); }
template <class M, class SG>
void iMPS<M,SG>::absorb_lambda_right(){ pimpl_->absorb_lambda_right(); }

template <class M, class SG>
void iMPS<M,SG>::resize(size_t L){ pimpl_->resize(L); }

template <class M, class SG>
typename iMPS<M,SG>::const_iterator iMPS<M,SG>::begin() const {return pimpl_->data().begin();}
template <class M, class SG>
typename iMPS<M,SG>::const_iterator iMPS<M,SG>::end() const {return pimpl_->data().end();}
template <class M, class SG>
typename iMPS<M,SG>::const_iterator iMPS<M,SG>::cbegin() const {return pimpl_->data().cbegin();}
template <class M, class SG>
typename iMPS<M,SG>::const_iterator iMPS<M,SG>::cend() const {return pimpl_->data().cend();}
template <class M, class SG>
typename iMPS<M,SG>::iterator iMPS<M,SG>::begin() {return pimpl_->data().begin();}
template <class M, class SG>
typename iMPS<M,SG>::iterator iMPS<M,SG>::end() {return pimpl_->data().end();}

template <class M, class SG>
typename iMPS<M,SG>::reverse_iterator iMPS<M,SG>::rbegin() {return pimpl_->data().rbegin();}
template <class M, class SG>
typename iMPS<M,SG>::reverse_iterator iMPS<M,SG>::rend() {return pimpl_->data().rend();}
template <class M, class SG>
typename iMPS<M,SG>::const_reverse_iterator iMPS<M,SG>::crbegin() const {return pimpl_->data().crbegin();}
template <class M, class SG>
typename iMPS<M,SG>::const_reverse_iterator iMPS<M,SG>::crend() const {return pimpl_->data().crend();}

template <class M, class SG>
typename iMPS<M,SG>::value_type& iMPS<M,SG>::front() { return pimpl_->data().front(); }
template <class M, class SG>
const typename iMPS<M,SG>::value_type& iMPS<M,SG>::front() const { return pimpl_->data().front(); }
template <class M, class SG>
typename iMPS<M,SG>::value_type& iMPS<M,SG>::back() { return pimpl_->data().back(); }
template <class M, class SG>
const typename iMPS<M,SG>::value_type& iMPS<M,SG>::back() const { return pimpl_->data().back(); }

template <class M, class SG>
typename iMPS<M,SG>::data_t& iMPS<M,SG>::data(){	return pimpl_->data(); }
template <class M, class SG>
const typename iMPS<M,SG>::data_t& iMPS<M,SG>::data() const { return pimpl_->data(); }

template <class M, class SG>
typename iMPS<M,SG>::size_t iMPS<M,SG>::canonization(bool b) const { return pimpl_->canonization(b); }
template <class M, class SG>
void iMPS<M,SG>::canonize(size_t center, DecompMethod method ) { pimpl_->canonize(center,method); }
template <class M, class SG>
void iMPS<M,SG>::mixed_canonize(  const size_t Mmax , const double tol , DecompMethod method )
{ pimpl_->mixed_canonize(Mmax,tol,method); }

template <class M, class SG>
typename iMPS<M,SG>::block_matrix_t iMPS<M,SG>::normalize_left() { return pimpl_->normalize_left(); }
template <class M, class SG>
typename iMPS<M,SG>::block_matrix_t iMPS<M,SG>::normalize_right() { return pimpl_->normalize_right(); }
template <class M, class SG>
void iMPS<M,SG>::normalize( const size_t Mmax , const double tol ) { pimpl_->normalize(Mmax,tol); }

template <class M, class SG>
void iMPS<M,SG>::move_normalization_l2r(size_t p1, size_t p2, DecompMethod method ) { pimpl_->move_normalization_l2r(p1,p2,method); }
template <class M, class SG>
void iMPS<M,SG>::move_normalization_r2l(size_t p1, size_t p2, DecompMethod method ) { pimpl_->move_normalization_r2l(p1,p2,method); }

template <class M, class SG>
void iMPS<M,SG>::shift_unit_cell() { pimpl_->shift_unit_cell(); }

template <class M, class SG>
bool iMPS<M,SG>::mixed_canonical() const { return pimpl_->mixed_canonical(); }
template <class M, class SG>
bool iMPS<M,SG>::measure_ready() const { return pimpl_->measure_ready(); }
template <class M, class SG>
bool iMPS<M,SG>::is_mixed_canonical() const { return pimpl_->mixed_canonical(); }
template <class M, class SG>
bool iMPS<M,SG>::is_measure_ready() const { return pimpl_->measure_ready(); }

template <class M, class SG>
void iMPS<M,SG>::start_boundaries( const typename iMPS<M,SG>::block_matrix_t& left_gauge , const typename iMPS<M,SG>::block_matrix_t& right_gauge )
{ pimpl_->start_boundaries(left_gauge,right_gauge); }

template <class M, class SG>
void iMPS<M,SG>::apply(const typename iMPS<M,SG>::block_matrix_t& op , const size_type i ) { pimpl_->apply(op,i); }
template <class M, class SG>
void iMPS<M,SG>::apply(const typename iMPS<M,SG>::block_matrix_t& fill , const typename iMPS<M,SG>::block_matrix_t& op, const size_type i){ pimpl_->apply(fill,op,i); }

template <class M, class SG>
void iMPS<M,SG>::save( const std::string& prefix ) const { pimpl_->save(prefix); }
template <class M, class SG>
void iMPS<M,SG>::load( const std::string& prefix ) { pimpl_->load(prefix); }

template <class M, class SG>
std::string iMPS<M,SG>::description() const { return pimpl_->description(); }
