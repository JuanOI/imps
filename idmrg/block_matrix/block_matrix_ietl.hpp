#ifndef BLOCK_MATRIX_IETL_GUARD_
#define BLOCK_MATRIX_IETL_GUARD_

#include "dmrg/block_matrix/block_matrix.h"

template<class Matrix, class SymmGroup>
block_matrix<Matrix, SymmGroup> operator/(block_matrix<Matrix, SymmGroup> m,
                                       const typename block_matrix<Matrix, SymmGroup>::scalar_type& t)
{
    m /= t;
    return m;
}

template<class Matrix, class SymmGroup>
block_matrix<Matrix, SymmGroup> operator-(block_matrix<Matrix, SymmGroup> m)
{
    m *= typename block_matrix<Matrix, SymmGroup>::scalar_type(-1.0);
    return m;
}

template<class Matrix, class SymmGroup>
typename Matrix::value_type
dot( block_matrix<Matrix, SymmGroup> const & x, block_matrix<Matrix, SymmGroup> const & y)
{
	typedef typename Matrix::value_type value_type;

	block_matrix<Matrix, SymmGroup> temp;
	gemm( transpose( conjugate( x ) ) , y , temp );

	return temp.trace();

	/*
	semi_parallel_for ( , std::size_t b = 0; b < i1.size(); ++b) {
	    typename SymmGroup::charge c = i1[b].first;
	    assert( x.has_block(c,c) && y.has_block(c,c) );
		
		Matrix temp;
		
		gemm( transpose( conjugate( x(c,c) ) ) , y(c,c) , temp );
		vt.push_back(trace(temp));
		
	    //vt.push_back(overlap(x(c,c), y(c,c)));
	}
	*/
	//return maquis::accumulate(vt.begin(), vt.end(), value_type(0.));
}


#endif // BLOCK_MATRIX_IETL_GUARD_