#pragma once

/** @file
    @brief Header file implementing the MultiBlockMatrixInterface struct template
*/

#include <boost/random.hpp>
#include <vector>

#include <alps/numeric/matrix.hpp>
#include "dmrg/block_matrix/detail/alps.hpp"

#include "dmrg/block_matrix/block_matrix.h"
#include "idmrg/block_matrix/block_matrix_ietl.hpp"

/** @struct MultiBlockMatrixInterface
    @brief struct providing an interface for the MultiBlockMatrix object required for usage with the Trilinos solvers.
*/
template<class Matrix , class SymmGroup>
struct MultiBlockMatrixInterface
{
	typedef block_matrix<Matrix,SymmGroup> vector_t;
	typedef std::vector<vector_t&> ref_container_t;
	typedef std::vector<vector_t> container_t;
	typedef typename Matrix::value_type value_type;
	typedef boost::variate_generator<boost::lagged_fibonacci607&, boost::uniform_real<> > real_engine_t;

	virtual std::size_t getNumVectors() const = 0;
	virtual std::size_t getVecSpaceSize() const = 0;

	virtual vector_t& operator[]( const int i ) = 0;
	virtual const vector_t& operator[]( const int i ) const = 0;

	virtual MultiBlockMatrixInterface& operator=( MultiBlockMatrixInterface ) = 0;
	virtual MultiBlockMatrixInterface& operator*=( const value_type alpha ) = 0;
	virtual MultiBlockMatrixInterface& operator+=( const value_type alpha ) = 0;

	virtual void generate() = 0;
};

/// Interface free functions
template<class Matrix , class SymmGroup>
MultiBlockMatrixInterface<Matrix,SymmGroup> operator+(	MultiBlockMatrixInterface<Matrix,SymmGroup> m ,
														const typename MultiBlockMatrixInterface<Matrix,SymmGroup>::scalar_type alpha )
{	return m += alpha;	}

template<class Matrix , class SymmGroup>
MultiBlockMatrixInterface<Matrix,SymmGroup> operator+(	const typename MultiBlockMatrixInterface<Matrix,SymmGroup>::scalar_type alpha ,
														MultiBlockMatrixInterface<Matrix,SymmGroup> m )
{	return m += alpha;	}

template<class Matrix , class SymmGroup>
MultiBlockMatrixInterface<Matrix,SymmGroup> operator*(	MultiBlockMatrixInterface<Matrix,SymmGroup> m ,
														const typename MultiBlockMatrixInterface<Matrix,SymmGroup>::scalar_type alpha )
{	return m *= alpha;	}

template<class Matrix , class SymmGroup>
MultiBlockMatrixInterface<Matrix,SymmGroup> operator*(	const typename MultiBlockMatrixInterface<Matrix,SymmGroup>::scalar_type alpha ,
														MultiBlockMatrixInterface<Matrix,SymmGroup> m )
{	return m *= alpha;	}

// Implementation of the children classes

template<class Matrix , class SymmGroup>
class MultiBlockMatrixView;

template<class Matrix , class SymmGroup>
class MultiBlockMatrix : public MultiBlockMatrixInterface<Matrix,SymmGroup>
{
	typedef typename MultiBlockMatrixInterface<Matrix,SymmGroup>::vector_t vector_t;
	typedef typename MultiBlockMatrixInterface<Matrix,SymmGroup>::container_t container_t;
	typedef typename MultiBlockMatrixInterface<Matrix,SymmGroup>::real_engine_t real_engine_t;

	container_t vecs_;

    std::size_t N_;

	static real_engine_t& eng_;

public:

    MultiBlockMatrix( vector_t const & m )
    : vecs_(1,m)
    {
        for (std::size_t k = 0; k < m.n_blocks(); ++k)
            N_ += num_rows(m[k]) * num_cols(m[k]);
    }

    MultiBlockMatrix( MultiBlockMatrix const & m )
    : vecs_(m.vecs_),N_(m.N_)
	{ }

    MultiBlockMatrix( MultiBlockMatrix const & m , const int numvecs )
    : vecs_( numvecs , m[0] ),N_(m.N_)
	{ }

	static void set_real_engine( const real_engine_t& eng )
	{	eng_ = eng;		}

	int getNumVectors() const
	{	return vecs_.size();	}

	int getVecSpaceSize() const
	{	return N_;	}

	vector_t& operator[]( const int i )
	{	return vecs_[i];	}

	const vector_t& operator[]( const int i ) const
	{	return vecs_[i];	}

	void swap( MultiBlockMatrix& m )
	{
		std::swap( vecs_ , m.vecs_ );
		std::swap( N_ , m.N_ );
	}

	void swap( MultiBlockMatrixView& m )
	{
		std::swap( vecs_ , m.vecs_ );
		std::swap( N_ , m.N_ );
	}

	MultiBlockMatrix& operator=( MultiBlockMatrix m )
	{
		this->swap( m );
		return *this;
	}

	MultiBlockMatrix& operator=( MultiBlockMatrixView m )
	{
		this->swap( m );
		return *this;
	}

	MultiBlockMatrix& operator*=( const value_type alpha )
	{
		for( int i = 0 ; i < getNumVectors() ; ++i )
			(*this)[i] *= alpha;

		return *this;
	}

	MultiBlockMatrix& operator+=( const value_type alpha )
	{
		for( int i = 0 ; i < getNumVectors() ; ++i )
			for( int j = 0 ; j < (*this)[i].n_blocks() ; ++j )
				(*this)[i][j] += alpha;

		return *this;
	}

	void generate()
	{
		for( int i = 0 ; i < getNumVectors() ; ++i )
			(*this)[i].generate( eng_ );
	}
};

template<class Matrix , class SymmGroup>
class MultiBlockMatrixView : public MultiBlockMatrixInterface<Matrix,SymmGroup>
{
	typedef typename MultiBlockMatrixInterface<Matrix,SymmGroup>::vector_t vector_t;
	typedef typename MultiBlockMatrixInterface<Matrix,SymmGroup>::ref_container_t ref_container_t;
	typedef typename MultiBlockMatrixInterface<Matrix,SymmGroup>::real_engine_t real_engine_t;

	ref_container_t vecs_;

	static real_engine_t& eng_;

    std::size_t& N_;

public:

    MultiBlockMatrixView( MultiBlockMatrix const & m , const std::vector<int>& index )
    : vecs_(index.size()),N_(m.N_)
	{
		for( int i = 0 ; i < index.size() ; ++i )
			vecs_[i] = m[ index[i] ];
	}

	static void set_real_engine( const real_engine_t& eng )
	{	eng_ = eng;		}

	int getNumVectors() const
	{	return vecs_.size();	}

	int getVecSpaceSize() const
	{	return N_;	}

	vector_t& operator[]( const int i )
	{	return vecs_[i];	}

	const vector_t& operator[]( const int i ) const
	{	return vecs_[i];	}

	void swap( MultiBlockMatrixView& m )
	{
		std::swap( vecs_ , m.vecs_ );
		std::swap( N_ , m.N_ );
	}

	void swap( MultiBlockMatrix& m )
	{
		std::swap( vecs_ , m.vecs_ );
		std::swap( N_ , m.N_ );
	}

	MultiBlockMatrixView& operator=( MultiBlockMatrix m )
	{
		this->swap( m );
		return *this;
	}

	MultiBlockMatrixView& operator=( MultiBlockMatrixView m )
	{
		if( &m.N_ == &N_ )
			return *this;

		this->swap( m );
		return *this;
	}

	MultiBlockMatrixView& operator*=( const value_type alpha )
	{
		for( int i = 0 ; i < getNumVectors() ; ++i )
			(*this)[i] *= alpha;

		return *this;
	}

	MultiBlockMatrixView& operator+=( const value_type alpha )
	{
		for( int i = 0 ; i < getNumVectors() ; ++i )
			for( int j = 0 ; j < (*this)[i].n_blocks() ; ++j )
				(*this)[i][j] += alpha;

		return *this;
	}

	void generate()
	{
		for( int i = 0 ; i < getNumVectors() ; ++i )
			(*this)[i].generate( eng_ );
	}
};
