#!/bin/bash

cmake \
-DCMAKE_C_COMPILER=clang \
-DCMAKE_CXX_COMPILER=clang++ \
-DCMAKE_Fortran_COMPILER=gfortran \
-DCMAKE_CXX_FLAGS:STRING="-Wno-unused" \
-DCMAKE_BUILD_TYPE:STRING="Release" \
-DTPL_ENABLE_Boost:BOOL=ON\
-DBoost_INCLUDE_DIRS:PATH=/Users/ethuser/src/boost_1_60_0 \
-DTPL_Boost_INCLUDE_DIRS:PATH=/Users/ethuser/src/boost_1_60_0 \
-DTPL_ENABLE_MPI:BOOL=OFF \
-DTrilinos_ENABLE_ALL_OPTIONAL_PACKAGES=OFF \
-DTrilinos_ENABLE_ALL_PACKAGES=OFF \
-DTrilinos_ENABLE_Teuchos=ON \
-DTrilinos_ENABLE_Anasazi=ON \
-DTrilinos_ENABLE_Epetra=ON \
-DTrilinos_ENABLE_Tpetra=ON \
-DTrilinos_ENABLE_Thyra=ON \
-DTrilinos_ENABLE_Galeri=ON \
-DTrilinos_ENABLE_TESTS:BOOL=OFF \
-DTrilinos_ENABLE_EXAMPLES:BOOL=ON \
-DTrilinos_ENABLE_TEUCHOS_TIME_MONITOR:BOOL=ON \
-DAnasazi_ENABLE_COMPLEX:BOOL=ON \
-DAnasazi_ENABLE_RBGen=OFF \
-DAnasazi_ENABLE_TSQR:BOOL=OFF \
-DCMAKE_INSTALL_PREFIX=../../trilinos_install \
.


#-DMPI_USE_COMPILER_WRAPPERS:BOOL=ON \
#-DTrilinos_ENABLE_Zoltan:BOOL=OFF \
#-DTPL_ENABLE_Netcdf=OFF \
#-DTPL_ENABLE_BoostLib=OFF \
#-DTrilinos_ENABLE_STK=OFF \

