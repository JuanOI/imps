// This example computes the eigenvalues of largest magnitude of an
// eigenvalue problem $A x = \lambda x$, using Anasazi's
// implementation of the Block Davidson method.

#include <iostream>

#include "AnasaziBlockKrylovSchurSolMgr.hpp"
#include "AnasaziBasicEigenproblem.hpp"
#include "AnasaziConfigDefs.hpp"

// Include header to provide Anasazi with Epetra adapters.  If you
// plan to use Tpetra objects instead of Epetra objects, include
// AnasaziTpetraAdapter.hpp instead; do analogously if you plan to use
// Thyra objects instead of Epetra objects

#include <boost/random.hpp>

#include <alps/numeric/matrix.hpp>
#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix< double > matrix_t;


#include "dmrg/block_matrix/symmetry.h"
typedef NU1 sym_group;

#include "dmrg/block_matrix/block_matrix.h"
#include "idmrg/block_matrix/block_matrix_ietl.hpp"
#include "dmrg/block_matrix/block_matrix_algorithms.h"

/*
#include "dmrg/models/coded/lattice.hpp"

#include "dmrg/models/model.h"
#include "dmrg/models/generate_mpo.hpp"

#include "dmrg/mp_tensors/contractions.h"
#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/mpo_ops.h"
#include "dmrg/mp_tensors/twositetensor.h"
*/

#include "../lattice/transfer_matrix.h"
#include "multi_block_matrix.hpp"
#include "anasazi_multi_block_matrix_traits.hpp"
#include "anasazi_transfer_matrix_traits.hpp"

// ****************************************************************************
// BEGIN MAIN ROUTINE
// ****************************************************************************

typedef boost::lagged_fibonacci607 rng_t;
typedef boost::uniform_real<> real_dist_t;
typedef boost::variate_generator<boost::lagged_fibonacci607&, boost::uniform_real<> > gen_t;

rng_t rng;
real_dist_t dist(-3.,3.);
gen_t gen(rng,dist);

template<>
gen_t& MultiBlockMatrix<matrix_t,sym_group>::random_engine_ = gen;

int
	main (int argc, char *argv[])
{
	using Teuchos::RCP;
	using Teuchos::rcp;
	using std::cerr;
	using std::cout;
	using std::endl;
	// Anasazi solvers have the following template parameters:
	//
	//   - Scalar: The type of dot product results.
	//   - MV: The type of (multi)vectors.
	//   - OP: The type of operators (functions from multivector to
	//     multivector).  A matrix (like Epetra_CrsMatrix) is an example
	//     of an operator; an Ifpack preconditioner is another example.
	//
	// Here, Scalar is double, MV is Epetra_MultiVector, and OP is
	typedef Teuchos::ScalarTraits<double> STS;
	//typedef Epetra_MultiVector MV;
	typedef MultiBlockMatrix<matrix_t,sym_group> MV;
	//typedef Epetra_Operator OP;
	typedef TransferMatrix<matrix_t,sym_group> OP;

	typedef Anasazi::MultiVecTraits<double, MV> MVT;

	//
	// Set up the test problem.
	//
	// We use Trilinos' Galeri package to construct a test problem.
	// Here, we use a discretization of the 2-D Laplacian operator.
	// The global mesh size is nx * nx.
	//
	// The transfer matrix
	typedef block_matrix<matrix_t, sym_group> block_matrix_t;
	block_matrix_t mat;
	{
		matrix_t filler(2,2);
		filler(0,0) = 1;
		filler(0,1) = 2;
		filler(1,0) = 3;
		filler(1,1) = 4;

		mat.insert_block( filler , sym_group::IdentityCharge , sym_group::IdentityCharge );
	}

	// Set eigensolver parameters.
	const double tol = 1.0e-8; // convergence tolerance
	const int nev = 1; // number of eigenvalues for which to solve
	const int blockSize = 1; // block size (number of eigenvectors processed at once)
	const int numBlocks = 3; // restart length
	const int maxRestarts = 100; // maximum number of restart cycles

	// Create a set of initial vectors to start the eigensolver.
	// This needs to have the same number of columns as the block size.

	//OP tm(mat);
	RCP<OP> tm( new OP(mat) );

	MultiBlockMatrix<matrix_t,sym_group> mbm(mat);
	RCP<MV> ivec = rcp (new MV (mbm, blockSize));
	ivec->generate();

	MVT::MvPrint( *ivec , cout );

	// Create the eigenproblem.  This object holds all the stuff about
	// your problem that Anasazi will see.  In this case, it knows about
	// the matrix A and the inital vectors.
	RCP<Anasazi::BasicEigenproblem<double, MV, OP> > problem =
		rcp (new Anasazi::BasicEigenproblem<double, MV, OP> (tm, ivec));


	// Tell the eigenproblem that the operator A is symmetric.
	problem->setHermitian (false);

	// Set the number of eigenvalues requested
	problem->setNEV (nev);

	// Tell the eigenproblem that you are finishing passing it information.
	const bool boolret = problem->setProblem();
	if (boolret != true) {
		cerr << "Anasazi::BasicEigenproblem::setProblem() returned an error." << endl;
		return -1;
	}


	// Create a ParameterList, to pass parameters into the Block
	// Davidson eigensolver.
	Teuchos::ParameterList anasaziPL;
	anasaziPL.set ("Which", "LM");
	anasaziPL.set ("Block Size", blockSize);
	anasaziPL.set ("Num Blocks", numBlocks);
	anasaziPL.set ("Maximum Restarts", maxRestarts);
	anasaziPL.set ("Convergence Tolerance", tol);
	anasaziPL.set ("Verbosity", Anasazi::Errors + Anasazi::Warnings +
		Anasazi::TimingDetails + Anasazi::FinalSummary);

	// Create the Block Davidson eigensolver.
	Anasazi::BlockKrylovSchurSolMgr<double, MV, OP> anasaziSolver(problem, anasaziPL);

	// Solve the eigenvalue problem.
	//
	// Note that creating the eigensolver is separate from solving it.
	// After creating the eigensolver, you may call solve() multiple
	// times with different parameters or initial vectors.  This lets
	// you reuse intermediate state, like allocated basis vectors.
	Anasazi::ReturnType returnCode = anasaziSolver.solve();
	if (returnCode != Anasazi::Converged ) {
		cout << "Anasazi eigensolver did not converge." << endl;
	}
	/*
	// Get the eigenvalues and eigenvectors from the eigenproblem.
	Anasazi::Eigensolution<double,MV> sol = problem->getSolution();
	// Anasazi returns eigenvalues as Anasazi::Value, so that if
	// Anasazi's Scalar type is real-valued (as it is in this case), but
	// some eigenvalues are complex, you can still access the
	// eigenvalues correctly.  In this case, there are no complex
	// eigenvalues, since the matrix pencil is symmetric.
	std::vector<Anasazi::Value<double> > evals = sol.Evals;
	RCP<MV> evecs = sol.Evecs;



		cout << "Solver manager returned "
			<< (returnCode == Anasazi::Converged ? "converged." : "unconverged.")
				<< endl << endl
					<< "------------------------------------------------------" << endl
						<< std::setw(16) << "Eigenvalue"
							<< std::setw(18) << "Direct Residual"
								<< endl
									<< "------------------------------------------------------" << endl;
		for (int i=0; i<sol.numVecs; ++i) {
			cout << std::setw(16) << evals[i].realpart
				<< std::setw(18) << normR[i] / evals[i].realpart
					<< endl;
		}
		cout << "------------------------------------------------------" << endl;
	*/


	return 0;
}
