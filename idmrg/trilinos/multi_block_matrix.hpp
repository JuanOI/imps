#pragma once

/** @file
    @brief Header file implementing the MultiBlockMatrix class templates
*/

#include <tuple>
#include <vector>
#include <memory>
#include <functional>
#include <random>

#include <alps/numeric/matrix.hpp>
#include "dmrg/block_matrix/detail/alps.hpp"
#include "dmrg/utils/random.hpp"

#include "utils/traits.hpp"

#include "dmrg/block_matrix/block_matrix.h"
#include "idmrg/block_matrix/block_matrix_ietl.hpp"

namespace Anasazi
{
	namespace MBM_random
	{
		std::random_device rand_dev;

		/** @struct generator
		    @brief helper struct used for generating pseudorandom numbers
		*/
		template<class T>
		struct generator
		{
			typedef T value_type;
			typedef value_type (*generator_type)();

			generator_type gen_;

			generator( generator_type gen )
			:gen_(gen)
			{}

			T operator()()
			{ return T( gen_() ) ; }
		};

		template<class T>
		struct generator< std::complex<T> >
		{
			typedef std::complex<T> value_type;
			typedef T (*generator_type)();

			generator_type gen_;

			generator( generator_type gen )
			:gen_(gen)
			{}

			value_type operator()()
			{ return value_type( gen_() , gen_() ); }
		};
	}

	/** @class MultiBlockMatrix
		@brief class implementing the notion of a multiblock matrix.
		This class provides an interface meant to represent a matrix of sparse vectors (block matrix objects) implementation to be used with the Trilinos solvers
	*/
	template<class Matrix , class SymmGroup>
	class MultiBlockMatrix
	{
	public:

		typedef typename Matrix::value_type value_type;
		typedef typename maquis::traits::scalar_type<Matrix>::type scalar_type;
		typedef block_matrix<Matrix, SymmGroup> vector_t;
		typedef std::vector< std::shared_ptr<vector_t> > container_t;

		MultiBlockMatrix( vector_t const & m )
			: vecs_( 1 , std::make_shared<vector_t>( m ) ),N_(0)
		{
			for (std::size_t k = 0; k < m.n_blocks(); ++k)
				N_ += num_rows(m[k]) * num_cols(m[k]);
		}

		MultiBlockMatrix( MultiBlockMatrix const & m )
			: vecs_( m.getNumVectors() ),N_(m.N_)
		{
			for( std::size_t i = 0 ; i < getNumVectors() ; ++i )
				vecs_[i] = std::make_shared<vector_t>( *m[i] );
		}

		MultiBlockMatrix( MultiBlockMatrix const & m , const int numvecs )
			: vecs_( numvecs , std::make_shared<vector_t>( *m[0] ) ),N_(m.N_)
		{
			for( std::size_t i = 0 ; i < numvecs ; ++i )
				vecs_[i] = std::make_shared<vector_t>( *m[0] );
		}

		MultiBlockMatrix( MultiBlockMatrix const & m , const std::vector<int>& index , const bool view = false )
			: vecs_(index.size()),N_(m.N_)
		{
			if( view )
				for( int i = 0 ; i < index.size() ; ++i )
					vecs_[i] = m[ index[i] ];
			else
				for( int i = 0 ; i < index.size() ; ++i )
					vecs_[i] = std::make_shared<vector_t>( *m[ index[i] ] );
		}

		int getNumVectors() const
			{	return vecs_.size();	}

		int getVecSpaceSize() const
			{	return N_;	}

		const std::shared_ptr<vector_t>& operator[]( const int i ) const
			{	return vecs_[i];	}

		void swap( MultiBlockMatrix& m )
		{
			std::swap( vecs_ , m.vecs_ );
			std::swap( N_ , m.N_ );
		}

		MultiBlockMatrix& operator=( MultiBlockMatrix m )
		{
			this->swap( m );
			return *this;
		}

		MultiBlockMatrix& operator*=( const value_type alpha )
		{
			for( int i = 0 ; i < getNumVectors() ; ++i )
				*(vecs_[i]) *= alpha;

			return *this;
		}

		MultiBlockMatrix& operator+=( const value_type alpha )
		{
			typedef typename Matrix::col_element_iterator col_iter_t;

			for( int i = 0 ; i < getNumVectors() ; ++i )
				for( int j = 0 ; j < (*(*this)[i]).n_blocks() ; ++j )
					for( int k = 0 ; k < num_cols( (*vecs_[i])[j] ) ; ++k )
			{
				col_iter_t curr,end;

				std::tie(curr,end) = (*vecs_[i])[j].col( k );

				while( curr != end )
				{
					*curr = *curr + alpha;
					curr++;
				}
			}

			return *this;
		}

		MultiBlockMatrix& operator+=( const MultiBlockMatrix& mbm )
		{
			for( int i = 0 ; i < getNumVectors() ; ++i )
				*vecs_[i] += *mbm[i];

			return *this;
		}

		void generate()
		{
			MBM_random::generator<scalar_type> gen( static_cast<dmrg_random::value_type(*)()>(&dmrg_random::normal) );

 			for( int i = 0 ; i < getNumVectors() ; ++i )
			{
				vecs_[i]->generate( gen );
			}
		}

	private:
		container_t vecs_;

		std::size_t N_;
	};

} // namespace trilinos

// Interface free functions
template<class Matrix , class SymmGroup>
Anasazi::MultiBlockMatrix<Matrix,SymmGroup> operator+(	Anasazi::MultiBlockMatrix<Matrix,SymmGroup> m ,
const typename Anasazi::MultiBlockMatrix<Matrix,SymmGroup>::scalar_type alpha )
	{	return m += alpha;	}

template<class Matrix , class SymmGroup>
Anasazi::MultiBlockMatrix<Matrix,SymmGroup> operator+(	const typename Anasazi::MultiBlockMatrix<Matrix,SymmGroup>::scalar_type alpha ,
Anasazi::MultiBlockMatrix<Matrix,SymmGroup> m )
	{	return m += alpha;	}

template<class Matrix , class SymmGroup>
Anasazi::MultiBlockMatrix<Matrix,SymmGroup> operator*(	Anasazi::MultiBlockMatrix<Matrix,SymmGroup> m ,
const typename Anasazi::MultiBlockMatrix<Matrix,SymmGroup>::scalar_type alpha )
	{	return m *= alpha;	}

template<class Matrix , class SymmGroup>
Anasazi::MultiBlockMatrix<Matrix,SymmGroup> operator*(	const typename Anasazi::MultiBlockMatrix<Matrix,SymmGroup>::scalar_type alpha ,
Anasazi::MultiBlockMatrix<Matrix,SymmGroup> m )
	{	return m *= alpha;	}
