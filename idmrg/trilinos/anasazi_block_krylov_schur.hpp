#pragma once

/** @file
    @brief Header file implementing a wrapper for the block Krylov-Schur solver provided by Trilinos
*/

#include <iostream>

#include "AnasaziBlockKrylovSchurSolMgr.hpp"
#include "AnasaziBasicEigenproblem.hpp"
#include "AnasaziConfigDefs.hpp"

// Include header to provide Anasazi with Epetra adapters.  If you
// plan to use Tpetra objects instead of Epetra objects, include
// AnasaziTpetraAdapter.hpp instead; do analogously if you plan to use
// Thyra objects instead of Epetra objects

#include "utils/traits.hpp"

#include "dmrg/block_matrix/block_matrix.h"
#include "idmrg/block_matrix/block_matrix_ietl.hpp"
#include "dmrg/block_matrix/block_matrix_algorithms.h"

#include "idmrg/lattice/transfer_matrix.h"
#include "idmrg/trilinos/multi_block_matrix.hpp"
#include "idmrg/trilinos/anasazi_multi_block_matrix_traits.hpp"
#include "idmrg/trilinos/anasazi_transfer_matrix_traits.hpp"

/** @fn std::pair< std::complex< typename maquis::traits::real_type<Matrix>::type > , block_matrix<Matrix,SymmGroup> >
	trilinos_block_krylov_schur( 	const Teuchos::RCP< TransferMatrix<Matrix,SymmGroup> >& transfer_matrix ,
									const Teuchos::RCP< Anasazi::MultiBlockMatrix<Matrix,SymmGroup> >& initial , const double tol = 1e-15 )
	@brief function providing a wrapper for the nonhermitian block Krylov-Schur solver
	@param[in] transfer_matrix TransferMatrix object representing the transfer matrix of a given iMPS
	@param[in] initial initial vector for the solver
	@param[in] tol tolerance for the solver
*/
template<class Matrix , class SymmGroup>
std::pair< std::complex< typename maquis::traits::real_type<Matrix>::type > , block_matrix<Matrix,SymmGroup> >
	trilinos_block_krylov_schur( 	const Teuchos::RCP< TransferMatrix<Matrix,SymmGroup> >& transfer_matrix ,
									const Teuchos::RCP< Anasazi::MultiBlockMatrix<Matrix,SymmGroup> >& initial ,
									const double tol = 1e-15 )
{
	using Teuchos::RCP;
	using Teuchos::rcp;
	using std::cerr;
	using std::cout;
	using std::endl;
	// Anasazi solvers have the following template parameters:
	//
	//   - Scalar: The type of dot product results.
	//   - MV: The type of (multi)vectors.
	//   - OP: The type of operators (functions from multivector to
	//     multivector).
	//
	typedef typename Matrix::value_type value_type;
	typedef typename maquis::traits::real_type<Matrix>::type real_type;

	typedef Anasazi::MultiBlockMatrix<Matrix,SymmGroup> MV;
	typedef TransferMatrix<Matrix,SymmGroup> OP;

	// Set eigensolver parameters.
	constexpr int nev = 1; // number of eigenvalues for which to solve
	const int blockSize = initial->getNumVectors(); // block size (number of eigenvectors processed at once)
	const int vecSpaceSize = initial->getVecSpaceSize();

	int numBlocks;
	if( vecSpaceSize < 10 )
		numBlocks = 3;
	else if( vecSpaceSize < 1<<4 )
		numBlocks = 50;
	else
		numBlocks = 150;

    const int maxRestarts = 250; // maximum number of restart cycles

	std::cout << "Vec space size: " << initial->getVecSpaceSize() << '\n';
	std::cout << "blocksize * numBlocks: " << blockSize * numBlocks << '\n';
	// Create the eigenproblem.  This object holds all the stuff about
	// your problem that Anasazi will see.  In this case, it knows about
	// the matrix A and the inital vectors.


	//const OP* tm_p = &transfer_matrix;
	//MV* in_p = &initial;

	//RCP<const OP> tm = rcp( const_cast<const OP*>( &transfer_matrix ) , false );
	//RCP<MV> start = rcp ( &initial , false );

	RCP<Anasazi::BasicEigenproblem<value_type, MV, OP> > problem =
		rcp (new Anasazi::BasicEigenproblem<value_type, MV, OP> (transfer_matrix, initial));


	// Tell the eigenproblem that the operator A is symmetric.
	problem->setHermitian (false);

	// Set the number of eigenvalues requested
	problem->setNEV (nev);

	// Tell the eigenproblem that you are finishing passing it information.
	const bool boolret = problem->setProblem();
	if (boolret != true) {
		cerr << "Anasazi::BasicEigenproblem::setProblem() returned an error." << endl;
		throw std::runtime_error("Setting the anasazi eigenproblem failed.");
	}

	// Create a ParameterList, to pass parameters into the Block
	// Krylov-Schur eigensolver.
	Teuchos::ParameterList anasaziPL;
	anasaziPL.set ("Which", "LM");
	anasaziPL.set ("Block Size", blockSize);
	anasaziPL.set ("Num Blocks", numBlocks);
	anasaziPL.set ("Maximum Restarts", maxRestarts);
	anasaziPL.set ("Convergence Tolerance", tol);
	anasaziPL.set ("Verbosity", Anasazi::Errors + Anasazi::Warnings +
		Anasazi::TimingDetails + Anasazi::FinalSummary);

	// Create the Block Krylov-Schur eigensolver.
	Anasazi::BlockKrylovSchurSolMgr<value_type, MV, OP> anasaziSolver(problem, anasaziPL);
	std::cout << "Running solver...\n";
	// Solve the eigenvalue problem.
	Anasazi::ReturnType returnCode = anasaziSolver.solve();
	if (returnCode != Anasazi::Converged ) {
		cerr << "Anasazi eigensolver did not converge." << endl;
        throw std::runtime_error("Anasazi eigensolver did not converge.");
	}

	// Get the eigenvalues and eigenvectors from the eigenproblem.
	Anasazi::Eigensolution<value_type,MV> sol = problem->getSolution();
	// Anasazi returns eigenvalues as Anasazi::Value, so that if
	// Anasazi's Scalar type is real-valued (as it is in this case), but
	// some eigenvalues are complex, you can still access the
	// eigenvalues correctly.
	std::vector<Anasazi::Value<value_type> > evals = sol.Evals;
	RCP<MV> evecs = sol.Evecs;

	return std::make_pair( std::complex<real_type>( evals[0].realpart , evals[0].imagpart ) , *(*evecs)[0] );
}
