// This example computes the eigenvalues of largest magnitude of an
// eigenvalue problem $A x = \lambda x$, using Anasazi's
// implementation of the Block Davidson method.

#include <iostream>
#include <iomanip>

#include "AnasaziBlockKrylovSchurSolMgr.hpp"
#include "AnasaziBasicEigenproblem.hpp"
#include "AnasaziConfigDefs.hpp"

// Include header to provide Anasazi with Epetra adapters.  If you
// plan to use Tpetra objects instead of Epetra objects, include
// AnasaziTpetraAdapter.hpp instead; do analogously if you plan to use
// Thyra objects instead of Epetra objects

#include <boost/random.hpp>

#include <alps/numeric/matrix.hpp>
#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix< std::complex<double> > matrix_t;
typedef std::complex<double> scalar_type;

#include "dmrg/block_matrix/symmetry.h"
typedef NU1 sym_group;

#include "dmrg/block_matrix/block_matrix.h"
#include "idmrg/block_matrix/block_matrix_ietl.hpp"
#include "dmrg/block_matrix/block_matrix_algorithms.h"

#include "../lattice/transfer_matrix.h"
#include "multi_block_matrix.hpp"
#include "anasazi_multi_block_matrix_traits.hpp"
#include "anasazi_transfer_matrix_traits.hpp"

#include "anasazi_block_krylov_schur.hpp"

int	main (int argc, char *argv[])
{
	using Teuchos::RCP;
	using Teuchos::rcp;
	using std::cerr;
	using std::cout;
	using std::endl;

	typedef Anasazi::MultiBlockMatrix<matrix_t,sym_group> MV;
	typedef TransferMatrix<matrix_t,sym_group> OP;
	typedef typename matrix_t::value_type scalar_type;

	//
	// Set up the test problem.
	// The transfer matrix
	typedef block_matrix<matrix_t, sym_group> block_matrix_t;
	block_matrix_t mat;
	{
		matrix_t filler(4,4);
		filler(0,0) = scalar_type(1,1);
		filler(0,1) = scalar_type(1,1)*2.;
		filler(0,2) = scalar_type(1,1)*3.;
		filler(0,3) = scalar_type(1,1)*4.;
		filler(1,0) = scalar_type(1,1)*5.;
		filler(1,1) = scalar_type(1,1)*6.;
		filler(1,2) = scalar_type(1,1)*7.;
		filler(1,3) = scalar_type(1,1)*8.;
		filler(2,0) = scalar_type(1,1)*9.;
		filler(2,1) = scalar_type(1,1)*10.;
		filler(2,2) = scalar_type(1,1)*11.;
		filler(2,3) = scalar_type(1,1)*12.;
		filler(3,0) = scalar_type(1,1)*13.;
		filler(3,1) = scalar_type(1,1)*14.;
		filler(3,2) = scalar_type(1,1)*15.;
		filler(3,3) = scalar_type(1,1)*16.;

		mat.insert_block( filler , sym_group::IdentityCharge , sym_group::IdentityCharge );
	}

	//OP tm(mat);
	//OP tm( mat );
	MV mbm(mat);

	Teuchos::RCP<MV> start = Teuchos::rcp ( new MV( mat ) );
	Teuchos::RCP<OP> transfer_matrix = Teuchos::rcp( new OP( mat ) );

	int blocksize = 1;

	//MV start(mbm, blocksize);
	//start.generate();

	std::complex<double> eval;
	block_matrix_t evec;

	std::tie(eval,evec) = trilinos_block_krylov_schur( transfer_matrix , start );

		cout 	<< endl << endl
					<< "------------------------------------------------------" << endl
						<< std::setw(16) << "Eigenvalue"
							<< std::setw(18) << "Direct Residual"
								<< endl
									<< "------------------------------------------------------" << endl;
			cout << std::setw(16) << eval << endl;
			cout << "------------------------------------------------------" << endl;

	cout << std::setprecision(15) << evec;

	return 0;
}
