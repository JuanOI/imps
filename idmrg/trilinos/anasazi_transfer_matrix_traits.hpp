#pragma once

/** @file
    @brief Header file implementing the OperatorTraits class template
*/

#include "AnasaziOperatorTraits.hpp"

#include "idmrg/trilinos/multi_block_matrix.hpp"
#include "idmrg/lattice/transfer_matrix.h"

namespace Anasazi {

    /** @class OperatorTraits
        @brief traits class implementing the matrix-vector multiplication used in the Trilinos solvers.
    */
    template<class ScalarType, class Matrix, class SymmGroup>
    class OperatorTraits< ScalarType , MultiBlockMatrix<Matrix,SymmGroup> , TransferMatrix<Matrix,SymmGroup> >
    {
    public:

    typedef MultiBlockMatrix<Matrix,SymmGroup> MV;
    typedef TransferMatrix<Matrix,SymmGroup> OP;

    static void Apply ( const OP& Op,
                        const MV& x,
                        MV& y )
    {
    	for( int i = 0 ; i < x.getNumVectors() ; ++i )
    		*y[i] = Op.multiply( *x[i] );
    };

    };

} // end Anasazi namespace
