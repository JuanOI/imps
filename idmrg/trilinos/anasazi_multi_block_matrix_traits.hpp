#pragma once

/** @file
    @brief Header file defining the MultiVecTraits class template
*/

#include <boost/random.hpp>

#include "AnasaziMultiVecTraits.hpp"
#include "idmrg/trilinos/multi_block_matrix.hpp"
#include "idmrg/block_matrix/block_matrix_ietl.hpp"

namespace Anasazi {

    /** @class MultiVecTraits
        @brief class implementing the operations required by the Trilinos framework
    */
    template<class ScalarType, class Matrix, class SymmGroup>
    class MultiVecTraits< ScalarType , MultiBlockMatrix<Matrix,SymmGroup> >
    {
      typedef MultiBlockMatrix<Matrix,SymmGroup> MV;
    public:
    //! @name Creation methods
    //@{

    /*! \brief Creates a new empty \c MV containing \c numvecs columns.

    \return Reference-counted pointer to the new multivector of type \c MV.
    */
    static Teuchos::RCP<MV> Clone( const MV& mv, const int numvecs )
    {
		Teuchos::RCP<MV> Y (new MV( mv , numvecs ) );
		return Y;
	}

    /*! \brief Creates a new \c MV and copies contents of \c mv into the new vector (deep copy).

      \return Reference-counted pointer to the new multivector of type \c MV.
    */
    static Teuchos::RCP<MV> CloneCopy( const MV& mv )
    {
		Teuchos::RCP<MV> Y (new MV( mv ) );
		return Y;
	}

    /*! \brief Creates a new \c MV and copies the selected contents of \c mv into the new vector (deep copy).

      The copied vectors from \c mv are indicated by the \c index.size() indices in \c index.
      \return Reference-counted pointer to the new multivector of type \c MV.
    */
    static Teuchos::RCP<MV> CloneCopy( const MV& mv, const std::vector<int>& index )
    {
		Teuchos::RCP<MV> Y (new MV( mv , index ) );
		return Y;
	}

    /// \brief Deep copy of specified columns of mv
    ///
    /// Create a new MV, and copy (deep copy) the columns of mv
    /// specified by the given inclusive index range into the new
    /// multivector.
    ///
    /// \param mv [in] Multivector to copy
    /// \param index [in] Inclusive index range of columns of mv
    /// \return Reference-counted pointer to the new multivector of type \c MV.
    static Teuchos::RCP<MV> CloneCopy( const MV& mv, const Teuchos::Range1D& index )
    { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return Teuchos::null; }

    /*! \brief Creates a new \c MV that shares the selected contents of \c mv (shallow copy).

    The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
    \return Reference-counted pointer to the new multivector of type \c MV.
    */
    static Teuchos::RCP<MV> CloneViewNonConst( MV& mv, const std::vector<int>& index )
    {
		Teuchos::RCP<MV> Y (new MV( mv , index , true ) );
		return Y;
    }

    /// \brief Non-const view of specified columns of mv
    ///
    /// Return a non-const view of the columns of mv specified by the
    /// given inclusive index range.
    ///
    /// \param mv [in] Multivector to view (shallow non-const copy)
    /// \param index [in] Inclusive index range of columns of mv
    /// \return Reference-counted pointer to the non-const view of specified columns of mv
    static Teuchos::RCP<MV> CloneViewNonConst( MV& mv, const Teuchos::Range1D& index )
    { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return Teuchos::null; }

    /*! \brief Creates a new const \c MV that shares the selected contents of \c mv (shallow copy).

    The index of the \c numvecs vectors shallow copied from \c mv are indicated by the indices given in \c index.
    \return Reference-counted pointer to the new const multivector of type \c MV.
    */
    static Teuchos::RCP<const MV> CloneView( const MV& mv, const std::vector<int>& index )
    {
		Teuchos::RCP<const MV> Y (new MV( mv , index , true ) );
		return Y;
    }

    /// \brief Const view of specified columns of mv
    ///
    /// Return a const view of the columns of mv specified by the
    /// given inclusive index range.
    ///
    /// \param mv [in] Multivector to view (shallow const copy)
    /// \param index [in] Inclusive index range of columns of mv
    /// \return Reference-counted pointer to the const view of specified columns of mv
    static Teuchos::RCP<MV> CloneView( MV& mv, const Teuchos::Range1D& index )
    { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); return Teuchos::null; }

    //@}

    //! @name Attribute methods
    //@{

    /// Return the number of rows in the given multivector \c mv.
    static ptrdiff_t GetGlobalLength( const MV& mv )
    {
    	return static_cast<ptrdiff_t>( mv.getVecSpaceSize() );
    }

    //! Obtain the number of vectors in \c mv
    static int GetNumberVecs( const MV& mv )
    {
    	return mv.getNumVectors();
    }

    //@}

    //! @name Update methods
    //@{

    /*! \brief Update \c mv with \f$ \alpha AB + \beta mv \f$.
     */
    static void MvTimesMatAddMv( const ScalarType alpha, const MV& A,
                                 const Teuchos::SerialDenseMatrix<int,ScalarType>& B,
                                 const ScalarType beta, MV& mv )
    {
    	int rows = B.numRows();
		int cols = B.numCols();

    	for( int i = 0 ; i < B.numCols() ; ++i )
		{
			*mv[i] *= beta;
			for( int j = 0 ; j < B.numRows() ; ++j )
				*mv[i] += *A[j] * B(j,i) * alpha;
		}
    }

    /*! \brief Replace \c mv with \f$\alpha A + \beta B\f$.
     */
    static void MvAddMv( const ScalarType alpha, const MV& A, const ScalarType beta, const MV& B, MV& mv )
    {
    	mv = A;
		mv *= alpha;
		mv += beta * B;
    }

    /*! \brief Scale each element of the vectors in \c mv with \c alpha.
     */
    static void MvScale ( MV& mv, const ScalarType alpha )
    {
    	mv *= alpha;
    }

    /*! \brief Scale each element of the \c i-th vector in \c mv with \c alpha[i].
     */
    static void MvScale ( MV& mv, const std::vector<ScalarType>& alpha )
    {
    	for( int i = 0 ; i < mv.getNumVectors() ; ++i )
			*mv[i] *= alpha[i];
    }

    /// \brief Compute <tt>C := alpha * A^H B</tt>.
    ///
    /// The result C is a dense, globally replicated matrix.
    static void
    MvTransMv (const ScalarType alpha, const MV& A, const MV& B,
               Teuchos::SerialDenseMatrix<int,ScalarType>& C)
    {
    	for( int i = 0 ; i < A.getNumVectors() ; ++i )
			for( int j = 0 ; j < B.getNumVectors() ; ++j )
		{
			auto temp = *A[i];
			//temp.adjoint_inplace();

			C(i,j) = dot( temp , *B[j] );
		}


		C *= alpha;
    }

    /*! \brief Compute a vector \c b where the components are the individual dot-products of the \c i-th columns of \c A and \c mv, i.e.\f$b[i] = A[i]^Hmv[i]\f$.
     */
    static void MvDot ( const MV& mv, const MV& A, std::vector<ScalarType> &b)
    {
		for( int i = 0 ; i < mv.getNumVectors() ; ++i )
		{
			b[i] = dot( *A[i] , *mv[i] );
		}

    }

    //@}
    //! @name Norm method
    //@{

    /*! \brief Compute the 2-norm of each individual vector of \c mv.
      Upon return, \c normvec[i] holds the value of \f$||mv_i||_2\f$, the \c i-th column of \c mv.
    */
    static void MvNorm( const MV& mv, std::vector<typename Teuchos::ScalarTraits<ScalarType>::magnitudeType> &normvec )
    {
		for( int i = 0 ; i < mv.getNumVectors() ; ++i )
			normvec[i] = mv[i]->norm();
    }

    //@}

    //! @name Initialization methods
    //@{
    /*! \brief Copy the vectors in \c A to a set of vectors in \c mv indicated by the indices given in \c index.

    The \c numvecs vectors in \c A are copied to a subset of vectors in \c mv indicated by the indices given in \c index,
    i.e.<tt> mv[index[i]] = A[i]</tt>.
    */
    static void SetBlock( const MV& A, const std::vector<int>& index, MV& mv )
    {
		for( int i = 0 ; i < index.size() ; ++i )
			*mv[ index[i] ] = *A[i];
    }

    /// \brief Deep copy of A into specified columns of mv
    ///
    /// (Deeply) copy the first <tt>index.size()</tt> columns of \c A
    /// into the columns of \c mv specified by the given index range.
    ///
    /// Postcondition: <tt>mv[i] = A[i - index.lbound()]</tt>
    /// for all <tt>i</tt> in <tt>[index.lbound(), index.ubound()]</tt>
    ///
    /// \param A [in] Source multivector
    /// \param index [in] Inclusive index range of columns of mv;
    ///   index set of the target
    /// \param mv [out] Target multivector
    static void SetBlock( const MV& A, const Teuchos::Range1D& index, MV& mv )
    { UndefinedMultiVecTraits<ScalarType, MV>::notDefined(); }

    /// \brief mv := A
    ///
    /// Assign (deep copy) A into mv.
    static void Assign( const MV& A, MV& mv )
    {
    	mv = A;
    }

    /*! \brief Replace the vectors in \c mv with random vectors.
     */
    static void MvRandom( MV& mv )
    {
		mv.generate();
	}

    /*! \brief Replace each element of the vectors in \c mv with \c alpha.
     */
    static void MvInit( MV& mv, const ScalarType alpha = Teuchos::ScalarTraits<ScalarType>::zero() )
    {
    	mv *= 0.;
		mv += alpha;
    }

    //@}

    //! @name Print method
    //@{

    /*! \brief Print the \c mv multi-vector to the \c os output stream.
     */
    static void MvPrint( const MV& mv, std::ostream& os )
    {
    	os << "MultiBlockMatrix Object:\n\n";
		for( int i = 0 ; i < mv.getNumVectors() ; ++i )
		{
			os << "BlockMatrix # " << i << "\n";
			os << *mv[i] << '\n';
		}
    }

    //@}

#ifdef HAVE_ANASAZI_TSQR
    /// \typedef tsqr_adaptor_type
    /// \brief TsqrAdaptor specialization for the multivector type MV.
    ///
    /// By default, we provide a "stub" implementation.  It has the
    /// right methods and typedefs, but its constructors and methods
    /// all throw std::logic_error.  If you plan to use TSQR in
    /// Anasazi (e.g., through TsqrOrthoManager), and if your
    /// multivector type MV is neither Epetra_MultiVector nor
    /// Tpetra::MultiVector, you must implement a functional TSQR
    /// adapter.  Please refer to Epetra::TsqrAdapter (for
    /// Epetra_MultiVector) or Tpetra::TsqrAdaptor (for
    /// Tpetra::MultiVector) for examples.
    typedef Anasazi::details::StubTsqrAdapter<MV> tsqr_adaptor_type;
#endif // HAVE_ANASAZI_TSQR
  };

} // namespace Anasazi
