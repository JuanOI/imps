#include <iostream>

#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix<double> matrix;
typedef alps::numeric::matrix<std::complex<double>> cmatrix;

#include "dmrg/models/model.h"
#include "dmrg/models/lattice.h"
#include "dmrg/utils/DmrgParameters.h"

#include "idmrg/models/measurements/utils.hpp"
#include "idmrg/models/imodel_descriptor.h"
#include "idmrg/mp_tensors/impo_maker.h"

typedef NU1 grp;

DmrgParameters make_basic_params()
{
    DmrgParameters parms;
    parms.set("MODEL_LIBRARY","/Users/zkitzo14/Documents/ETHZ_files/Research/TensorNetworks/iMPS/imps/xml/models.xml");
    parms.set("LATTICE_LIBRARY","/Users/zkitzo14/Documents/ETHZ_files/Research/TensorNetworks/iMPS/imps/xml/lattices.xml");
    parms.set("verbose",true);

    return parms;
}

DmrgParameters make_spin_params(const int length, const float spin)
{
    auto parms = make_basic_params();
    parms.set("LATTICE", "open chain lattice");
    parms.set("L",       length                  );
    parms.set("MODEL",   "spin"   );
    parms.set("local_S", spin);

    return parms;
}

DmrgParameters make_hubbard_chain_params(const int length, const int N_total, const float Sz_total)
{
    auto parms = make_basic_params();
    parms.set("LATTICE", "open chain lattice");
    parms.set("L",       length                  );
    parms.set("MODEL",   "alternative fermion Hubbard"   );
    parms.set("CONSERVED_QUANTUMNUMBERS","Sz");
    parms.set("CONSERVED_QUANTUMNUMBERS","N");
    parms.set("N_total",N_total);
    parms.set("Sz_total",Sz_total);

    return parms;
}

DmrgParameters make_hubbard_cylinder_params(const int length, const int width, const int N_total, const float Sz_total)
{
    auto parms = make_basic_params();
    parms.set("LATTICE", "square cylinder");
    parms.set("L",       length           );
    parms.set("W",       width           );
    parms.set("MODEL",   "alternative fermion Hubbard"   );
    parms.set("CONSERVED_QUANTUMNUMBERS","Sz");
    parms.set("CONSERVED_QUANTUMNUMBERS","N");
    parms.set("N_total",N_total);
    parms.set("Sz_total",Sz_total);

    return parms;
}

template <class Matrix, class SymmGroup>
void create_mps( DmrgParameters&& parms )
{
    Lattice lattice(parms);

    const int length = parms["L"];
    const int center = length/2 - 1;

    Model<Matrix, SymmGroup> model(lattice, parms);
    idmrg::iModelDescriptor<Matrix,SymmGroup> model_desc( parms , lattice , center );

    using default_imps_init = default_imps_init<Matrix,SymmGroup>;
    default_imps_init imps_init(parms , model_desc);

    using iMPS = iMPS<Matrix,SymmGroup>;
    iMPS mps = iMPS( lattice.size() , center , imps_init );

    // Initialize lambdas
    {
        using charge_t = typename SymmGroup::charge;
        charge_t IdCharge;
        IdCharge = SymmGroup::IdentityCharge;

        using real_diag_matrix_t = typename iMPS::real_diag_matrix_t;
        real_diag_matrix_t mid = real_diag_matrix_t::identity_matrix(1);
        mps.lambda_old().insert_block( mid , IdCharge , IdCharge );
    }
}

int main(int argc, char ** argv)
{
    ///// ----- Real arithmetic ----- /////

    create_mps<matrix, grp>(make_hubbard_chain_params(8,8,0.0));

    create_mps<matrix, grp>(make_hubbard_chain_params(8,7,0.5));
    create_mps<matrix, grp>(make_hubbard_chain_params(9,9,0.5));
    create_mps<matrix, grp>(make_hubbard_chain_params(9,8,1.0));

    create_mps<matrix, grp>(make_hubbard_cylinder_params(4,2,8,0.0));
    create_mps<matrix, grp>(make_hubbard_cylinder_params(2,3,6,0.0));
    create_mps<matrix, grp>(make_hubbard_cylinder_params(4,2,7,1.5));
    create_mps<matrix, grp>(make_hubbard_cylinder_params(2,3,7,0.5));

    ///// ----- Complex arithmetic ----- /////

    create_mps<cmatrix, grp>(make_hubbard_chain_params(8,8,0.0));
    create_mps<cmatrix, grp>(make_hubbard_chain_params(8,7,0.5));
    create_mps<cmatrix, grp>(make_hubbard_chain_params(9,9,0.5));
    create_mps<cmatrix, grp>(make_hubbard_chain_params(9,8,1.0));

    create_mps<cmatrix, grp>(make_hubbard_cylinder_params(4,2,8,0.0));
    create_mps<cmatrix, grp>(make_hubbard_cylinder_params(2,3,6,0.0));
    create_mps<cmatrix, grp>(make_hubbard_cylinder_params(4,2,7,1.5));
    create_mps<cmatrix, grp>(make_hubbard_cylinder_params(2,3,7,0.5));

}
