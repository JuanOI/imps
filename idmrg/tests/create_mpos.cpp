#include <iostream>

#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix<double> matrix;
typedef alps::numeric::matrix<std::complex<double>> cmatrix;

#include "dmrg/models/model.h"
#include "dmrg/models/lattice.h"
#include "dmrg/utils/DmrgParameters.h"

#include "idmrg/models/measurements/utils.hpp"
#include "idmrg/models/imodel_descriptor.h"
#include "idmrg/mp_tensors/impo_maker.h"

typedef NU1 grp;

DmrgParameters make_basic_params()
{
    DmrgParameters parms;
    parms.set("MODEL_LIBRARY","/Users/zkitzo14/Documents/ETHZ_files/Research/TensorNetworks/iMPS/imps/xml/models.xml");
    parms.set("LATTICE_LIBRARY","/Users/zkitzo14/Documents/ETHZ_files/Research/TensorNetworks/iMPS/imps/xml/lattices.xml");

    return parms;
}

DmrgParameters make_hubbard_params(const int length, const int N_total, const float Sz_total)
{
    auto parms = make_basic_params();
    parms.set("LATTICE", "open chain lattice");
    parms.set("L",       length                  );
    parms.set("MODEL",   "alternative fermion Hubbard"   );
    parms.set("CONSERVED_QUANTUMNUMBERS","Sz");
    parms.set("CONSERVED_QUANTUMNUMBERS","N");
    parms.set("N_total",N_total);
    parms.set("Sz_total",Sz_total);

    return parms;
}

DmrgParameters make_spin_params(const int length, const float spin)
{
    auto parms = make_basic_params();
    parms.set("LATTICE", "open chain lattice");
    parms.set("L",       length                  );
    parms.set("MODEL",   "spin"   );
    parms.set("local_S", spin);

    return parms;
}

DmrgParameters make_sym_spin_params(const int length, const float spin, const float Sz_total)
{
    auto parms = make_basic_params();
    parms.set("LATTICE", "open chain lattice");
    parms.set("L",       length          );
    parms.set("MODEL",   "spin"   );
    parms.set("CONSERVED_QUANTUMNUMBERS","Sz");
    parms.set("Sz_total",Sz_total);
    parms.set("local_S", spin);

    return parms;
}

template <class Matrix, class SymmGroup>
void create_mpo( DmrgParameters&& parms )
{
    Lattice lattice(parms);
    Model<Matrix, SymmGroup> model(lattice, parms);

    const int length = parms["L"];
    const int center = length/2 - 1;

    idmrg::iModelDescriptor<Matrix,SymmGroup> model_desc( parms , lattice , center );

	using iMPO = iMPO<Matrix,SymmGroup>;
	using TaggedIMPOMaker = generate_mpo::TaggedIMPOMaker<Matrix, SymmGroup>;

    TaggedIMPOMaker mpom(lattice, model_desc );
    iMPO mpo = mpom.create_mpo();

    // just for debugging
    print_mpo( std::cout , mpo );
}

int main(int argc, char ** argv)
{
    ///// ----- Real arithmetic ----- /////

    /// ----- Non-symmetric simulations ----- ///
    create_mpo<matrix, grp>(make_spin_params(10,0.5));
    create_mpo<matrix, grp>(make_spin_params(10,1.0));

    /// ----- Symmetric simulations ----- ///
    create_mpo<matrix, grp>(make_hubbard_params(8,8,0.0));
    create_mpo<matrix, grp>(make_hubbard_params(8,7,0.5));
    create_mpo<matrix, grp>(make_hubbard_params(9,9,0.5));
    create_mpo<matrix, grp>(make_hubbard_params(9,8,0.0));

    create_mpo<matrix, grp>(make_sym_spin_params(8,0.5,0.0));
    create_mpo<matrix, grp>(make_sym_spin_params(8,0.5,1.0));
    create_mpo<matrix, grp>(make_sym_spin_params(9,0.5,0.5));
    create_mpo<matrix, grp>(make_sym_spin_params(9,0.5,1.5));

    ///// ----- Complex arithmetic ----- /////

    /// ----- Non-symmetric simulations ----- ///
    create_mpo<cmatrix, grp>(make_spin_params(10,0.5));
    create_mpo<cmatrix, grp>(make_spin_params(10,1.0));

    /// ----- Symmetric simulations ----- ///
    create_mpo<cmatrix, grp>(make_hubbard_params(8,8,0.0));
    create_mpo<cmatrix, grp>(make_hubbard_params(8,7,0.5));
    create_mpo<cmatrix, grp>(make_hubbard_params(9,9,0.5));
    create_mpo<cmatrix, grp>(make_hubbard_params(9,8,0.0));

    create_mpo<cmatrix, grp>(make_sym_spin_params(8,0.5,0.0));
    create_mpo<cmatrix, grp>(make_sym_spin_params(8,0.5,1.0));
    create_mpo<cmatrix, grp>(make_sym_spin_params(9,0.5,0.5));
    create_mpo<cmatrix, grp>(make_sym_spin_params(9,0.5,1.5));
}
