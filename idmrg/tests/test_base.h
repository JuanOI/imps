#pragma once

#include <string>

#include <boost/filesystem.hpp>

#include "dmrg/utils/DmrgParameters.h"

struct TestBase{

    DmrgParameters parms;

    TestBase( DmrgParameters&& p ):
    parms(p)
    {}

    virtual void check_validity(){};

    virtual ~TestBase(){
        check_validity();

        std::string resultfile = parms["resultfile"];
        boost::filesystem::remove_all(resultfile);

        std::string chkpfile = parms["chkpfile"];
        boost::filesystem::remove_all(resultfile);
    }
};
