#include <iostream>

#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix<double> matrix;

#include "dmrg/models/model.h"
#include "dmrg/models/lattice.h"
#include "dmrg/utils/DmrgParameters.h"

#include "measurements/utils.hpp"
#include "idmrg/models/imodel_descriptor.h"

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include <boost/tuple/tuple.hpp>

#include <alps/parser/xmlstream.h>

#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "dmrg/block_matrix/symmetry.h"
#include "dmrg/models/measurement.h"
#include "dmrg/models/measurement_utils.hpp"

#include "idmrg/mp_tensors/imps_mpo_ops.h"
#include "idmrg/lattice/lattice.hpp"
#include "idmrg/mp_tensors/impo.h"

int main () {
using namespace boost::numeric::ublas;
compressed_matrix< std::pair< unsigned , std::complex<double> > > m;

    std::pair<unsigned , std::complex<double> > p = m(0,0);

    std::cout << p.first << std::endl;


}
