#include <iostream>

#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix<double> matrix;
typedef alps::numeric::matrix<std::complex<double>> cmatrix;

#include "dmrg/models/model.h"
#include "dmrg/models/lattice.h"
#include "dmrg/utils/DmrgParameters.h"

#include "idmrg/models/measurements/utils.hpp"
#include "idmrg/models/imodel_descriptor.h"

typedef NU1 grp;

template <class Matrix, class SymmGroup>
void run()
{
    // Define the minimal set of parameters
    DmrgParameters parms;
    parms.set("LATTICE", "open chain lattice");
    parms.set("L",       10                  );
    parms.set("MODEL",   "alternative fermion Hubbard"   );
    parms.set("MODEL_LIBRARY","/Users/zkitzo14/Documents/ETHZ_files/Research/TensorNetworks/iMPS/imps/xml/models.xml");

    parms.set("MEASURE_LOCAL[ XHOYX]" , "Sz" );
    parms.set(" MEASURE_LOCAL[ Hubbard on-site ] " , "hubbard_onsite" );

    /// Build lattice and model
    Lattice lattice(parms);
    Model<Matrix, SymmGroup> model(lattice, parms);

    idmrg::iModelDescriptor<Matrix,SymmGroup> model_desc( parms , lattice , 4 );

    auto measurements = imeasurement::prepare_and_create_measurements( parms , model_desc );
}


int main(int argc, char ** argv)
{
    run<matrix, grp>();
    run<cmatrix, grp>();
}
