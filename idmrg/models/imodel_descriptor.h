#pragma once

/** @file
    @brief Header file defining the iModelDescriptor class.
*/

#include <unordered_map>
#include <map>
#include <string>
#include <tuple>

#include <boost/shared_ptr.hpp>
#include <boost/any.hpp>

#include "dmrg/models/lattice.h"
#include "dmrg/models/model.h"
#include "dmrg/models/op_handler.h"
#include "dmrg/models/tag_detail.h"
#include "dmrg/models/generate_mpo/utils.hpp"
#include "dmrg/models/measurement_term_desc.hpp"

#include "idmrg/mp_tensors/imps.h"
#include "idmrg/lattice/lattice.h"

namespace idmrg{

	template<class Matrix, class SymmGroup>
	struct iDMRG;

/** @struct iModelDescriptor
    @brief struct implementing an intermediate layer between the iDMRG class and the ALPS model.
	Ths is necessary because the actual properties of the system do not match those provided by the user one-to-one. The reason is that to ease the incorporation of quantum numbers we insert a physical index which is of trivial dimension but of nontrivial symmetry charge.
*/
template<class Matrix,class SymmGroup>
struct iModelDescriptor
{
	typedef typename iMPS<Matrix,SymmGroup>::scalar_type scalar_type;
    typedef typename Matrix::value_type value_type;

	typedef Lattice::pos_t pos_t;

	typedef typename SymmGroup::charge charge_t;
	typedef Index<SymmGroup> index_t;
    typedef int site_type_t;

	typedef std::vector< index_t > index_cont_t;
	typedef std::vector< site_type_t > site_type_cont_t;

	typedef typename Model<Matrix,SymmGroup>::terms_type terms_type;
	typedef typename Model<Matrix,SymmGroup>::term_descriptor term_descriptor;
	typedef typename std::vector< std::vector<pos_t> > neighbour_list_t;
	typedef MeasurementTermDescriptor<typename Matrix::value_type> measurement_term_desc_t;

	typedef typename Model<Matrix, SymmGroup>::op_t op_t;
	typedef typename Model<Matrix, SymmGroup>::table_ptr handler_ptr;
	typedef boost::shared_ptr<OPTable<Matrix, SymmGroup> > table_ptr;
	typedef typename generate_mpo::Operator_Tag_Term<Matrix, SymmGroup>::tag_type tag_type;

	friend struct idmrg::iDMRG<Matrix, SymmGroup>;

	iModelDescriptor(	DmrgParameters& parms ,
						const Lattice& lattice ,
						const std::size_t shift_site )
	: 	model_(lattice,parms),
		shift_site_(shift_site),
		shift_type_( lattice.maximum_vertex_type() + 1 ) ,
		shift_charge_( model_.total_quantum_numbers( parms ) ),
		shift_index_(),
		indices_(),
		site_type_(),
		uc_size_(lattice.size()),
		identities_(),
		fillings_(),
		hamiltonian_terms_()
	{
		// Gather identities/fillings and site types
		for( int i = 0 ; i <= lattice.maximum_vertex_type() ; ++i )
		{
			indices_.push_back( model_.phys_dim(i) );

			identities_.push_back( model_.identity_matrix_tag( i ) );
			fillings_.push_back( model_.filling_matrix_tag( i ) );
		}

		// Read out site types
		for( int i = 0 ; i < lattice.size() ; ++i )
			site_type_.push_back( lattice.get_prop<int>( "type" , i ) );

		// Register the shifted index
		shift_index_ = indices_[ site_type_[shift_site_] ];
		shift_index_.shift( -shift_charge_ );

		// We need to fake having a lattice twice as big to generate all mpo terms properly
		int L = parms["L"];
		parms.set("L" , 2*L);

		Lattice double_lattice( parms );
		Model<Matrix, SymmGroup> double_model(double_lattice , parms);

		// Reestablish original lattice dimension
		parms.set("L",L);

		// Select hamiltonian terms connected to the first unit cell
		{
			auto connected_term = [&lattice]( const term_descriptor& term_desc ) -> bool
			{
				for( int i = 0 ; i < term_desc.size() ; ++i )
					if( ( 0 <= term_desc.position( i ) ) && ( term_desc.position( i ) < lattice.size() ) )
						return true;

				return false;
			};

			auto it = double_model.hamiltonian_terms().begin();
			auto it_e = double_model.hamiltonian_terms().end();

			while( it != it_e )
			{
				if( connected_term( *it ) )
					hamiltonian_terms_.push_back( *it );

				++it;
			}
		}

		// Adjust for the shift charge by modifying the operators and the tags in the descriptors
		// At this point we can't get a cache because we don't know which operators they actually are.
		{
			auto shift_term = [this,&lattice]( term_descriptor& term_desc )
			{
				for( int i = 0 ; i < term_desc.size() ; ++i )
					if( shift_site_ == term_desc.position( i ) % lattice.size() )
					{
						tag_type old_tag = term_desc.operator_tag( i );
						auto shift_op = get_tag_handler()->get_op( old_tag );

						shift_op.shift_basis( -shift_charge_ );

						auto kind = get_tag_handler()->is_fermionic( old_tag ) ? tag_detail::fermionic : tag_detail::bosonic ;

						auto new_tag = get_tag_handler()->register_op( shift_op , kind );
						boost::get<1>(term_desc[i]) = new_tag;
					}
			};

			auto it = hamiltonian_terms_.begin();
			auto it_e = hamiltonian_terms_.end();

			std::for_each( it , it_e , shift_term );
		}

	}

	std::size_t shift_site() const
	{ return shift_site_; }

	std::size_t shift_type() const
	{ return shift_type_; }

	const charge_t& shift_charge() const
	{ return shift_charge_; }

	const index_t& shift_index() const
	{ return shift_index_; }

	const index_cont_t& indices() const
	{ return indices_; }

	const index_t& physical_index( const int i ) const
	{
		assert( i < indices_.size() );
		return indices_[i];
	}

	const site_type_cont_t& site_types() const
	{ return site_type_; }

    site_type_t site_type( const int i ) const
    { return i == shift_site_ ? shift_type_ : site_type_[i]; }

	handler_ptr get_tag_handler() const
	{ return model_.operators_table(); }

	table_ptr operator_table() const
	{ return get_tag_handler()->get_operator_table(); }

	const terms_type& hamiltonian_terms() const
	{ return hamiltonian_terms_; }

	const op_t& identity_matrix( const int site ) const;
	tag_type identity_matrix_tag( const int site ) const;

	const op_t& filling_matrix( const int site ) const;
	tag_type filling_matrix_tag( const int site ) const;

	const op_t& get_operator( const int site , const std::string& name ) const;
	tag_type get_operator_tag( const int site , const std::string& name ) const;

    const op_t& get_operator( const int site , const std::vector<std::string>& names ) const
    { return get_product(site , names); }
    tag_type get_operator_tag( const int site , const std::vector<std::string>& names ) const
    { return get_product_tag(site , names); }

    const op_t& get_product( const int site , const std::vector<std::string>& names ) const;
    auto get_product_tag( const int site , const std::vector<std::string>& names ) const -> tag_type;

    auto get_product_tag( const tag_type tag1 , const tag_type tag2 ) const -> std::pair<tag_type,value_type>
    { return get_tag_handler()->get_product_tag( tag1 , tag2 ); }

    std::vector<measurement_term_desc_t> unpack_measurement_terms(std::string const & name) const
    { return model_.unpack_measurement_terms( name ); }

	void print_operator_pool() const
	{
		for( auto t : shifted_op_tag_cache_ )
		{
			std::cout << t.first << " " << t.second << '\n';
		}
	}

protected:

	void push_into_cache( const std::string name , const tag_type tag , const scalar_type prefactor = 1. ) const
	{
		op_t op = get_tag_handler()->get_op( tag );
		auto kind = get_tag_handler()->is_fermionic( tag ) ? tag_detail::fermionic : tag_detail::bosonic;

        op.shift_basis( -shift_charge_ );

        tag_type new_tag = get_tag_handler()->register_op( op*prefactor , kind );

		shifted_op_tag_cache_[name] = new_tag;

		assert( shifted_op_tag_cache_.find(name) != shifted_op_tag_cache_.end() );
	}

	const Model<Matrix, SymmGroup> model_;

	std::size_t	shift_site_;
	const std::size_t shift_type_;
	const charge_t	shift_charge_;
	mutable index_t shift_index_;

	mutable index_cont_t indices_;
	site_type_cont_t site_type_;

	const std::size_t uc_size_;

	mutable std::unordered_map<std::string , tag_type> shifted_op_tag_cache_;
	mutable std::map<std::pair<std::string,site_type_t> , tag_type> prod_op_tag_cache_;

	//Identity and filling tags organized by site type.
	mutable std::vector<tag_type> identities_;
	mutable std::vector<tag_type> fillings_;

	mutable terms_type hamiltonian_terms_;
};

template<class Matrix,class SymmGroup>
const typename iModelDescriptor<Matrix,SymmGroup>::op_t& iModelDescriptor<Matrix,SymmGroup>::identity_matrix( const int site ) const
{
	int uc_site = site % uc_size_;

	if( uc_site == shift_site_ )
	{
		if( shifted_op_tag_cache_.find("ident") == shifted_op_tag_cache_.end() )
			push_into_cache( "ident" , model_.identity_matrix_tag( site_type_[uc_site] ) );

		return get_tag_handler()->get_op( shifted_op_tag_cache_["ident"] );
	}
	else
		return model_.identity_matrix( site_type_[uc_site] );
}

template<class Matrix,class SymmGroup>
typename iModelDescriptor<Matrix,SymmGroup>::tag_type iModelDescriptor<Matrix,SymmGroup>::identity_matrix_tag( const int site ) const
{
	int uc_site = site % uc_size_;

	if( uc_site == shift_site_ )
	{
		if( shifted_op_tag_cache_.find("ident") == shifted_op_tag_cache_.end() )
			push_into_cache( "ident" , model_.identity_matrix_tag( site_type_[uc_site] ) );

		return shifted_op_tag_cache_["ident"];
	}
	else
		return identities_[ site_type_[uc_site] ];
}

template<class Matrix,class SymmGroup>
const typename iModelDescriptor<Matrix,SymmGroup>::op_t& iModelDescriptor<Matrix,SymmGroup>::filling_matrix( const int site ) const
{
	int uc_site = site % uc_size_;

	if( uc_site == shift_site_ )
	{
		if( shifted_op_tag_cache_.find("fill") == shifted_op_tag_cache_.end() )
			push_into_cache( "fill" , model_.filling_matrix_tag( site_type_[uc_site] ) );

		return get_tag_handler()->get_op( shifted_op_tag_cache_["fill"] );
	}
	else
		return model_.filling_matrix( site_type_[uc_site] );
}

template<class Matrix,class SymmGroup>
typename iModelDescriptor<Matrix,SymmGroup>::tag_type iModelDescriptor<Matrix,SymmGroup>::filling_matrix_tag( const int site ) const
{
	int uc_site = site % uc_size_;

	if( uc_site == shift_site_ )
	{
		if( shifted_op_tag_cache_.find("fill") == shifted_op_tag_cache_.end() )
			push_into_cache( "fill" , model_.filling_matrix_tag( site_type_[uc_site] ) );

		return shifted_op_tag_cache_["fill"];
	}
	else
		return fillings_[ site_type_[uc_site] ] ;
}

template<class Matrix,class SymmGroup>
const typename iModelDescriptor<Matrix,SymmGroup>::op_t& iModelDescriptor<Matrix,SymmGroup>::get_operator( const int site , const std::string& name ) const
{
	int uc_site = site % uc_size_;

	if( uc_site == shift_site_ )
	{
		if( shifted_op_tag_cache_.find(name) == shifted_op_tag_cache_.end() )
			push_into_cache( name , model_.get_operator_tag( name , site_type_[uc_site] ) );

		return get_tag_handler()->get_op( shifted_op_tag_cache_[name] );
	}
	else
		return model_.get_operator( name , site_type_[uc_site] );
}

template<class Matrix,class SymmGroup>
typename iModelDescriptor<Matrix,SymmGroup>::tag_type iModelDescriptor<Matrix,SymmGroup>::get_operator_tag( const int site , const std::string& name ) const
{
	int uc_site = site % uc_size_;

	if( uc_site == shift_site_ )
	{
		if( shifted_op_tag_cache_.find(name) == shifted_op_tag_cache_.end() )
			push_into_cache( name , model_.get_operator_tag( name , site_type_[uc_site] ) );

		return shifted_op_tag_cache_[name];
	}
	else
		return model_.get_operator_tag( name , site_type_[uc_site] );
}

template<class Matrix,class SymmGroup>
const typename iModelDescriptor<Matrix,SymmGroup>::op_t& iModelDescriptor<Matrix,SymmGroup>::get_product( const int site , const std::vector<std::string>& names ) const
{
    auto tag = get_product_tag( site , names );

    return get_tag_handler()->get_op(tag);
}


template<class Matrix,class SymmGroup>
auto iModelDescriptor<Matrix,SymmGroup>::get_product_tag( const int site , const std::vector<std::string>& names ) const -> tag_type
{
    assert( names.size() > 0 );

    if( names.size() == 1 )
        return get_operator_tag(site , names[0]);
    else
    {
        std::pair<std::string,site_type_t> name_type_pair;

        name_type_pair.first = names[0] + "<" + std::to_string( site_type(site) ) + ">";
        name_type_pair.second = site_type_[site];

        int ops_checked = 1;
        while( ops_checked < names.size() )
        {
            auto next_name = name_type_pair.first + "*" + names[ops_checked] + "<" + std::to_string( site_type(site) ) + ">";

            if( prod_op_tag_cache_.find(make_pair(next_name , site_type_[site])) == prod_op_tag_cache_.end())
            {
                // Compute product tag/prefactor
                tag_type product_tag;
                if( ops_checked == 1 )
                    product_tag = get_operator_tag(site , names[0]);
                else
                    product_tag = prod_op_tag_cache_[name_type_pair];

                scalar_type prefactor = 1.;
                for( auto n_it = names.begin() + ops_checked ; n_it != names.end() ; n_it++ )
                {
                    scalar_type temp;
                    std::tie(product_tag , prefactor) = get_tag_handler()->get_product_tag( product_tag , get_operator_tag(site , *n_it) );

                    // Register prefactor*op
                    auto op = get_tag_handler()->get_op( product_tag );
                    op *= prefactor;

                    auto kind = get_tag_handler()->is_fermionic( product_tag ) ? tag_detail::fermionic : tag_detail::bosonic ;

                    name_type_pair.first = next_name;
                    prod_op_tag_cache_.insert(make_pair(name_type_pair , get_tag_handler()->register_op( op , kind )));
                }
            }
            else
            {
                name_type_pair.first = next_name;
                //ops_checked++;
            }

            ops_checked++;
        }

        return prod_op_tag_cache_[name_type_pair];
    }
}
}
