#pragma once

/** @file
    @brief Header file defining the iMeasurement class.
*/

#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>

#include <alps/parser/xmlstream.h>

#include "dmrg/models/measurement.h"
#include "dmrg/utils/DmrgParameters.h"

//#include "idmrg/lattice/lattice.hpp"
//#include "idmrg/models/imodel_descriptor.h"

namespace idmrg{
    template<class M, class SG>
    struct iModelDescriptor;
}

class iLattice;

/** @class iMeasurement
    @brief class implementing infinite measurements.
*/
template<class Matrix, class SymmGroup>
class iMeasurement {
public:
    typedef typename Matrix::value_type value_type;
	typedef std::vector<value_type> results_type;
	typedef std::vector<std::string> labels_type;
    typedef iMPS<Matrix,SymmGroup> iMPS_type;
	typedef typename iMPS_type::lambda_t lambda_t;
    typedef idmrg::iModelDescriptor<Matrix, SymmGroup> iModelDescriptor;

    iMeasurement(std::string const& n="") : cast_to_real(true), is_super_meas(false), name_(n), eigenstate(0) { }
    virtual ~iMeasurement() { }

    virtual void evaluate( const lambda_t& inv_lambda , iMPS_type const& , iLattice& lattice ,  const iModelDescriptor& model_desc ) = 0;
    template <class Archive>
    void save(Archive &, const std::string& prefix_path = "") const;
    void write_xml(alps::oxstream &) const;
    virtual std::ostream& print(std::ostream& os) const;

    std::string const& name() const { return name_; }
    int& eigenstate_index() { return eigenstate; }
    int eigenstate_index() const { return eigenstate; }

    void set_super_meas(Index<SymmGroup> const& phys_psi_);

    iMeasurement* clone() const { return do_clone(); }

	const results_type& get_results() const
	{ return vector_results; }

	const labels_type& get_labels() const
	{ return labels; }

protected:
    virtual iMeasurement* do_clone() const =0;

    bool cast_to_real, is_super_meas;
    labels_type labels;
    results_type vector_results;

    Index<SymmGroup> phys_psi;

private:
    std::string name_;
    int eigenstate;
};

template<class Matrix, class SymmGroup>
inline iMeasurement<Matrix, SymmGroup>* new_clone( const iMeasurement<Matrix, SymmGroup>& m )
{
    return m.clone();
}

template<class Matrix, class SymmGroup>
template <class Archive>
void iMeasurement<Matrix, SymmGroup>::save(Archive & ar, const std::string& prefix_path ) const
{
    const std::string prefixed_name = prefix_path + storage::encode(name());

    if (vector_results.size() > 0) {
        if (cast_to_real) {
            save_val_at_index(ar, prefixed_name + "/mean/value",  maquis::real(vector_results), eigenstate_index());
        } else {
            save_val_at_index(ar, prefixed_name + "/mean/value", vector_results, eigenstate_index());
        }
        if (labels.size() > 0)
            ar[prefixed_name + std::string("/labels")] << labels;
    } else {
        if (cast_to_real) {
            save_val_at_index(ar, prefixed_name + "/mean/value", maquis::real(vector_results[0]), eigenstate_index());
        } else {
            save_val_at_index(ar, prefixed_name + "/mean/value", vector_results[0], eigenstate_index());
        }
    }
}

template<class Matrix, class SymmGroup>
void iMeasurement<Matrix, SymmGroup>::write_xml(alps::oxstream & out) const
{
    if (labels.size() > 0) {
        out << alps::start_tag("VECTOR_AVERAGE") << alps::attribute("name", name());
        for (int i=0; i<labels.size(); ++i) {
            out << alps::start_tag("SCALAR_AVERAGE");
            out << alps::attribute("indexvalue",labels[i]) << alps::no_linebreak;

            if (cast_to_real) {
                out << alps::start_tag("MEAN") << alps::no_linebreak << maquis::real(vector_results[i]) << alps::end_tag("MEAN");
            } else {
                out << alps::start_tag("MEAN") << alps::no_linebreak << vector_results[i] << alps::end_tag("MEAN");
            }
            out << alps::end_tag("SCALAR_AVERAGE");

        }
        out << alps::end_tag("VECTOR_AVERAGE");
    } else {
        out << alps::start_tag("SCALAR_AVERAGE") << alps::attribute("name", name()) << alps::no_linebreak;
        out << alps::start_tag("MEAN") <<  alps::no_linebreak << ((cast_to_real) ?  maquis::real(vector_results[0]) : vector_results[0]) << alps::end_tag("MEAN");
        out << alps::end_tag("SCALAR_AVERAGE");
    }
}


template<class Matrix, class SymmGroup>
void iMeasurement<Matrix, SymmGroup>::set_super_meas(Index<SymmGroup> const& phys_psi_)
{
    phys_psi = phys_psi_;
    is_super_meas = true;
}

template<class Matrix, class SymmGroup>
std::ostream& iMeasurement<Matrix, SymmGroup>::print(std::ostream& os) const
{
    return os << "MEASURE[" << name_ << "]";
}

template<class Matrix, class SymmGroup>
std::ostream& operator<<(std::ostream& os, iMeasurement<Matrix, SymmGroup> const& m)
{
    return m.print(os);
}
