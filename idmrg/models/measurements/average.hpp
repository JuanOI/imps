#pragma once

/** @file
    @brief Header file implementing the @p average measurement class.
*/

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

class iLattice;

namespace idmrg{
	template<class Matrix, class SymmGroup>
	struct iModelDescriptor;
}

namespace imeasurement
{
	template<class Matrix, class SymmGroup>
	class local;

	/** @class local
        @brief class deriving from the class iMeasurement and implementing the notion of an infinite @p average measurement
        Average measurements in our case refer to local measurements for which only the average is delivered
    */
    template <class Matrix, class SymmGroup>
    class average : public local<Matrix, SymmGroup>
	{
        protected:
        typedef local<Matrix, SymmGroup> base_type;

        typedef typename base_type::measurement_term_desc_t measurement_term_desc_t;
        typedef typename base_type::measurement_desc_t measurement_desc_t;

		typedef typename base_type::iMPS_type iMPS_type;
		typedef idmrg::iModelDescriptor<Matrix, SymmGroup> iModelDescriptor;
		typedef typename iModelDescriptor::scalar_type scalar_type;

		typedef typename iMeasurement<Matrix,SymmGroup>::lambda_t lambda_t;

		public:

        average(std::string const& name_,
              measurement_desc_t const& desc )
        :
		base_type(name_,desc)
        {}


        void evaluate( const lambda_t& lambda_inv , iMPS_type const& mps, iLattice& lattice , const iModelDescriptor& model_desc ) override
        {
			base_type::evaluate(lambda_inv , mps , lattice , model_desc);

            evaluate( *this , lattice );
        }

        void evaluate( const base_type& measurement , iLattice& lattice )
        {
			auto res = std::accumulate(measurement.get_results().begin() , measurement.get_results().end() , scalar_type(0.));

			res /= this->vector_results.size();

			this->vector_results.resize(0);
			this->labels.resize(0);

			register_measurement( res );
        }

    protected:

        average* do_clone() const override
        {
            return new average(*this);
        }

	private:
		void register_measurement( const scalar_type result )
		{
			this->vector_results.push_back( result );
		}

    }; // Class average
} // namespace imeasurement
