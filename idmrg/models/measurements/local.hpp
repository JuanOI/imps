#pragma once

/** @file
    @brief Header file implementing the @p local measurement class.
*/

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <unordered_map>

#include <boost/tuple/tuple.hpp>

#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "utils/traits.hpp"

#include "idmrg/mp_tensors/imps_mpo_ops.h"
#include "idmrg/lattice/lattice.h"
#include "idmrg/mp_tensors/impo.h"

namespace idmrg{
    template<class Matrix, class SymmGroup>
    struct iModelDescriptor;
}

template<class Matrix, class SymmGroup>
class iMeasurement;

namespace imeasurement
{
    /** @class local
        @brief class deriving from the class iMeasurement and implementing the notion of an infinite @p local measurement
        Local measurements in our case refer to single-site or bond measurements
    */
    template <class Matrix, class SymmGroup>
    class local : public iMeasurement<Matrix, SymmGroup>
	{
        protected:

        typedef iMeasurement<Matrix, SymmGroup> base_type;

        typedef idmrg::iModelDescriptor<Matrix, SymmGroup> iModelDescriptor;
        typedef typename iModelDescriptor::scalar_type scalar_type;
        typedef typename iModelDescriptor::tag_type tag_type;

        typedef typename base_type::iMPS_type iMPS_type;
        typedef iMPO<Matrix, SymmGroup> iMPO_type;
        typedef std::unordered_map<std::string , typename local<Matrix, SymmGroup>::iMPO_type> labeled_mpo_cont_t;

		typedef MeasurementTermDescriptor<typename Matrix::value_type> measurement_term_desc_t;
        typedef std::vector<measurement_term_desc_t> measurement_desc_t;
        typedef std::unordered_map<std::string , typename maquis::traits::real_type<scalar_type>::type> labeled_results_cont_t;

		typedef std::vector<boost::tuple<std::size_t, std::size_t, tag_type, scalar_type> > prempo_t;
		typedef typename MPO<Matrix, SymmGroup>::elem_type mpo_tensor;

		typedef typename iMeasurement<Matrix,SymmGroup>::lambda_t lambda_t;

		public:

        local(std::string const& name_,
              measurement_desc_t const& desc )
        :
		base_type(name_) ,
		meas_desc_(desc)
        {

        }

        void evaluate( const lambda_t& lambda_inv , iMPS_type const& mps, iLattice& lattice , const iModelDescriptor& model_desc ) override
        {
            assert(mps.is_measure_ready());

            std::cout << "Measuring: " + this->name() + "...";

            this->vector_results.clear();
            this->labels.clear();

            // This is needed because bond measurements will necessarily span beyond a single unit cell. Since all local measurements will never touch beyond NN unit cells, we simply double the unit cell for measuring.
			auto num_graphs = lattice.get_num_graphs();
			if( meas_desc_[0].op_names.size() == 2 && num_graphs != 2 )
				lattice.set_num_graphs( 2 );

			const int measurement_L = lattice.size();
            lattice.set_num_graphs( 1 );  // Local measurements only require one graph (plus boundary terms generated internally)

			double measure_norm = norm( measurement_L , lambda_inv , mps );

            labeled_results_cont_t results;
            for( const auto& measurement_term : meas_desc_ )
            {
                auto labeled_mpos = prepare_mpos( measurement_L , measurement_term , lattice , model_desc ); // unordered_map

                for( const auto& lmpo : labeled_mpos )
                {
                    const auto& label = lmpo.first;
                    const auto& mpo = lmpo.second;

                    //std::cout << label << std::endl;

                    if( results.find( label ) == results.end() )
                        results[label] = 0.;

                    //std::cout << "Label is " << label << std::endl;
                    auto res = maquis::real( expval(lambda_inv , mps , mpo) )/measure_norm;
                    //std::cout << "Res is " << res << std::endl;

                    results[label] += res;
                }
            }

            register_measurement( results );

			lattice.set_num_graphs( num_graphs );

            std::cout << "Done." << std::endl;
        }

    protected:

        auto prepare_mpos( const int measurement_L , const measurement_term_desc_t& measurement_term , const iLattice& lattice , const iModelDescriptor& model_desc ) const -> labeled_mpo_cont_t;

        local* do_clone() const override
        {
            return new local(*this);
        }

		const measurement_desc_t meas_desc_;

    private:
        void register_measurement( const labeled_results_cont_t& results )
        {
            for( const auto& val : results )
            {
                this->labels.push_back( val.first );
                this->vector_results.push_back( val.second );
            }
        }
    };


    template<class Matrix, class SymmGroup>
    auto local<Matrix, SymmGroup>::prepare_mpos( const int measurement_L , const measurement_term_desc_t& measurement_term , const iLattice& lattice , const iModelDescriptor& model_desc ) const -> labeled_mpo_cont_t
    {
        labeled_mpo_cont_t labeled_mpos;

        switch( measurement_term.op_names.size() )
        {
            case 2: // Bond measurements
            {
                assert( measurement_term.op_names[0].size() == 1 && measurement_term.op_names[1].size() == 1 );

                auto bonds = lattice.bonds();

                for( const auto& b : bonds)
                {
                    int source = b[0];
                    int target = b[1];

                //for( int source = 0 ; source < lattice.size() ; ++source )
                //{
                    //for( int t_i = 0 ; t_i < forward[source].size() ; ++t_i )
                    //{
                        //int target = forward[source][t_i];
                        //std::cout << "Current source is " << source << std::endl;
                        //std::cout << "Current target is " << target << std::endl;

                        tag_type source_tag = model_desc.get_operator_tag( source , measurement_term.op_names[0][0] );
                        tag_type target_tag = model_desc.get_operator_tag( target , measurement_term.op_names[1][0] );

                        //std::cout << "Done getting tags." << std::endl;

                        // Get minus signs properly in case of fermions
                        // Make sure both operators are of the same type (we assume we are always dealing with parity preserving operators).
                        assert( !(model_desc.get_tag_handler()->is_fermionic( source_tag ) ^ model_desc.get_tag_handler()->is_fermionic( target_tag )) );

                        bool fermionic = model_desc.get_tag_handler()->is_fermionic( source_tag );

                        scalar_type pre_factor = 1.;

                        if( source > target )
                        {
                            std::swap(source,target);
                            std::swap(source_tag,target_tag);
                            if(fermionic) pre_factor *= -1;
                        }

                        if( fermionic )
                        {
                            std::pair<tag_type,scalar_type> tag_coef_pair = model_desc.get_product_tag( model_desc.filling_matrix_tag( source ) , source_tag );

                            source_tag = tag_coef_pair.first;
                            pre_factor *= measurement_term.coeff * tag_coef_pair.second;
                        }
                        else
                        {
                            pre_factor *= measurement_term.coeff;
                        }

                        iMPO_type mpo;

                        for( int current_site = 0 ; current_site < measurement_L ; ++current_site )
                        {
                            prempo_t prempo;

                            if( current_site < source || current_site > target )
                            {
                                tag_type tag = model_desc.identity_matrix_tag( current_site );
                                prempo.push_back(boost::make_tuple(0 , 0 , tag , 1. ));
                            }
                            else if( current_site > source && current_site < target )
                            {
                                tag_type tag = model_desc.filling_matrix_tag( current_site );
                                prempo.push_back(boost::make_tuple(0 , 0 , tag , 1. ));
                            }
                            else if( current_site == source )
                            {
                                prempo.push_back(boost::make_tuple(0 , 0 , source_tag , pre_factor ));
                            }
                            else
                            {
                                prempo.push_back(boost::make_tuple(0 , 0 , target_tag , 1. ));
                            }

                            mpo.push_back( mpo_tensor(1, 1, prempo, model_desc.operator_table()) );
                        }

                        labeled_mpos.insert( make_pair(boost::any_cast<std::string>( lattice.get_prop_("label" , std::vector<int>({source,target}) ) ) , mpo) );
                    //}
                }
                break;
            }
            case 1: // Site measurements
                for( int insert_site = 0 ; insert_site < measurement_L ; ++insert_site )
                {
                    iMPO_type mpo;
                    tag_type tag = std::numeric_limits<tag_type>::max();

                    for( int current_site = 0 ; current_site < measurement_L ; ++current_site )
                    {
                        prempo_t prempo;

                        if( insert_site != current_site )
                        {
                            tag = model_desc.identity_matrix_tag( current_site );
                            prempo.push_back(boost::make_tuple(0 , 0 , tag , 1.));
                        }
                        else
                        {
                            tag = model_desc.get_product_tag( current_site , measurement_term.op_names[0] );
                            prempo.push_back(boost::make_tuple(0 , 0 , tag , measurement_term.coeff));
                        }

                        mpo.push_back( mpo_tensor(1, 1, prempo, model_desc.operator_table()) );
                    }

                    labeled_mpos.insert( make_pair(boost::any_cast<std::string>( lattice.get_prop_("label" , std::vector<int>({insert_site})) ) , mpo) );
                }
                break;
            default:
                throw std::runtime_error("LOCAL/AVERAGE measurements with terms made up of more than 2 sites are not currently supported.");
        }

        return labeled_mpos;
    }

}
