#pragma once

/** @file
    @brief Header file implementing the @p local_at measurement class.
*/

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <functional>
#include <algorithm>
#include <limits>

#include <alps/parser/xmlstream.h>

#include <boost/tuple/tuple.hpp>

#include "dmrg/block_matrix/block_matrix_algorithms.h"
#include "dmrg/block_matrix/symmetry.h"

#include "dmrg/models/measurement.h"

#include "idmrg/lattice/lattice.h"
#include "idmrg/models/imodel_descriptor.h"
#include "idmrg/mp_tensors/impo.h"

template<class Matrix, class SymmGroup>
class iMeasurement;

namespace imeasurement {

    /** @class local_at
        @brief class deriving from the class iMeasurement and implementing the notion of an infinite @p local_at measurement
        Local_at measurements in our case refer to local measurements which only take place at user specified locations
    */
    template <class Matrix, class SymmGroup>
    class local_at : public iMeasurement<Matrix, SymmGroup>
	{
		//typedef std::pair< std::vector<std::string> , std::vector< std::vector<int> > > interface_descriptor_type;

        typedef MeasurementTermDescriptor<typename Matrix::value_type> measurement_term_desc_t;
        typedef std::vector< std::vector<int> > positions_container_t;

        /*
		struct Descriptor
		{
			typedef std::vector<std::string> operator_container_type;
			typedef std::pair< std::vector<std::size_t> , std::size_t > positions_type; // The first element in the pair corresponds to the sites where the operators act, the second element corresponds to the number of unit cells required to perform the measurement.
			typedef std::vector< positions_type > positions_container_type;

			Descriptor( const interface_descriptor_type& desc )
			:
			operators_( desc.first ),
			positions_()
			{
                positions_.resize( desc.second.size() );
				for( auto i = 0 ; i < desc.second.size() ; ++i )
				{
					std::copy( desc.second[i].begin() , desc.second[i].end() , std::inserter( positions_[i].first , positions_[i].first.end() ) );
					positions_[i].second = std::numeric_limits<std::size_t>::max();
				}
			}

			void fill_sizes( iLattice& lattice )
			{
				auto num_graphs = lattice.get_num_graphs();

				lattice.set_num_graphs( 1 );
				auto sites_per_unit_cell = lattice.size();
				lattice.set_num_graphs( num_graphs );

				for( auto& pos : positions_ )
					pos.second = *std::max_element( pos.first.begin() , pos.first.end() ) / sites_per_unit_cell + 1;
			}

			void sort_positions()
			{
				using std::placeholders::_1;
				using std::placeholders::_2;
				std::sort( positions_.begin() , positions_.end() , std::bind( &Descriptor::less_than , this , _1 , _2 ) );
			}

			bool less_than( const positions_type& a , const positions_type& b ) const
			{
				assert( *std::max_element(a.first.begin(),a.first.end()) > *std::min_element(a.first.begin(),a.first.end()) );
				assert( *std::max_element(b.first.begin(),b.first.end()) > *std::min_element(b.first.begin(),b.first.end()) );
				assert( a.second != std::numeric_limits<std::size_t>::max() );
				assert( b.second != std::numeric_limits<std::size_t>::max() );

				return a.second < b.second;
			}

			const operator_container_type operators_;
			positions_container_type positions_;
		};
         typedef Descriptor descriptor_type;
		*/

		typedef iMeasurement<Matrix, SymmGroup> base_type;
        typedef typename base_type::iMPS_type iMPS_type;


		public:

			typedef iMPO<Matrix, SymmGroup> iMPO_type;
			typedef idmrg::iModelDescriptor<Matrix, SymmGroup> iModelDescriptor;
			typedef typename iModelDescriptor::scalar_type scalar_type;
			typedef typename iModelDescriptor::tag_type tag_type;

			typedef std::vector<boost::tuple<std::size_t, std::size_t, tag_type, scalar_type> > prempo_t;
			typedef typename MPO<Matrix, SymmGroup>::elem_type mpo_tensor;

			typedef typename iMeasurement<Matrix,SymmGroup>::lambda_t lambda_t;

			local_at(	std::string const& name_,
                        std::vector<measurement_term_desc_t> descriptors ,
						positions_container_t const& positions )
			:
			base_type(name_) ,
			desc_(descriptors),
            pos_(positions)
			{ }


		// AT THIS POINT FERMIONIC OPERATORS ARE NOT TREATED PROPERLY
        void evaluate( const lambda_t& lambda_inv , iMPS_type const& mps, iLattice& lattice , const iModelDescriptor& model_desc ) override
        {}
        /*
        void evaluate( const lambda_t& lambda_inv , iMPS_type const& mps, iLattice& lattice , const iModelDescriptor_type& model_desc ) override
        {
			this->vector_results.clear();
            this->labels.clear();

			// Figure out how many unit cells are necessary to carry out the meaurements and sort them according to these sizes.
			desc_.fill_sizes( lattice );
			desc_.sort_positions();

			auto num_graphs = lattice.get_num_graphs();

			unsigned L = lattice.size();

			double measure_norm = norm( L , lambda_inv , mps );

			for( unsigned measure_i = 0 ; measure_i < desc_.positions_.size() ; ++measure_i )
			{
				assert( desc_.operators_.size() == desc_.positions_[measure_i].first.size() );

				auto measurement_size = desc_.positions_[measure_i].second;

				if( lattice.get_num_graphs() != measurement_size )
				{
					lattice.set_num_graphs( measurement_size );
					measure_norm = norm( L , lambda_inv , mps );
				}

				L = lattice.size();

				std::cout << "L = " << L << '\n';
				std::cout << "Measured norm: " << measure_norm << std::endl;
				std::cout << "Descriptor: " << this->name() << std::endl;

				iMPO_type mpo(L);

				fill_mpo( mpo , desc_.positions_[measure_i].first , model_desc );
				auto eval = expval(lambda_inv , mps , mpo);
				register_measurement( eval / measure_norm , desc_.positions_[measure_i].first , lattice );
			}

			lattice.set_num_graphs( num_graphs );
        }
		*/
    protected:
		/*
		auto get_mpo_tensor( const std::vector<std::size_t>& curr_sites , const std::size_t i , const iModelDescriptor_type& model_desc ) const -> mpo_tensor;

		void fill_mpo( MPO<Matrix, SymmGroup>& mpo , const std::vector<std::size_t>& curr_sites , const iModelDescriptor_type& model_desc )
		{
			for( int i = 0 ; i < mpo.size() ; ++i )
				mpo[i] = get_mpo_tensor( curr_sites , i , model_desc );
		}

		void register_measurement( const scalar_type eval , const std::vector<std::size_t>& curr_sites , const iLattice& lattice )
		{
			this->vector_results.push_back( eval );

			std::string label;
			for( auto d : desc_.operators_ )
				label += d;

			label += '(';
			for( auto it = curr_sites.begin() ; it != curr_sites.end() - 1 ; ++it )
				label += std::to_string( *it ) + ',';
			label +=  std::to_string( curr_sites.back() ) + ')';

			this->labels.push_back( label );
		}
		*/

        local_at* do_clone() const override
        {
            return new local_at(*this);
        }

        /*
        template<typename COMP>
        void count_fermion( const iModelDescriptor_type& model_desc , std::vector<std::string>::const_iterator pos_desc , int& counter , const int i , const std::size_t pos );
        */

		//Descriptor desc_;
        std::vector<measurement_term_desc_t> desc_;
        positions_container_t pos_;

    };

    template<class Matrix,class SymmGroup,class COMP>
    struct count_fermion
    {
        typedef idmrg::iModelDescriptor<Matrix, SymmGroup> iModelDescriptor;

        count_fermion( const iModelDescriptor& model_desc , std::vector<std::string>::const_iterator pos_desc , const std::size_t i )
        :
        op_(COMP()),
        model_desc_( model_desc ),
        pos_desc_( pos_desc ),
        count_(0),
        site_(i)
        {}

        void operator()( const std::size_t pos )
        {
            auto local_tag = model_desc_.get_operator_tag( pos , *pos_desc_++ );

            if( op_(pos , site_) && model_desc_.get_tag_handler()->is_fermionic( local_tag ) )
                count_++;
        }

        COMP op_;
        const iModelDescriptor& model_desc_;
        std::vector<std::string>::const_iterator pos_desc_;
        int count_;
        const std::size_t site_;
    };

    /*
    template<typename COMP>
    void count_fermion( const iModelDescriptor_type& model_desc , std::vector<std::string>::const_iterator pos_desc , int& counter , const int i , const std::size_t pos )
    {
        COMP op = COMP();

        tag_type local_tag = model_desc.get_operator_tag( pos , *pos_desc++ );

        if( op(pos , i) && model_desc.get_tag_handler()->is_fermionic( local_tag ) )
            counter++;
    };
    */
    /*
	template<class Matrix, class SymmGroup>
	auto local_at<Matrix, SymmGroup>::get_mpo_tensor( const std::vector<std::size_t>& curr_sites , const std::size_t i , const iModelDescriptor_type& model_desc ) const -> typename local_at<Matrix, SymmGroup>::mpo_tensor
	{

		tag_type tag;

		auto curr_sites_loc = std::find( curr_sites.begin() , curr_sites.end() , i );

		if( curr_sites_loc != curr_sites.end() )
		{
			int pos = curr_sites_loc - curr_sites.begin();

			tag = model_desc.get_operator_tag( i , desc_.operators_[pos] );
		}
		else if( i > curr_sites.front() && i < curr_sites.back() )
		{
            bool need_sign = false;
            {
                //using std::placeholders::_1;

                //int left_fermions = 0;
                //int right_fermions = 0;

                auto left_fermions = std::for_each( curr_sites.begin() , curr_sites.end() , count_fermion<Matrix, SymmGroup,std::less<std::size_t>>( model_desc , desc_.operators_.begin() , i ) );
                auto right_fermions = std::for_each( curr_sites.begin() , curr_sites.end() , count_fermion<Matrix, SymmGroup,std::greater<std::size_t>>( model_desc , desc_.operators_.begin() , i ) );

                // We assume fermionic parity is conserved
                if( (left_fermions.count_ % 2) && (right_fermions.count_ % 2) )
                    need_sign = true;
            }

            if( need_sign )
                tag = model_desc.filling_matrix_tag( i );
            else
                tag = model_desc.identity_matrix_tag( i );
		}
        else
            tag = model_desc.identity_matrix_tag( i );

		prempo_t prempo;
		prempo.push_back(boost::make_tuple(0,0,tag,1.));

		return mpo_tensor(1, 1, prempo, model_desc.operator_table() );
	}
    */
}
