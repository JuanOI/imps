#pragma once

/** @file
    @brief Header file implementing utility functions for measurements
*/

#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <regex>
#include <algorithm>

#include "dmrg/utils/DmrgParameters.h"
#include "dmrg/models/measurement_parser.hpp"

#include "idmrg/mp_tensors/imps.h"
#include "idmrg/models/imeasurement.h"
#include "idmrg/models/measurements/local.hpp"
#include "idmrg/models/measurements/average.hpp"
#include "idmrg/models/measurements/local_at.hpp"

namespace idmrg{
    template<class M, class SG>
    struct iModelDescriptor;
}

class iLattice;

namespace imeasurement
{
    template<class Matrix, class SymmGroup>
    class local;

    template<class Matrix, class SymmGroup>
    class average;

    template<class Matrix, class SymmGroup>
    class local_at;

    template<class Matrix, class SymmGroup>
    using measurement_ptr = std::unique_ptr< iMeasurement<Matrix,SymmGroup> >;

    typedef std::string measurement_type_t;
    typedef std::string measurement_name_t;

    template<class Matrix, class SymmGroup>
    using measurement_cont_t = std::unordered_map< measurement_name_t , std::unordered_map<measurement_type_t , measurement_ptr<Matrix,SymmGroup> > >;

    typedef MeasurementSpecs measurement_specs_t;
    typedef std::vector<MeasurementSpecs> measurement_specs_cont_t;

    template<typename T>
    using measurement_term_desc_t = MeasurementTermDescriptor<T>;

    template<class Matrix, class SymmGroup>
    using imodel_descriptor_t = idmrg::iModelDescriptor<Matrix,SymmGroup>;

    /** @fn auto prepare_and_create_measurements(   DmrgParameters& parms ,
                                            const imodel_descriptor_t<Matrix,SymmGroup>& model_desc )
                                            -> std::unordered_map<std::string,measurement_cont_t<Matrix,SymmGroup>>
        @brief structuring user-requested measurements as a hash table.
        @param [in] parms DmrgParameters object specifying inputs for the simulation.
        @param [in] model_desc iModelDescriptor object for the system.
    	@pre @p parms shall be of type DmrgParameters.
        @pre @p parms shall correspond to the same parameters that @p model_desc was created with
    */
    template<class Matrix, class SymmGroup>
    auto prepare_and_create_measurements(   DmrgParameters& parms ,
                                            const imodel_descriptor_t<Matrix,SymmGroup>& model_desc )
                                            -> std::unordered_map<std::string,measurement_cont_t<Matrix,SymmGroup>>
    {
        using local = local<Matrix,SymmGroup>;
        using average = average<Matrix,SymmGroup>;
        using local_at = local_at<Matrix,SymmGroup>;

        using measurement_ptr = measurement_ptr<Matrix,SymmGroup>;
        using measurement_cont_t = measurement_cont_t<Matrix,SymmGroup>;
        using measurement_term_desc_t = measurement_term_desc_t<typename Matrix::value_type>;

        std::cout << "Parsing measurements...";
        auto meas_specs = parse_measurements( parms );
        std::cout << "done." << std::endl;

        std::vector<std::string> meas_types;
        meas_types.push_back("CONVERGENCE");
        meas_types.push_back("FINAL");

        std::cout << "Constructing measurements...\n" << std::endl;

        std::unordered_map<std::string,measurement_cont_t> measurements;
        for( const auto& type: meas_types )
            for( auto it_p = meas_specs.begin() ; it_p != meas_specs.end() ; ++it_p)
            {
                if( (type == "CONVERGENCE"  && it_p->meas_class.find("CONVERGENCE") == std::string::npos) ||
                    (type == "FINAL"        && it_p->meas_class.find("CONVERGENCE") != std::string::npos) )
                    continue;

                auto& measurement_specs = *it_p;
                std::regex conv_re("_CONVERGENCE");
                if( std::regex_replace(measurement_specs.meas_class,conv_re,"") == "LOCAL" ||
                    std::regex_replace(measurement_specs.meas_class,conv_re,"") == "AVERAGE" )
                {
                    std::cout << "Found '" << measurement_specs.name << "' of type " << measurement_specs.meas_class << "." << std::endl;

                    auto measurement_terms = model_desc.unpack_measurement_terms( measurement_specs.args );

                    if( measurement_specs.meas_class == "LOCAL" )
                        measurements[type][measurement_specs.name]["LOCAL"] = measurement_ptr( new local( measurement_specs.name , measurement_terms ) );
                    else
                        measurements[type][measurement_specs.name]["AVERAGE"] = measurement_ptr( new average( measurement_specs.name , measurement_terms ) );

                    continue;
                }

                if( std::regex_replace(measurement_specs.meas_class,conv_re,"") == "LOCAL_AT" )
                {
                    assert( measurement_specs.args.find("|") != std::string::npos );

                    std::cout << "Found '" << measurement_specs.name << "' of type " << measurement_specs.meas_class << "." << std::endl;

                    std::regex terms("[[:blank:]]*(.+)[[:blank:]]*\\|[[:blank:]]*(.+)[[:blank:]]*");
                    std::smatch sub_parts;
                    std::regex_search( it_p->args , sub_parts , terms );

                    std::string operators = sub_parts.str(1);
                    std::string positions_set = sub_parts.str(2);

                    // Register operator names involved
                    // At this point we only support simple measurements, ie. measurements made up of single terms (over an arbitrary number of sites) with unit prefactor.

                    std::vector<measurement_term_desc_t> measurement_terms(1 , measurement_term_desc_t(1. , std::vector<std::vector<std::string>>()));
                    {
                        std::regex colon(":");
                        auto operator_it = std::sregex_token_iterator( operators.begin() , operators.end() , colon , -1 );

                        while( operator_it != std::sregex_token_iterator() ){
                            measurement_terms.front().op_names.push_back( std::vector<std::string>(1,*operator_it++) );
                        }
                    }

                    // Register operator positions involved
                    std::vector<std::vector<int>> measurement_positions;
                    {
                        std::regex parenthesized_expression("\\([0-9,[:blank:]]*\\)");
                        auto positions_set_it = std::sregex_token_iterator( positions_set.begin() , positions_set.end() , parenthesized_expression );

                        while( positions_set_it != std::sregex_token_iterator() )
                        {
                            std::string positions = *positions_set_it++;
                            positions = std::regex_replace( positions , std::regex("[\\(\\)]") , "" );

                            std::regex comma(",");
                            auto positions_it = std::sregex_token_iterator( positions.begin() , positions.end() , comma , -1 );

                            measurement_positions.emplace_back();
                            while( positions_it != std::sregex_token_iterator() ){
                                measurement_positions.back().push_back( std::stoi( *positions_it++ ) );
                            }
                        }
                    }

                    measurements[type][measurement_specs.name]["LOCAL_AT"] = measurement_ptr( new local_at( measurement_specs.name , measurement_terms , measurement_positions ) );

                    continue;
                }

                std::cout << "Found measurement: '" << measurement_specs.name << "' of unsupported type " << measurement_specs.meas_class << ".\n Skipping it." << std::endl;
            }
            std::cout << "\nFinished.\n" << std::endl;

        return measurements;
    }

    /** @fn void run_measurements( 	measurement_cont_t<Matrix,SymmGroup>& measurements ,
                            const typename iMPS<Matrix,SymmGroup>::lambda_t& lambda_inverse ,
                            const iMPS<Matrix,SymmGroup>& mps ,
                            iLattice& lattice ,
                            const iModelDescriptor<Matrix, SymmGroup>& model_desc ,
                            DmrgParameters& parms ,
                            bool conv_measurements = false )
        @brief function running all measurements requested
        @param [in] measurements set of extracted measurements
        @param [in] lambda_inverse matrix of inverse Schmidt values for second to last iteration
        @param [in] mps the iMPS to measure on
        @param [in] lattice the infinite lattice describing the actual geometry of the system
        @param [in] model_desc the model descriptor for the system
        @param [in] parms the set of parameters used for the simulation
        @param [in] conv_measurements parameter indicating whether these measurements are so-called convergence measurements or regular measurements.
    */
    template<class Matrix, class SymmGroup>
    void run_measurements( 	measurement_cont_t<Matrix,SymmGroup>& measurements ,
                            const typename iMPS<Matrix,SymmGroup>::lambda_t& lambda_inverse ,
                            const iMPS<Matrix,SymmGroup>& mps ,
                            iLattice& lattice ,
                            const imodel_descriptor_t<Matrix, SymmGroup>& model_desc ,
                            DmrgParameters& parms ,
                            bool conv_measurements = false )
    {
        bool verbose = parms["verbose"];

        std::cout << "\nRunning measurements...\n" << std::endl;

        std::string obs_name = parms["resultfile"];

        std::string prefix_path = "spectrum/";
        if(conv_measurements)
        {
            int conv_meas_step = parms["convergence_measure_step"];
            int sweeps = parms["sweeps"];
            prefix_path +=  "convergence/" +
                            std::to_string(conv_meas_step) +
                            "/iteration/" +
                            std::to_string(sweeps) +
                            "/";
        }
        prefix_path += "results/";

        //std::cout << "----------------------------------" << prefix_path << std::endl;

        for( auto& by_name : measurements )
        {
            for( auto& by_type : by_name.second )
            {
                if(verbose) std::cout << "Measuring type " << by_type.first << "." << std::endl;
                auto& imeasurement = *(by_type.second);

                imeasurement.evaluate(lambda_inverse , mps , lattice , model_desc);
                {
                    storage::archive obs_ar(obs_name,"w");
                    imeasurement.save( obs_ar , prefix_path );
                }
            }
        }

        std::cout << "\nFinished.\n" << std::endl;
    }

    /** @fn void measure(   const typename iMPS<Matrix,SymmGroup>::lambda_t& lambda_inverse ,
                    const iMPS<Matrix,SymmGroup>& mps ,
                    iLattice& lattice ,
                    const iModelDescriptor<Matrix, SymmGroup>& model_desc ,
                    DmrgParameters& parms ,
                    bool conv_measurements = false )
        @brief convenience function wrapping the previous two functions
    */
    template<class Matrix, class SymmGroup>
    void measure(   const typename iMPS<Matrix,SymmGroup>::lambda_t& lambda_inverse ,
                    const iMPS<Matrix,SymmGroup>& mps ,
                    iLattice& lattice ,
                    const imodel_descriptor_t<Matrix, SymmGroup>& model_desc ,
                    DmrgParameters& parms ,
                    bool conv_measurements = false )
    {
        static std::unordered_map<std::string,measurement_cont_t<Matrix,SymmGroup>> measurements = prepare_and_create_measurements( parms , model_desc );

        run_measurements( conv_measurements ? measurements["CONVERGENCE"] : measurements["FINAL"] , lambda_inverse , mps , lattice , model_desc , parms , conv_measurements );

    }

} // namespace imeasurement
