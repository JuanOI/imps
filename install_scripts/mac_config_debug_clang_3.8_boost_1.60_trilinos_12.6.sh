#!/bin/bash

export MKLROOT=""

BUILD_PREFIX=build_debug_clang_3.8_boost_1.60_trilinos_12.6.1
INSTALL_PREFIX=~/opt/iMPS/debug_clang_3.8_boost_1.60_trilinos_12.6.1

rm -rf ${BUILD_PREFIX} 
mkdir ${BUILD_PREFIX} 
cd ${BUILD_PREFIX}

module purge 
module load alps-2.3.b1-clang-3.8-boost-1.60-debug
module load trilinos-12.6.1-clang-3.8-boost-1.60-debug 

cmake \
-DCMAKE_C_COMPILER=clang \
-DCMAKE_CXX_COMPILER=clang++ \
-DCMAKE_BUILD_TYPE:STRING="Debug" \
-DCMAKE_EXE_LINKER_FLAGS:STRING="-framework Accelerate" \
-DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_PREFIX} \
-DLOCAL_POSTFIX:STRING="_clang" \
..

make -j 2 install
