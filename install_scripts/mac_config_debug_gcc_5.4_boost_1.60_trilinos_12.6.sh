#!/bin/bash

export MKLROOT=""

rm -rf build_debug_gcc_5.4_boost_1.60_trilinos_12.6.1
mkdir build_debug_gcc_5.4_boost_1.60_trilinos_12.6.1
cd build_debug_gcc_5.4_boost_1.60_trilinos_12.6.1

module load alps-2.3.b1-gcc-5.4-boost-1.60-debug
module load trilinos-12.6.1-gcc-5.4-boost-1.60-debug 

cmake \
-DCMAKE_C_COMPILER=gcc \
-DCMAKE_CXX_COMPILER=g++ \
-DCMAKE_CXX_FLAGS:STRING="-std=c++11 -fmax-errors=10 -Wall -Wextra -Wunreachable-code -pedantic -Wno-sign-compare -Wno-unused -Wno-deprecated-declarations" \
-DCMAKE_BUILD_TYPE:STRING="Debug" \
-DCMAKE_EXE_LINKER_FLAGS:STRING="-framework Accelerate" \
-DCMAKE_INSTALL_PREFIX:PATH=~/opt/iMPS/debug_gcc_5.4_boost_1.60_trilinos_12.6.1 \
-DALPS_DIR:PATH=${ALPSHOME}/share/alps \
-DALPS_MPS_DIR:PATH=${ALPSHOME}/share/alps \
-DTrilinos_DIR:PATH=${TRILINOSHOME} \
-DLOCAL_POSTFIX:STRING="_gcc" \
..

make -j 2 install
