/*****************************************************************************
 *
 * ALPS MPS DMRG Project
 *
 * Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
 *               2011-2013 by Michele Dolfi <dolfim@phys.ethz.ch>
 *                            Bela Bauer <bauerb@comp-phys.org>
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 *****************************************************************************/

#include <iostream>
#include <sstream>
#include <fstream>
#include <boost/shared_ptr.hpp>
#include <alps/hdf5.hpp>

#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix<double> matrix;


#include "dmrg/block_matrix/symmetry.h"
typedef NU1 grp;

#include "dmrg/models/lattice.h"
#include "dmrg/models/model.h"
#include "dmrg/models/generate_mpo.hpp"

#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/mpo_ops.h"
#include "dmrg/mp_tensors/twositetensor.h"

#include "dmrg/optimize/ietl_lanczos_solver.h"
#include "dmrg/optimize/ietl_jacobi_davidson.h"

#include "dmrg/utils/DmrgParameters.h"

template <class Matrix, class SymmGroup>
MPO<Matrix, SymmGroup> from_model() {
    int M = 20;
    int L = 4;
    double Jz = 1.;
    double Jxy = 1.;
    double h = 1.;
    
    DmrgParameters parms;
    parms.set("LATTICE", "open chain lattice");
    parms.set("L", L);
    parms.set("MODEL", "spin");
    parms.set("Jz", Jz);
    parms.set("h", h);
    parms.set("Jxy", Jxy);
    
    Lattice lattice(parms);
    Model<Matrix, SymmGroup> model(lattice, parms);
    
    Index<SymmGroup> phys = model.phys_dim(0);
    
    MPO<Matrix, SymmGroup> mpo = make_mpo(lattice, model, parms);
    return mpo;
}

template <class Matrix, class SymmGroup>
MPO<Matrix, SymmGroup> from_op_in_model() {
    int M = 20;
    int L = 4;
    double Jz = 0.;
    double h = 1.;
    double Jxy = 1.;

    DmrgParameters parms;
    parms.set("LATTICE", "open chain lattice");
    parms.set("L", L);
    parms.set("MODEL", "spin");
    parms.set("Jz", Jz);
    parms.set("h", h);
    parms.set("Jxy", Jxy);
    
    Lattice lattice(parms);
    Model<Matrix, SymmGroup> model(lattice, parms);
    
    Index<SymmGroup> phys = model.phys_dim(0);
    
    typedef typename MPOTensor<Matrix, SymmGroup>::tag_type tag_type;
    typedef std::vector<boost::tuple<std::size_t, std::size_t, tag_type, double> > prempo_t;
    
    tag_type ident_tag  = model.identity_matrix_tag();
    tag_type mag_tag    = model.get_operator_tag("Sz");
    tag_type splus_tag  = model.get_operator_tag("Splus");
    tag_type sminus_tag = model.get_operator_tag("Sminus");
    
    std::cout << "TAG " << ident_tag << " :: ident" << std::endl;
    std::cout << "TAG " <<  mag_tag  << " :: Sz" << std::endl;
    std::cout << "TAG " << splus_tag << " :: Splus" << std::endl;
    std::cout << "TAG " << sminus_tag << " :: Sminus" << std::endl;

    MPO<Matrix, SymmGroup> mpo(L);
    {
        prempo_t prempo;
        prempo.push_back(boost::make_tuple(0,0,ident_tag,1.));
        prempo.push_back(boost::make_tuple(1,1,ident_tag,1.));
        prempo.push_back(boost::make_tuple(0,1,mag_tag,h));
        prempo.push_back(boost::make_tuple(0,2,splus_tag,Jxy/2.));
        prempo.push_back(boost::make_tuple(0,3,sminus_tag,Jxy/2.));
        prempo.push_back(boost::make_tuple(2,1,sminus_tag,1.));
        prempo.push_back(boost::make_tuple(3,1,splus_tag,1.));
        
        MPOTensor<Matrix, SymmGroup> m(4, 4, prempo, model.operators_table()->get_operator_table());
        
        for (unsigned p=1; p<L-1; ++p)
            mpo[p] = m;
    }
    {
        unsigned p=0;
        
        prempo_t prempo;
        prempo.push_back(boost::make_tuple(0,0,ident_tag,1.));
        prempo.push_back(boost::make_tuple(0,1,mag_tag,h));
        prempo.push_back(boost::make_tuple(0,2,splus_tag,Jxy/2.));
        prempo.push_back(boost::make_tuple(0,3,sminus_tag,Jxy/2.));
        
        MPOTensor<Matrix, SymmGroup> m(1, 4, prempo, model.operators_table()->get_operator_table());
        mpo[p] = m;
    }
    {
        unsigned p=L-1;
        
        prempo_t prempo;
        prempo.push_back(boost::make_tuple(0,0,mag_tag,h));
        prempo.push_back(boost::make_tuple(1,0,ident_tag,1.));
        prempo.push_back(boost::make_tuple(2,0,sminus_tag,1.));
        prempo.push_back(boost::make_tuple(3,0,splus_tag,1.));
        
        MPOTensor<Matrix, SymmGroup> m(4, 1, prempo, model.operators_table()->get_operator_table());
        mpo[p] = m;
    }
    return mpo;
}

template <class Matrix, class SymmGroup>
MPO<Matrix, SymmGroup> from_scratch() {
    
    typedef block_matrix<Matrix, SymmGroup> op_t;
    typedef typename SymmGroup::charge charge;
    
    int M = 20;
    int L = 4;
    double h = 1.;
    double Jxy = 1.;
    
    charge Up, Down;
    Up[0] = 1;
    Down[0] = -1;
    
    // Spin-1/2
    Index<SymmGroup> phys;
    phys.insert(std::make_pair(Up, 1));
    phys.insert(std::make_pair(Down, 1));
    
    // phys.insert(std::make_pair(SymmGroup::IdentityCharge, 2));
    
    op_t magop;
    magop.insert_block(Matrix(1,1,1./2), Up,Up);
    magop.insert_block(Matrix(1,1,-1./2), Down,Down);
    op_t ident = identity_matrix<Matrix>(phys);
    
    op_t splus, sminus;
    {
        Matrix m(1,1);
        m(0,0) = 1.;
        splus.insert_block(m, Down,Up);
    }
    sminus.insert_block(Matrix(1,1,1), Up,Down);
    
    typedef typename MPOTensor<Matrix, SymmGroup>::tag_type tag_type;
    typedef std::vector<boost::tuple<std::size_t, std::size_t, tag_type, double> > prempo_t;
    typedef typename MPOTensor<Matrix, SymmGroup>::op_table_ptr op_table_ptr;
    
    op_table_ptr optable(new OPTable<Matrix, SymmGroup>());
    tag_type ident_tag  = optable->register_op(ident);
    tag_type mag_tag    = optable->register_op(magop);
    tag_type splus_tag  = optable->register_op(splus);
    tag_type sminus_tag = optable->register_op(sminus);
    
    std::cout << "TAG " << ident_tag << " :: ident" << std::endl;
    std::cout << "TAG " <<  mag_tag  << " :: Sz" << std::endl;
    std::cout << "TAG " << splus_tag << " :: Splus" << std::endl;
    std::cout << "TAG " << sminus_tag << " :: Sminus" << std::endl;
    
    MPO<Matrix, SymmGroup> mpo(L);
    {
        prempo_t prempo;
        prempo.push_back(boost::make_tuple(0,0,ident_tag,1.));
        prempo.push_back(boost::make_tuple(1,1,ident_tag,1.));
        prempo.push_back(boost::make_tuple(0,1,mag_tag,h));
        prempo.push_back(boost::make_tuple(0,2,splus_tag,Jxy/2.));
        prempo.push_back(boost::make_tuple(0,3,sminus_tag,Jxy/2.));
        prempo.push_back(boost::make_tuple(2,1,sminus_tag,1.));
        prempo.push_back(boost::make_tuple(3,1,splus_tag,1.));
        
        MPOTensor<Matrix, SymmGroup> m(4, 4, prempo, optable);
        
        for (unsigned p=1; p<L-1; ++p)
            mpo[p] = m;
    }
    {
        unsigned p=0;
        
        prempo_t prempo;
        prempo.push_back(boost::make_tuple(0,0,ident_tag,1.));
        prempo.push_back(boost::make_tuple(0,1,mag_tag,h));
        prempo.push_back(boost::make_tuple(0,2,splus_tag,Jxy/2.));
        prempo.push_back(boost::make_tuple(0,3,sminus_tag,Jxy/2.));
        
        MPOTensor<Matrix, SymmGroup> m(1, 4, prempo, optable);
        mpo[p] = m;
    }
    {
        unsigned p=L-1;
        
        prempo_t prempo;
        prempo.push_back(boost::make_tuple(0,0,mag_tag,h));
        prempo.push_back(boost::make_tuple(1,0,ident_tag,1.));
        prempo.push_back(boost::make_tuple(2,0,sminus_tag,1.));
        prempo.push_back(boost::make_tuple(3,0,splus_tag,1.));
        
        MPOTensor<Matrix, SymmGroup> m(4, 1, prempo, optable);
        mpo[p] = m;
    }
    
    return mpo;
}


template <class Matrix, class SymmGroup, class F>
void run_test(F const & generator) {
    MPO<Matrix, SymmGroup> mpo = generator();
    follow_and_print_terms(mpo, -1, 0, 0);
}


int main(int argc, char ** argv)
{
    std::cout << "\n\n### FROM MODEL ###" << std::endl;
    run_test<matrix, grp>(from_model<matrix, grp>);
    std::cout << "\n\n### OPS FROM MODEL ###" << std::endl;
    run_test<matrix, grp>(from_op_in_model<matrix, grp>);
    std::cout << "\n\n### FROM SCRATCH ###" << std::endl;
    run_test<matrix, grp>(from_scratch<matrix, grp>);
}

