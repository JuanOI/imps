import pyalps
import numpy as np
import matplotlib.pyplot as plt
import pyalps.plot

import sys

if len(sys.argv) != 2:
    print 'Usage:', sys.argv[0], '<res.h5>'
    sys.exit(1)
resfile = sys.argv[1]


data = pyalps.loadEigenstateMeasurements([resfile], ['Energy'])
for d in pyalps.flatten(data):
    print 'Energy:', d.y


data = pyalps.loadEigenstateMeasurements([resfile], ['Local density'])
for d in pyalps.flatten(data):
    print 'Density x:', d.x
    print 'Density y:', d.y
    
    d.y = d.y[0]

plt.figure()
pyalps.plot.plot(data)



data = pyalps.loadEigenstateMeasurements([resfile], ['Density correlations'])
for d in pyalps.flatten(data):
    print 'Correlations x:\n', d.x
    print 'Correlations y:\n', d.y
    

plt.show()