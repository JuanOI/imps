#include <alps/hdf5.hpp>

#include <vector>
#include <string>

int main() {
  
  std::string resfile = "res.h5";
  
  unsigned L = 10;
  
  double energy = 1.5;
  
  std::vector<double> density;
  std::vector<std::string> density_labels;
  for (unsigned p=0; p<L; ++p) {
    density.push_back(1.);
    density_labels.push_back("( " +std::to_string(p)+ " )");
  }
  

  std::vector<double> corr;
  std::vector<std::string> corr_labels;
  for (unsigned i=0; i<L-1; ++i) {
    for (unsigned j=i+1; j<L; ++j) {
      corr.push_back(i+double(j)/i);
      corr_labels.push_back("( "+std::to_string(i)+" ) -- ( "+std::to_string(j)+" )");
    }
  }
  
  
  {
    alps::hdf5::archive ar(resfile, "w");
    
    /// Parameters (mandatory)
    ar["/parameters/L"] << L;
    /// if using MPS BaseParameters, just do:
    /// ar["/parameters"] << parms;
    
    /// Save energy
    ar["/spectrum/results/Energy/mean/value"] << std::vector<double>(1, energy);


    /// Save density
    ar["/spectrum/results/Local density/mean/value"] << std::vector<std::vector<double> >(1, density);
    ar["/spectrum/results/Local density/labels"] << density_labels;
    
    
    /// Save correlations
    ar["/spectrum/results/Density correlations/mean/value"] << std::vector<std::vector<double> >(1, corr);
    ar["/spectrum/results/Density correlations/labels"] << corr_labels;
  }
}