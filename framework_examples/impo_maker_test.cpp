/*****************************************************************************
 *
 * ALPS MPS DMRG Project
 *
 * Copyright (C) 2014 Institute for Theoretical Physics, ETH Zurich
 *               2011-2013 by Michele Dolfi <dolfim@phys.ethz.ch>
 *                            Bela Bauer <bauerb@comp-phys.org>
 *
 * This software is part of the ALPS Applications, published under the ALPS
 * Application License; you can use, redistribute it and/or modify it under
 * the terms of the license, either version 1 or (at your option) any later
 * version.
 *
 * You should have received a copy of the ALPS Application License along with
 * the ALPS Applications; see the file LICENSE.txt. If not, the license is also
 * available from http://alps.comp-phys.org/.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 *****************************************************************************/

#include <iostream>
#include <sstream>
#include <fstream>

#include "dmrg/block_matrix/detail/alps.hpp"
typedef alps::numeric::matrix<double> matrix;

#include "dmrg/block_matrix/symmetry.h"
typedef NU1 grp;

#include "dmrg/models/lattice.h"
#include "dmrg/models/model.h"
#include "dmrg/models/generate_mpo.hpp"

#include "dmrg/mp_tensors/mps.h"
#include "dmrg/mp_tensors/twositetensor.h"

#include "dmrg/optimize/ietl_lanczos_solver.h"
#include "dmrg/optimize/ietl_jacobi_davidson.h"

#include "dmrg/utils/DmrgParameters.h"

#include "mp_tensors/impo_maker.h"


template <class Matrix, class SymmGroup>
void print_mpo(std::ostream & os, MPO<Matrix, SymmGroup> const& mpo, bool print_each_table=false)
{
    for (int p = 0; p < mpo.size(); ++p) {
        os << "+++ SITE " << p << " +++" << "\n++++++++++++++" << std::endl;
        for (int b1 = 0; b1 < mpo[p].row_dim(); ++b1) {
            for (int b2 = 0; b2 < mpo[p].col_dim(); ++b2) {
                if (mpo[p].has(b1, b2)) os << mpo[p].tag_number(b1,b2) << " ";
                else os << ". ";
            }
            os << std::endl;
        }
        
        os << std::endl;
        
        if (print_each_table) {
            typename MPOTensor<Matrix, SymmGroup>::op_table_ptr op_table = mpo[p].get_operator_table();
            for (unsigned tag=0; tag<op_table->size(); ++tag) {
                os << "TAG " << tag << std::endl;
                os << " * op :\n" << (*op_table)[tag] << std::endl;
            }
        }
    }
    if (!print_each_table) {
        os << "+++ OPERATORS +++" << "\n+++++++++++++++++" << std::endl;
        typename MPOTensor<Matrix, SymmGroup>::op_table_ptr op_table = mpo[0].get_operator_table();
        for (unsigned tag=0; tag<op_table->size(); ++tag) {
            os << "TAG " << tag << std::endl;
            os << " * op :\n" << (*op_table)[tag] << std::endl;
        }
    }

}


template <class Matrix, class SymmGroup>
void test(BaseParameters & model_parms)
{
    std::cout << model_parms << std::endl;
    
    Lattice lattice(model_parms);
    Model<Matrix, SymmGroup> model(lattice, model_parms);
    
    generate_mpo::TaggedIMPOMaker<Matrix, SymmGroup> mpom(lattice, model);
    MPO<Matrix, SymmGroup> mpo = mpom.create_mpo();
    print_mpo(std::cout, mpo);
}




int main(int argc, char ** argv)
{
    try {
        
        {
            DmrgParameters model_parms;
            model_parms.set("LATTICE", "chain lattice"); // in ALPS this is periodic
            model_parms.set("L", 2);
            model_parms.set("MODEL", "hardcore boson");
            model_parms.set("t", 1.);
            model_parms.set("CONSERVED_QUANTUMNUMBERS", "N");
            model_parms.set("N_total", 2);
            
            
            test<matrix,grp>(model_parms);
        }
        std::cout << "\n\n==========================================\n==========================================\n" << std::endl;
        {
            DmrgParameters model_parms;
            model_parms.set("LATTICE", "ladder"); // in ALPS this is periodic
            model_parms.set("L", 2);
            model_parms.set("MODEL", "hardcore boson");
            model_parms.set("t", 1.);
            model_parms.set("CONSERVED_QUANTUMNUMBERS", "N");
            model_parms.set("N_total", 2);

            test<matrix,grp>(model_parms);
        }
        std::cout << "\n\n==========================================\n==========================================\n" << std::endl;
        {
            DmrgParameters model_parms;
            model_parms.set("LATTICE", "ladder"); // in ALPS this is periodic
            model_parms.set("L", 2);
            model_parms.set("MODEL", "spinless fermions");
            model_parms.set("t", 1.);
            model_parms.set("CONSERVED_QUANTUMNUMBERS", "N");
            model_parms.set("N_total", 2);

            test<matrix,grp>(model_parms);
        }
        std::cout << "\n\n==========================================\n==========================================\n" << std::endl;
        {
            DmrgParameters model_parms;
            model_parms.set("LATTICE", "ladder"); // in ALPS this is periodic
            model_parms.set("L", 2);
            model_parms.set("MODEL", "spinless fermions");
            model_parms.set("t", 1.);
            model_parms.set("t\'", 1.);
            model_parms.set("CONSERVED_QUANTUMNUMBERS", "N");
            model_parms.set("N_total", 2);

            test<matrix,grp>(model_parms);
        }
        std::cout << "\n\n==========================================\n==========================================\n" << std::endl;
        {
            DmrgParameters model_parms;
            model_parms.set("LATTICE", "ladder"); // in ALPS this is periodic
            model_parms.set("L", 2);
            model_parms.set("MODEL", "fermion Hubbard");
            model_parms.set("t", 1.);
            model_parms.set("CONSERVED_QUANTUMNUMBERS", "Nup,Ndown");
            model_parms.set("Nup_total", 1);
            model_parms.set("Ndown_total", 1);

            test<matrix,grp>(model_parms);
        }
        
    } catch (std::exception & e) {
        maquis::cerr << "Exception caught:" << std::endl << e.what() << std::endl;
        throw;
    }
}

